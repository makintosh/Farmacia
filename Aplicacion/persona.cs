﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fabisa.Aplicacion
{
    public class persona
    {
        //Atributos

        private string cedula;
        private string nombre;
        private string apellido;
        private string telefono;
        private string direccion;
        private bool estado;

        // Getters y Setters
        public string getCedula()
        { return cedula; }

        public void setCedula(string cedula) 
        { this.cedula = cedula; }

        public string getNombre()
        { return nombre; }

        public void setNombre(string nombre) 
        { this.nombre = nombre; }

        public string getApellido()
        {
            return apellido;
        }

        public void setApellido(string apellido) 
        { this.apellido = apellido; }


        public string getTelefono() 
        { return telefono; }

        public void setTelefono(string telefono) 
        { this.telefono = telefono; }

        public void setDireccion(string direccion)
        { this.direccion = direccion; }

        public string getDireccion() 
        { return direccion; }


        public bool getEstado()
        {
            return estado;
        }

        public void setEstado(bool estado)
        {
            this.estado = estado;
        }




        //Constructor

        public persona()
        {
            
        }


        // validacion de la cédula.
        public static bool validar_cedula(String ced)
        {
            bool t = false;
           
                Char[] cedula = new Char[13];
                
                cedula = ced.ToCharArray(); //convertir cadena de caracteres a vector
                int numeroProvincia = int.Parse(String.Concat(cedula[0], cedula[1]));//convierte de char a string y concatena


                if (numeroProvincia > 0 && numeroProvincia <= 24)
                {

                    int sumaPares = 0, sumaImpares = 0, numeroImpar = 0, contadorP = 1, contadorI = 0;

                    for (int i = 0; i < 5; i++)
                    {

                        sumaPares += int.Parse(cedula[contadorP].ToString()); // suma las posiciones pares
                        numeroImpar = int.Parse(cedula[contadorI].ToString()) * 2;

                        if (numeroImpar > 9)
                        {
                            cedula[contadorI] = char.Parse((numeroImpar - 9).ToString());
                        }
                        else
                        {
                            cedula[contadorI] = char.Parse(numeroImpar.ToString());
                        }

                        sumaImpares += int.Parse(cedula[contadorI].ToString()); // suma las posiciones impares
                        contadorI += 2;
                        contadorP += 2;

                    }

                    sumaPares -= int.Parse(cedula[9].ToString());
                    int sumaTotal = sumaImpares + sumaPares;

                    if (((((sumaTotal / 10) + 1) * 10) - sumaTotal) == int.Parse(cedula[9].ToString()) || int.Parse(sumaTotal.ToString().Substring(1)) == 0)
                    {
                        t = true;
                        return t;
                    }
                    else
                    {
                        t = false;
                        return t;

                    }
                }

                else
                {
                    t = false;
                    return t;
                }
         
        }// fin validacion de cédula        
        
    }
}
