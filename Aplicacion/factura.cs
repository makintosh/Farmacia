﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using fabisa.Conexion;
using System.Data.OleDb;
using System.Data;
using Npgsql;
using System.Windows.Forms;


namespace fabisa.Aplicacion
{
    public class factura
    {
        //atributos
        private Int64 numero_fac;
        private DateTime fecha_fac;
        private double subtotal_fac;
        private double iva_fac;
        private double ivacero_fac;
        private double total_fac;
        private double descuento_fac;


        public double getDescuento_fac()
        {
            return descuento_fac;
        }

        public void setDescuento_fac(double descuento)
        {
            this.descuento_fac = descuento;
        }


        public Int64 getNumeroFactura() 
        { 
            return numero_fac; 
        }

        public void setNumeroFactura(Int64 numeroFactura)
        { 
            this.numero_fac = numeroFactura;
        }

        public DateTime getFechaFactura() 
        { 
            return fecha_fac;
        }
        
        public void setFechaFactura(DateTime fecha) 
        { 
            this.fecha_fac = fecha;
        }

        public double getSubtotal() 
        { 
            return subtotal_fac;
        }

        public void setSubtotal(double subtotal) 
        { 
            this.subtotal_fac = subtotal;
        }

        public double getIva()
        { 
            return iva_fac;
        }
        
        public void setIva(double iva) 
        { 
            this.iva_fac = iva;
        }

        public double getIvaCero() 
        { 
            return ivacero_fac;
        }
        
        public void setIvaCero(double descuento) 
        { 
            this.ivacero_fac = descuento; 
        }

        public double getTotalFactura() 
        { 
            return total_fac;
        }

        public void setTotalFactura(double total) 
        { 
            this.total_fac = total;
        }
       
        
        
        

       
        //constructor
        public factura()
        {
            
            
        }
        
        // Global variables
        string sql;
        conexion_bd con = new conexion_bd();

        // Methods
        //selecciona el stock de el producto en la BD
        public int saber_stock(int codigo)
        {

            
            String sql = ("SELECT unidades_pro FROM PRODUCTO WHERE codigo_pro=" + codigo + "");
            String dato = con.EjecutarExecuteQuery(sql);
            return int.Parse(dato);
        }

        
        //guarda el stock modificado en la BD
        public void modificar_stock(int codigo, int stock)
        {
            
            String sql = ("UPDATE producto SET unidades_pro='" + stock + "' WHERE codigo_pro=" + codigo + "");
            con.EjecutarExecuteNonQuery(sql);
        }

        /// <summary>
        /// Select a current product and it's current price
        /// </summary>
        /// <param name="nombreProducto"></param>
        /// <returns></returns>
        public DataSet completar_producto_factura_venta(string nombreProducto)
        {
            DataSet data = new DataSet();
            try
            {
                sql = ("SELECT codigo, p_venta::NUMERIC as Precio, iva_pro , unic_ode FROM producto_view WHERE producto ='" + nombreProducto + "'");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(data, "producto");
            }
            catch (NpgsqlException exception) 
            {
                MessageBox.Show(exception.ErrorSql);
            }
            return data;
        }


        
        
        /// <summary>
        /// Selecciona el completar_producto y el precio de un producto parametro de entrada la descripcion del producto en la factura de compras
        /// </summary>
        /// <param name="nombreProducto"></param>
        /// <returns></returns>
        public DataSet completar_producto_factura_compra(string nombreProducto)
        {
            
            DataSet data = new DataSet();
            try
            {
                sql = ("SELECT codigo ,p_compra as Precio, iva_pro , unic_ode FROM producto_view WHERE producto ='" + nombreProducto + "'");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(data, "producto");
            }
            catch (NpgsqlException exception) 
            { 
                MessageBox.Show(exception.ErrorSql);
            }
            return data;
        }




        /// <summary>
        /// Obtiene todos los productos para la accion de autocompletar en factura de compra
        /// </summary>
        /// <returns></returns>
        public DataTable autocompletar_producto_facturacompra()
        {
            DataTable dt_producto = new DataTable();
            sql = ("SELECT  producto as PRODUCTO, codigo as CODIGO FROM producto_view");
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
            consulta.Fill(dt_producto);
            return dt_producto;
        }

        /// <summary>
        /// Obtiene todos los productos con stock para autocompletar.
        /// </summary>
        /// <returns></returns>
        public DataTable autocompletar_producto_facturaventa()
        {
            DataTable dt_producto = new DataTable();
            try
            {
                sql = ("SELECT producto as PRODUCTO FROM producto_view WHERE stock > 0");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(dt_producto);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.Message);
            }
            return dt_producto;
        }


        /// <summary>
        /// Anula una Factura
        /// </summary>
        /// <param name="numeroFactura"># de factura</param>
        /// <param name="entidad">tabla de bd</param>
        public void anularFactura(int numeroFactura, string entidad)
        {
            string sql = "UPDATE " + entidad + " SET est_fac = FALSE WHERE numero_fac = " + numeroFactura + " ";
            con.EjecutarExecuteNonQuery(sql);
        }


        public void eliminarFacturaCompra(int numero)
        {
            string sql = "DELETE FROM factura_compra WHERE numero_fac = '"+numero+"' ";
                con.EjecutarExecuteNonQuery(sql);
        }

        /// <summary>
        /// Get products
        /// </summary>
        /// <returns></returns>
        public DataTable getProductData()
        {
            DataTable productDataTable = new DataTable();
            try {
                sql = ("SELECT CONCAT(producto,' ',talla) as producto, codigo FROM producto_view ORDER BY producto");
                NpgsqlDataAdapter query = con.EjecutarExecuteReader1(sql);
                query.Fill(productDataTable);
            }
            catch (NpgsqlException exception) {
                MessageBox.Show(exception.Message);
            }
            return productDataTable;
        }
    }
}
