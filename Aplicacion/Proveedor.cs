﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using fabisa.Conexion;
using Npgsql;
using System.Windows.Forms;

namespace fabisa.Aplicacion
{
    public class Proveedor
    {
        //ATRIBUTOS

        private string razon_social;
        private string ruc;
        private string telefono;
        private string direccion;
        private string ciudad;

        //private List<proveedor> listaDeProveedor;

        //public void setListaDeProveedor(proveedor proveedor)
        //{
        //    listaDeProveedor.Add(proveedor);
        //}




        // getters y setters
        public string getRazon()
        {
            return razon_social;
        }

        public void setRazon(string razon)
        {
            razon_social = razon;
        }

        public string getRuc()
        {
            return ruc;
        }

        public void setRuc(string ruc)
        {
            this.ruc = ruc;
        }


        public string getTelefono()
        { 
            return telefono; 
        }

        public void setTelefono(string telefono)
        { 
            this.telefono = telefono; 
        }

        public string getDireccion()
        { 
            return direccion;
        }

        public void setDireccion(string direccion)
        { 
            this.direccion = direccion; 
        }

        public string getCiudad()
        { 
            return ciudad; 
        }

        public void setCiudad(string ciudad)
        { 
            this.ciudad = ciudad; 
        }

       


        
        
        
        
        //CONSTRUCTOR

        public Proveedor()
        {
            
        }

        // METODOS
        conexion_bd con = new conexion_bd();

        /// <summary>
        /// Guarda un Proveedor
        /// </summary>
        /// <param name="prov">Una instancia de la clase Proveedor</param>
        public void guardar(Proveedor prov)
        {
            
            string sql = ("INSERT INTO proveedor (ruc_prv,razon_social_prv,telefono_prv,direccion_prv,ciudad_prv,estado_prv) VALUES ('" + prov.getRuc() + "','" + prov.getRazon() + "','" + prov.getTelefono() + "','" + prov.getDireccion() + "','" + prov.getCiudad() + "','"+true+"')");
            con.EjecutarExecuteNonQuery(sql);
        }

        /// <summary>
        /// Modifica un Proveedor
        /// </summary>
        /// <param name="prov">Una instancia de la clase Proveedor</param>
        public void actulizar(Proveedor prov)
        {
            
            string sql = ("UPDATE proveedor SET razon_social_prv='" + prov.getRazon() + "', telefono_prv='" + prov.getTelefono() + "',direccion_prv='" + prov.getDireccion() + "',ciudad_prv='" + prov.getCiudad() + "' WHERE ruc_prv='" + prov.getRuc() + "'");
            con.EjecutarExecuteNonQuery(sql);
        }

        /// <summary>
        /// Elimina un proveedor
        /// </summary>
        /// <param name="ruc">El Ruc del Proveedor</param>
        public void eliminar(string ruc)
        {
            
            string sql = ("UPDATE proveedor SET estado_prv = False WHERE ruc_prv='" + ruc + "'");
            con.EjecutarExecuteReader(sql);
        }


        /// <summary>
        /// Carga todos los Proveedores con estado activo
        /// </summary>
        /// <returns></returns>
        public DataTable seleccionarpro()
        {
            DataTable tabla = new DataTable();            
            String sql = ("SELECT * FROM proveedor WHERE estado_prv = true");
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
            consulta.Fill(tabla);
            return tabla;

        }

        //buscar por ruc del proveedor
        public DataTable buscarxFiltros(String filtro, string tablaEnBaseDeDatos)
        {
            DataTable dtProveedor = new DataTable();
            try
            {
                String sql = ("SELECT * FROM proveedor WHERE " + tablaEnBaseDeDatos + " LIKE '%" + filtro + "%' AND estado_prv = true");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(dtProveedor);
                
            }
            catch(NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return dtProveedor;

        }

       

        /// <summary>
        /// Carga todos los proveedores con estado activo 
        /// </summary>
        /// <returns></returns>

        public DataSet cargar_proveedor()
        {
            DataSet cargar = new DataSet();
            try
            {
                string sql = ("SELECT * from proveedor WHERE estado_prv = true");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar, "prov");
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }
            return cargar;
        }


     

       




    }
}
