﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fabisa.Conexion;

namespace fabisa.Aplicacion
{
    class movimiento
    {

        private int codigoMovimiento;
        private double ingresoMovimiento;
        private double egresoMovimiento;
        private String asuntoMovimiento;
        private double totalMovimiento;
        private DateTime fechaMovimiento;

        public void setCodigoMovimiento(int codigo)
        {
            this.codigoMovimiento = codigo;
        }

        public int getCodigoMovimiento()
        {
            return codigoMovimiento;
        }


        public void setIngresoMovimiento(double ingreso)
        {
            ingresoMovimiento = ingreso;
        }

        public double getIngresoMovimiento()
        {
            return ingresoMovimiento;
        }

        public void setEgresoMovimiento(double egreso)
        {
            egresoMovimiento = egreso;
        }

        public double getEgresoMovimiento()
        {
            return egresoMovimiento;
        }

        public void setAsuntoMovimiento(String asunto)
        {
            asuntoMovimiento = asunto;
        }

        public String getAsuntoMovimiento()
        {
            return asuntoMovimiento;
        }

        public void setTotalMovimiento(double total)
        {
            totalMovimiento = total;
        }

        public double getTotalMovimiento()
        {
            return totalMovimiento;
        }

        public void setFechaMovimiento(DateTime fecha)
        {
            fechaMovimiento = fecha;
        }

        public DateTime getFechaMovimiento()
        {
            return fechaMovimiento;
        }



        string sql;
        conexion_bd conex = new conexion_bd();

        public void guardar(movimiento movimiento, String cedulaUsuario)
        {
            try
            {
                sql = "INSERT INTO gastos ('" + movimiento.getIngresoMovimiento() + "','" + movimiento.getEgresoMovimiento() + "','" + movimiento.getAsuntoMovimiento() + "','" + movimiento.getTotalMovimiento() + "','"+cedulaUsuario+"','" + movimiento.getFechaMovimiento() + "')";
                conex.EjecutarExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public void actualizar(movimiento movimiento, String cedulaUsuario)
        {
            try
            {
                sql = " UPDATE gastos SET ing_gst='" + movimiento.getIngresoMovimiento() + "', egr_gst='" + movimiento.getEgresoMovimiento() + "',ast_ast='" + movimiento.getAsuntoMovimiento() + "',tot_gst='" + movimiento.getTotalMovimiento() + "',ced_per='"+cedulaUsuario+"',fec_gst='" + movimiento.getFechaMovimiento() + "' WHERE cod_gst = "+movimiento.getCodigoMovimiento()+" ";
                conex.EjecutarExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public void borrar(int codigoMovimiento)
        {
            try
            {
                sql = "DELETE * FROM gastos WHERE cod_gst ="+codigoMovimiento+"";
                conex.EjecutarExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
 
        }





    }
}
