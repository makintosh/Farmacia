﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using fabisa.Conexion;
using System.Windows.Forms;
using Npgsql;



namespace fabisa.Aplicacion
{
    public class producto
    {
        //Atributos

        private int codigo;
        private string descripcion;
        private string fabricante;
        private double precioCompra;
        private double precioVenta;
        private int unidades;
        private DateTime fechaExpiracion;
        private bool iva;
        private int tipoProducto;
        private int marca;
        private string nombreMarca;
        private string talla;
        private string codigoIdentificador;
        private bool tienda;


        public void setTienda(bool t)
        {
            this.tienda = t;
        }

        public bool getTienda()
        {
            return tienda;
        }

        public void setCodigoIdentificador(string idCode)
        {
            codigoIdentificador = idCode;
        }

        public string getCodigoIdentificador()
        {
            return codigoIdentificador;
        }


        public string getNombreMarca()
        {
            return nombreMarca;
        }

        public void setNombreMarca(string nombreMarca)
        {
            this.nombreMarca = nombreMarca;
        }
        
        
        public void setTipoProducto(int tipoProducto)
        {
            this.tipoProducto = tipoProducto;
        }

        public int getTipoProducto()
        {
            return tipoProducto;
        }

        public void setMarca(int marca)
        {
            this.marca = marca;
        }

        public int getMarca()
        {
            return marca;
        }

        public void setTalla(string talla)
        {
            this.talla = talla;
        }

        public string getTalla()
        {
            return talla;
        }

        public void setPrecioVenta(double precio_venta)
        {
            this.precioVenta = precio_venta;
        }



        public double getPrecioVenta()
        {
            return precioVenta;
        }


        public int getCodigo()
        {
            return codigo;
        }


        public void setCodigo(int codigo)
        {
            this.codigo = codigo;
        }


        public string getDescripcion()
        {
            return descripcion;
        }

        public void setDescripcion(string descripcion)
        {
            this.descripcion = descripcion;
        }


        public string getFabricante()
        {
            return fabricante;
        }


        public void setFabricante(string fabricante)
        {
            this.fabricante = fabricante;
        }


        public double getPrecioCompra()
        {
            return precioCompra;
        }

        public void setPrecioCompra(double precio_compra)
        {
            this.precioCompra = precio_compra;
        }


        public int getUnidades()
        {
            return unidades;
        }

        public void setUnidades(int unidades)
        {
            this.unidades = unidades;
        }



        public DateTime getFechaExpiracion()
        {
            return fechaExpiracion;
        }


        public void setFechaExpiracion(DateTime fecha)
        {
            this.fechaExpiracion = fecha;
        }



        public bool getIva()
        {
            return iva;
        }


        public void setIva(bool iva)
        {
            this.iva = iva;
        }

        //CONSTRUCTOR
        public producto()
        {

        }


        conexion_bd conexion = new conexion_bd();
        string consultaSql;
        //METODOS

        /// <summary>º
        /// Guarda un Producto
        /// </summary>
        /// <param name="pro"></param>
        public void guardar_producto(producto pro)
        {
            try
            {
                consultaSql = ("INSERT INTO producto(codigo_pro,descripcion_pro,unidades_pro,iva_pro,precio_venta_pro ,precio_compra_pro ,  id_mar , talla_pro , codigo_identificador_pro, id_tip_pro, tienda_pro) VALUES('" + pro.getCodigo() + "','" + pro.getDescripcion() + "','"+pro.getUnidades()+"','" + pro.getIva() + "','" + pro.getPrecioVenta() + "' , '" + pro.getPrecioCompra() + "','" + pro.getMarca() + "','" + pro.getTalla() + "','" + pro.getCodigoIdentificador() + "','"+pro.getTipoProducto()+"','"+pro.getTienda()+"' )");
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }

        }


        /// <summary>
        /// Guarda todo el detalle del producto(talla , tipo , stock)
        /// </summary>
        /// <param name="talla"></param>
        /// <param name="producto"></param>
        /// <param name="tipo"></param>
        /// <param name="stock"></param>
        public void guardarDetalleProductoTallaTipo(int talla, int producto, int tipo, int stock)
        {
            try
            {
                consultaSql = "INSERT INTO detalle_producto_talla_tipo(id_pro , id_talla , id_tipo , stock) VALUES (" + producto + "," + talla + "," + tipo + "," + stock + ")";
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }
        }

        /// <summary>
        /// Eliminar Producto
        /// </summary>
        /// <param name="codigo">codigo del producto</param>
        public void eliminar_producto_codigo(int codigo)
        {

            try
            {
                consultaSql = ("DELETE  FROM  producto WHERE codigo_pro='" + codigo + "'");
                conexion.EjecutarExecuteReader(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }

        }




        /// <summary>
        /// Modificar Producto
        /// </summary>
        /// <param name="producto">Una instancia de la clase Producto</param>
        public void actualizar_producto(producto producto)
        {
            try
            {
                consultaSql = ("UPDATE producto SET descripcion_pro='" + producto.getDescripcion() + "',unidades_pro='" + producto.getUnidades() + "',iva_pro = '" + producto.getIva() + "', precio_venta_pro= '" + producto.getPrecioVenta() + "', talla_pro = '" + producto.getTalla() + "', id_tip_pro = '" + producto.getTipoProducto() + "' , id_mar = '" + producto.getMarca() + "' , codigo_identificador_pro = '" + producto.getCodigoIdentificador() + "' , tienda_pro = '"+producto.getTienda()+"'  WHERE codigo_pro=" + producto.getCodigo() + "");
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }
        }



        //TABLAS


        

        /// <summary>
        /// Obtiene todos los Tipos de Productos
        /// </summary>
        /// <returns></returns>
        public DataTable getProductType()
        {
            DataTable dataProductType = new DataTable();
            try
            {
                consultaSql = "SELECT * FROM tipo_producto ORDER BY id ASC";
                NpgsqlDataAdapter getRequest = conexion.EjecutarExecuteReader1(consultaSql);
                getRequest.Fill(dataProductType);
            }
            catch (NpgsqlException exep)
            {
                MessageBox.Show(exep.Message);
            }
            return dataProductType;

        }

        /// <summary>
        /// Obtiene la Marca
        /// </summary>
        /// <returns></returns>
        public DataTable getBrand()
        {
            DataTable dataBrand = new DataTable();
            try
            {
                consultaSql = "SELECT * FROM marca";
                NpgsqlDataAdapter getRequest = conexion.EjecutarExecuteReader1(consultaSql);
                getRequest.Fill(dataBrand);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.Message);
            }
            return dataBrand;
        }


        /// <summary>
        /// Obtiene todos los productos
        /// </summary>
        /// <returns></returns>
        public DataTable seleccionar_productos()
        {
            DataTable tabla_bus = new DataTable();
            try
            {

                consultaSql = ("SELECT unic_ode, descripcion, talla , stock , p_venta , codigo,iva_pro , p_compra , tienda  FROM producto_view ORDER BY codigo ASC");
                NpgsqlDataAdapter consulta = conexion.EjecutarExecuteReader1(consultaSql);
                consulta.Fill(tabla_bus);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar en la bd: " + ex + ".");

            }
            return tabla_bus;
        }

        /// <summary>
        /// Busqueda de Productos por Filtros (Descripcion.....)
        /// </summary>
        /// <param name="filtro"></param>
        /// <param name="nombreTabla"></param>
        /// <returns></returns>
        public DataTable buscarporFiltros(string filtro, string nombreTabla)
        {
            DataTable tabla_bus = new DataTable();
            try {
                consultaSql = ("SELECT unic_ode, descripcion, talla, stock, p_venta, codigo, iva_pro, p_compra, tienda FROM producto_view WHERE " + nombreTabla + " LIKE '%" + filtro + "%'");
                NpgsqlDataAdapter consulta = conexion.EjecutarExecuteReader1(consultaSql);
                consulta.Fill(tabla_bus);

            } catch (NpgsqlException ex) {
                MessageBox.Show("Error al buscar en la bd: " + ex + ".");
            }
            return tabla_bus;
        }

        public DataSet cargar_productos()
        {
            DataSet cargar = new DataSet();
            try
            {
                consultaSql = ("SELECT * FROM producto_view ORDER BY codigo ASC");
                NpgsqlDataAdapter consulta = conexion.EjecutarExecuteReader1(consultaSql);
                consulta.Fill(cargar, "PRODUCTO");
            }
            catch (NpgsqlException ex)
            {
                MessageBox.Show("Error al buscar en la bd: " + ex + ".");

            }
            return cargar;
        }



        public int numeroMenor()
        {
            consultaSql = "SELECT MIN(codigo_pro) FROM producto";
            return int.Parse(conexion.EjecutarExecuteQuery(consultaSql));


        }

        public int numeroMayor()
        {
            consultaSql = "SELECT MAX(codigo_pro) FROM producto";
            return int.Parse(conexion.EjecutarExecuteQuery(consultaSql));
        }


        public void actualizarPrecioVenta(decimal precioVentaModificado, int codigoProducto)
        {
            try
            {
                consultaSql = "UPDATE producto SET precio_venta_pro = '" + precioVentaModificado + "' where codigo_pro=" + codigoProducto + "";
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }
        }

        public void actualizarPrecioCompra(double precioCompraModificado, int codigoProducto)
        {
            try
            {
                consultaSql = "UPDATE producto SET precio_compra_pro = '" + precioCompraModificado + "' where codigo_pro=" + codigoProducto + "";
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }
        }


        /// <summary>
        /// Guarda la Marca
        /// </summary>
        /// <param name="nombreMarca"></param>
        public void guardarMarca(string nombreMarca)
        {
            try
            {
                consultaSql = "INSERT INTO marca (mar_nom) VALUES ('" + nombreMarca + "')";
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        public void editarMarca(string nombreMarca , int id)
        {
            try
            {
                consultaSql = "UPDATE marca SET mar_nom = '"+nombreMarca+"' WHERE id = "+id+" ";
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }



        public DataTable obtenerMarca()
        {
            DataTable getMarca = new DataTable();
            try
            {

                consultaSql = ("SELECT mar_nom as marca, id  FROM marca ORDER BY id ASC");
                NpgsqlDataAdapter consulta = conexion.EjecutarExecuteReader1(consultaSql);
                consulta.Fill(getMarca);

            }
            catch (NpgsqlException ex)
            {
                MessageBox.Show("Error al buscar en la bd: " + ex.ErrorSql + ".");

            }
            return getMarca;
        }


        public void guardarImagen(string rutaImagen, int codigoProducto)
        {
            try
            {
                consultaSql = "INSERT INTO imagen (img_ruta , id_pro) VALUES ('" + rutaImagen + "', " + codigoProducto + ")";
                conexion.EjecutarExecuteNonQuery(consultaSql);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }


        public DataSet cargarImagenes(int codigo)
        {
            DataSet ds = new DataSet();
            try
            {
                consultaSql = "SELECT img_ruta as ruta FROM imagen WHERE id_pro = " + codigo + " ";
                NpgsqlDataAdapter adapter = conexion.EjecutarExecuteReader1(consultaSql);
                adapter.Fill(ds, "imagen");

            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.Message);
            }
            return ds;
        }
        

        /// <summary>
        /// Obtiene las tallas desde la Bd de acuerdo al tipo
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public DataTable getTalla(int tipo)
        {
            DataTable data = new DataTable();
            try
            {
                consultaSql = "SELECT id , talla_nom as talla FROM talla";
                NpgsqlDataAdapter adapter = conexion.EjecutarExecuteReader1(consultaSql);
                adapter.Fill(data);
            }
            catch (NpgsqlException exception)
            {
                MessageBox.Show(exception.ErrorSql);
            }
            return data;

        }


    } // fin class producto
}