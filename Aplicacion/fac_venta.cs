﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using fabisa.Conexion;
using Npgsql;
using System.Windows.Forms;

namespace fabisa.Aplicacion
{
    class fac_venta:factura
    {
               

        //constructor heredando de factura

        public fac_venta():base()
            
        {
            
        }

        public detalleFactura detalleFacturaVenta
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        //Metodos

        //cuenta el numero de campos en la tabla factura_venta     
        conexion_bd con = new conexion_bd();
        string sql;
        

        
        /// <summary>
        /// obtiene el numero mayor de las facturas de venta
        /// </summary>
        /// <returns></returns>
 
        public Int64 contar_max()
        {
            
            sql = ("SELECT MAX(numero_fac) FROM factura_venta");
            String dato = con.EjecutarExecuteQuery(sql);
            return Convert.ToInt64(dato);
        }

        //guardar la factura venta
        public void guardar(fac_venta fac,string ced_cli,string ced_usu,DateTime fecha)
        {

            sql = ("INSERT INTO factura_venta (numero_fac,fecha_fac,subtotal_fac,iva_fac,ivacero_fac,total_fac,hora_fac,cedula_usu,descuento_fac,cedula_cli,est_fac) VALUES ('" + fac.getNumeroFactura() + "','" + fecha.Date + "','" + fac.getSubtotal() + "','" + fac.getIva() + "','" + fac.getIvaCero() + "','" + fac.getTotalFactura() + "','" + fac.getFechaFactura().ToShortTimeString() + "','" + ced_usu + "','" + fac.getDescuento_fac() + "','" + ced_cli + "',TRUE)");
            con.EjecutarExecuteNonQuery(sql);
        }

        
        //guardar el detalle de la factura venta
        public void guardar_detalle_factura(double total, int cantidad, Int64 numero_fac, int codigo_pro, double currentPrice)
        {
            
            sql = ("INSERT INTO detalle_factura_venta (precio_det_fac_ven,cantidad_det_fac_ven,codigo_pro,numero_fac_ven, current_price_pro) VALUES ('" + total + "','" + cantidad + "','" + codigo_pro + "','" + numero_fac + "', '"+currentPrice+"')");
            con.EjecutarExecuteNonQuery(sql);

        }

        //muestra las facturas
        public DataTable mostrar_facturas()
        {
            DataTable cargar = new DataTable();
            try
            {
            sql = ( "SELECT numero, CONCAT(nombre,' ',apellido) as cliente, fecha, total FROM factura_venta_view WHERE estado = true" );
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }

            return cargar;
        }

        
        //muestra las el detalle de las facturas segun el numero de factura
        public DataTable cargar_detalle_facturas(int numerofac)
        {
            DataTable cargar = new DataTable();
            try
            {
                sql = ("SELECT codigo , id , CONCAT(producto, ' ',unidad_medida) as producto, cantidad , precio::NUMERIC , total::NUMERIC  FROM detalle_factura_venta_view WHERE numero=" + numerofac + "");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        public DataTable mostrar_facturasxfecha(DateTime fecha1, DateTime fecha2)
        {
            DataTable cargar = new DataTable();
            try {
            sql = ( "SELECT numero, CONCAT(nombre,' ',apellido) as cliente, fecha, total FROM factura_venta_view WHERE fecha BETWEEN '" + fecha1 + "' AND '" + fecha2 + "' AND estado = TRUE" );
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e) {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        public DataTable mostrar_total_facxdia(string fecha)
        {
            DataTable cargar = new DataTable();
            try
            {
                sql = ("SELECT numero, cliente,fecha, total FROM factura_venta_view WHERE fecha = '" + fecha + "'");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        public DataTable mostrar_total_facxmes(string fecha)
        {
            DataTable cargar = new DataTable();
            try
            {
            sql = ( "SELECT numero, cliente,fecha, total FROM factura_venta_view WHERE EXTRACT(MONTH FROM fecha) = '" + fecha + "'" );
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        public DataTable mostrar_total_facxyear(string fecha)
        {
            DataTable cargar = new DataTable();
            try
            {
            sql = ( "SELECT numero, cliente,fecha, total FROM factura_venta_view WHERE EXTRACT(YEAR FROM fecha_fac) = '" + fecha + "'" );
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        //carga el combo box con la cedula y nombre del vendedor
        public DataTable cargar_combo_usuario()
        {
            DataTable data = new DataTable();
            try
            {
                sql = ("SELECT CONCAT(nombre_per,' ',apellido_per) as Vendedor,cedula_per as Cedula FROM usuario WHERE estado_per = TRUE ORDER BY nombre_per");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(data);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return data;

        }

        //carga en el combobox de cliente
        public DataTable cargar_combo_cliente()
        {
            DataTable data = new DataTable();
            try
            {
                sql = ("SELECT CONCAT(nombre_per,' ',apellido_per) as Cliente,cedula_per as Cedula FROM cliente");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(data);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return data;

        }


        //facturas por vendedor
        public DataTable fac_x_vendedor(string ced)
        {
            DataTable cargar = new DataTable();
            try
            {
            sql = ( "SELECT numero, CONCAT(nombre,' ',apellido) as cliente,fecha, total FROM factura_venta_view WHERE cedula_usu='" + ced + "' AND estado = TRUE " );
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        //facturas de cada cliente
        public DataTable fac_x_cli(string ced)
        {
            DataTable cargar = new DataTable();
            try
            {
            sql = ( "SELECT numero, CONCAT(nombre,' ',apellido) as cliente,fecha, total FROM factura_venta_view WHERE cedula_cli='" + ced + "' AND estado = TRUE ORDER BY numero ASC" );
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        public DataSet cargar_factura()
        {
            DataSet cargar = new DataSet();

            try
            {
                sql = ("SELECT * FROM factura_venta_view");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar, "FACTURA");
            }

            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return cargar;
        }

        

        //autocompletar cliente
        public DataTable autocompletar_cliente()
        {
            DataTable dt_cliente = new DataTable();            
            sql = ("SELECT cedula_per FROM cliente");
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
            consulta.Fill(dt_cliente);
            return dt_cliente;
        }

        /// <summary>
        /// Método para completar los textbox que pertenecen al cliente 
        /// </summary>
        /// <param name="ced">Cédula</param>
        /// <returns>Retorna un DataSet con los datos del Cliente referido</returns>
        public DataSet autocompletar_cliente_en_textbox(string ced)
        {
            DataSet data = new DataSet();            
            sql = ("SELECT CONCAT(nombre_per,' ',apellido_per) as Cliente, telefono_per as Tel, direccion_per as Direccion FROM cliente WHERE cedula_per='"+ced+"'");
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
            consulta.Fill(data, "cliente");
            return data;
        }
        
        /// <summary>
        /// Get invoice by numero_fac (PRIMARY KEY)
        /// </summary>
        /// <param name="invoiceId">numero_fac</param>
        /// <returns></returns>
        public DataSet getInvoiceById(int invoiceId){
            DataSet data = new DataSet();
            try {
                sql = ("SELECT * FROM factura_venta_view WHERE numero = '"+invoiceId+"' ");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(data, "FACTURA");
            }catch (NpgsqlException e) {
                MessageBox.Show("" + e);
            }
            return data;
        }

        /// <summary>
        /// Filters sales report by
        /// </summary>
        /// <param name="from">start date</param>
        /// <param name="to">end date</param>
        /// <param name="employeeCi">employee's identification number</param>
        /// <param name="clientId">client's identification number</param>
        /// <returns></returns>
        public DataTable getConsolidatedReport(DateTime from, DateTime to, string employeeCi, string clientId) {
            DataTable cargar = new DataTable();
            try {
                sql = "SELECT numero, vendedor, CONCAT(nombre,' ',apellido) as cliente, fecha, total"+
                " FROM factura_venta_view "+ 
                "WHERE fecha BETWEEN '" + from + "' AND '" + to + "' AND estado = TRUE";
                if (employeeCi != "-1") {
                    sql += " AND cedula_usu = '" + employeeCi + "'";
                }
                if (clientId != "-1") {
                    sql += " AND cedula_cli = '" + clientId + "'";
                }
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar);
            }
            catch (NpgsqlException e) {
                MessageBox.Show("" + e);
            }
            return cargar;
        }
    }
}
