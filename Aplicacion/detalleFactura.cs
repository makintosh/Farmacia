﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fabisa.Aplicacion
{
    public class detalleFactura
    {
        int cantidad;
        double currentPrice;
        double total;

        public int getCantidad()
        {
            return cantidad;
        }

        public void setCantidad(int cantidad)
        {
                this.cantidad = cantidad;
        }


        public double getTotal()
        {
            return total;
        }

        public void setTotal(double total)
        {
            this.total = total;
        }

        public double getCurrentPrice() {
            return this.currentPrice;
        }

        public void setCurrentPrice(double currentPrice){
            this.currentPrice = currentPrice;
        }

    }
}
