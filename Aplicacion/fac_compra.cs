﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fabisa.Conexion;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using Npgsql;

namespace fabisa.Aplicacion
{
    public class fac_compra:factura
    {
        private string numeroFacturaReal;

        public void setNumeroFacturaReal(string numeroFacturaReal)
        {
            this.numeroFacturaReal = numeroFacturaReal;
        }


        public string getNumeroFacturaReal()
        {
            return this.numeroFacturaReal;
        }


        public fac_compra()
            : base()
        {
            
        }

        public detalleFactura detalleFactura
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        //Metodos
        conexion_bd con = new conexion_bd();

        DataTable dataTableFacturaCompra = new DataTable();
        string query;

   

        //retorna el > de todos los numeros de compra
        public Int64 max_fac_com()
        {
            query = ("SELECT MAX(numero_fac) FROM factura_compra");
            string dato = con.EjecutarExecuteQuery(query);
            return Convert.ToInt64(dato);
        }

     


        //guardar la factura compra
        public void guardar_factura_compra(fac_compra fac,string ruc,string ced_usu, DateTime fecha)
        {
            try
            {
                query = ("INSERT INTO factura_compra (numero_fac,fecha_fac,subtotal_fac,iva_fac,ivacero_fac,total_fac,hora_fac,ruc_prv,cedula_usu,descuento_fac,numero_fac_real,est_fac) VALUES('" + fac.getNumeroFactura() + "','" + fac.getFechaFactura() + "','" + fac.getSubtotal() + "','" + fac.getIva() + "','" + fac.getIvaCero() + "','" + fac.getTotalFactura() + "','" + fecha.ToShortTimeString() + "','" + ruc + "','" + ced_usu + "','" + fac.getDescuento_fac() + "','"+fac.getNumeroFacturaReal()+"',TRUE)");
                con.EjecutarExecuteNonQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                

            }
        }

        //guardar el detalle de la factura compra
        public void guardar_detalle_factura_compra(double total, int codigo, int cantidad, Int64 numerofac, double currentPrice)
        {
            try
            {
                query = ("INSERT INTO detalle_factura_compra (precio_det_fac_com,codigo_pro,cantidad_det_fac_com,numero_fac_com, current_price_pro) VALUES ('" + total + "','" + codigo + "','" + cantidad + "','" + numerofac + "', '"+currentPrice+"')");
                con.EjecutarExecuteNonQuery(query);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        // DataTabla para extraer de la base de datos el ruc
        public DataTable autocompletar_ruc()
        {
            DataTable dt = new DataTable();            
            try
            {
                query = ("SELECT ruc_prv FROM proveedor");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(dt);
                return dt;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return dt;
            }
        }

        // Dataset para completar los atributos del proveedor
        public DataSet completar_proveedor(string ruc)
        {
            DataSet dt_proveedor = new DataSet();
            try
            {
                query = ("SELECT razon_social_prv,telefono_prv,direccion_prv,ciudad_prv FROM proveedor WHERE ruc_prv='" + ruc + "'");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(dt_proveedor, "proveedor");
                return dt_proveedor;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return dt_proveedor;
            }
        }

        // Dataset que carga la factura de compra
        public DataSet cargar_factura_compra()
        {
            DataSet ds_fcompra = new DataSet();
            try
            {
                query = ("SELECT * FROM factura_compra_view");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(ds_fcompra, "factura_compra");
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                
            }
            return ds_fcompra;
        }

        public DataTable cargar_detalle_factura_compra(int numero)
        {
            //DataTable dt_fcompra = new DataTable();
            dataTableFacturaCompra.Clear();
            try
            {
                query = ("SELECT codigo , producto , cantidad , precio , total FROM detalle_factura_compra_view WHERE numero = " + numero + "");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(dataTableFacturaCompra);
                return dataTableFacturaCompra;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return dataTableFacturaCompra;
            }
        }


        /// <summary>
        /// Utilizada en el Formulario Mostrar Facturas compradas 
        /// </summary>
        /// <returns></returns>
        public DataTable mostrar_facturas()
        {
            
            dataTableFacturaCompra.Clear();
            try
            {
                
                query = ("SELECT numero, nombre, fecha, total FROM factura_compra_view");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(dataTableFacturaCompra);
                return dataTableFacturaCompra;
            }
            catch(Exception e)
            {
                MessageBox.Show("" + e);
                return dataTableFacturaCompra;
            }
        }

        /// <summary>
        /// Muestra todas la facturas de el rango de fechas elegido.
        /// </summary>
        /// <param name="fecha1"></param>
        /// <param name="fecha2"></param>
        /// <returns></returns>
        public DataTable mostrar_facturasxfecha(DateTime fecha1, DateTime fecha2)
        {
            //DataTable cargar = new DataTable();
            dataTableFacturaCompra.Clear();
            try
            {
            query = ( "SELECT  numero,nombre,fecha,total FROM factura_compra_view WHERE fecha BETWEEN '" + fecha1 + "' AND '" + fecha2 + "' AND estado = TRUE" );
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(dataTableFacturaCompra);
                return dataTableFacturaCompra;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return dataTableFacturaCompra;
            }
        }



        public DataTable mostrarProveedores()
        {
            DataTable dtMostrarProveedores = new DataTable();
            try
            {
            query = "SELECT razon_social_prv as proveedor ,  ruc_prv as ruc FROM proveedor";
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
            consulta.Fill(dtMostrarProveedores);
            return dtMostrarProveedores;        
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return dtMostrarProveedores;
            }
        }


        public DataTable mostrarFacturasPorProveedor(string rucDelProveedor)
        {
            DataTable dtMostrarFacturasPorProveedor = new DataTable();
            try
            {
                query = "SELECT factura_compra.numero_fac as numero , factura_compra.fecha_fac as fecha , factura_compra.total_fac as total FROM factura_compra WHERE ruc_prv = '"+rucDelProveedor+"'";
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(dtMostrarFacturasPorProveedor);
                return dtMostrarFacturasPorProveedor;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Source);
                return dtMostrarFacturasPorProveedor;
            }
        }

        /// <summary>
        /// Return an invoice by id
        /// </summary>
        /// <returns></returns>
        public DataSet getInvoiceById(int invoiceId)
        {
            DataSet ds_fcompra = new DataSet();
            try {
                query = ("SELECT * FROM factura_compra_view WHERE numero = '"+invoiceId+"' ");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(query);
                consulta.Fill(ds_fcompra, "factura_compra");
            }
            catch (Exception e) {
                MessageBox.Show(e.Message);
            }
            return ds_fcompra;
        }

    }
}
