﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fabisa.Conexion;
using System.Data;
using Npgsql;

namespace fabisa.Aplicacion
{
    class CustomizedReport {
        
        // Global variables
        string sql;
        conexion_bd dbConnection = new conexion_bd();

        /// <summary>
        /// Get profitability data
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="employeeId"> Employee's id</param>
        /// <param name="productId"> Product's code</param>
        /// <returns></returns>
        public DataTable getProfitabilityData(DateTime from, DateTime to, string employeeId, string productId) {
            DataTable data = new DataTable();
            try {
                sql = "SELECT fv.fecha_fac as invoice_date,"+
                " dfvv.numero as invoice_number," +
                " dfvv.codigo as product_code," +
                " CONCAT(dfvv.producto, ' ',dfvv.unidad_medida) as product_name," +
                " ROUND( (dfvv.precio * dfvv.cantidad), 4) as sell_price," +
                " ROUND( (dfvv.current_purchase_price_pro * dfvv.cantidad), 4) as purchase_price," +
                " dfvv.cantidad as quantity," +
                " ROUND( (dfvv.precio - dfvv.current_purchase_price_pro) * dfvv.cantidad, 4) as benefit," +
                " ROUND( (((dfvv.precio - dfvv.current_purchase_price_pro)/dfvv.current_purchase_price_pro)*100), 2) as profitability" +
                " FROM detalle_factura_venta_view dfvv, factura_venta fv" +
                " WHERE fv.numero_fac = dfvv.numero" +
                " AND fv.est_fac = true"+
                " AND fv.fecha_fac BETWEEN '" + from + "' AND '" + to + "'";
                if (employeeId != "-1")
                {
                    sql += " AND fv.cedula_usu = '" + employeeId + "'";
                }
                if (productId != "-1")
                {
                    sql += " AND dfvv.codigo = '" + productId + "'";
                }
                NpgsqlDataAdapter query = dbConnection.EjecutarExecuteReader1(sql);
                query.Fill(data);
            }
            catch (NpgsqlException e) {
                throw new Exception(e.Message);
            }
            return data;
        }
    }
}
