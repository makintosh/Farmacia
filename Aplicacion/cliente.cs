﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using fabisa.Conexion;
using System.Windows.Forms;
using Npgsql;




namespace fabisa.Aplicacion
{
    public class cliente:persona
    {
        //ATRIBUTOS                  
       
        

        //CONSTRUCTOR

        public cliente():base()
        {
            
        }

 
        //METODOS



        conexion_bd con = new conexion_bd();
        string sql; 


        public void guardar(cliente cli)
        {
            try
            {
                //conexion_bd con = new conexion_bd();
                sql = ("INSERT INTO cliente(cedula_per,nombre_per,apellido_per,telefono_per,direccion_per,estado_per) VALUES('" + cli.getCedula() + "','" + cli.getNombre() + "','" + cli.getApellido() + "','" + cli.getTelefono() + "','" + cli.getDireccion() + "', '" + cli.getEstado() + "')");
                con.EjecutarExecuteNonQuery(sql);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            
        }

        //ELIMINAR
        public void eliminar(String cedula,bool estado)
        {
            try
            {
               
                sql = ("UPDATE cliente SET estado_per =" + estado + " where cedula_per='" + cedula + "'");
                con.EjecutarExecuteReader(sql);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }

        }

        public void actualizar(cliente cli)
        {
            try
            {
                sql = ("UPDATE cliente SET nombre_per='" + cli.getNombre() + "',apellido_per='" + cli.getApellido() + "',telefono_per='" + cli.getTelefono() + "',direccion_per='" + cli.getDireccion() + "' WHERE cedula_per='" + cli.getCedula() + "'");
                con.EjecutarExecuteNonQuery(sql);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
        }



        //TABLAS


        public DataTable buscarXFiltros(String filtro , string nombreTablaEnBD)
        {
            DataTable tabla_bus = new DataTable();
            try
            {                          
            
                sql = ("SELECT * FROM cliente WHERE estado_per = TRUE AND "+nombreTablaEnBD+" LIKE '%" + filtro + "%'");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(tabla_bus);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar en la bd: " + ex + ".");
                
            }
            return tabla_bus;
        }




       public DataTable seleccionartodo()
        {
            DataTable tabla_bus = new DataTable();
            try
            {
                //conexion_bd con = new conexion_bd();
                sql = ("SELECT cedula_per, nombre_per, apellido_per, direccion_per , telefono_per FROM cliente WHERE estado_per = TRUE ");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(tabla_bus);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar en la bd: " + ex + ".");
                
            }
            return tabla_bus;
        }

   

        public DataSet cargar_cliente()
        {
            DataSet cargar = new DataSet();
            try
            {
                sql = ("SELECT * from cliente WHERE estado_per=TRUE");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(cargar, "cliente");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar en la bd: " + ex + ".");
                
            }
            return cargar;
        }


        // CONTAR CLIENTES
        public int contarClientes()
        {
            try
            {
                sql = "SELECT COUNT(*) FROM cliente";
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al buscar en la bd: " + ex + ".");
                
            }
            return int.Parse(con.EjecutarExecuteQuery(sql));
        }


   
    }
}
