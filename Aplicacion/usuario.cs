﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using fabisa.Conexion;
using Npgsql;
using System.Windows.Forms;
namespace fabisa.Aplicacion
{
    public class usuario:persona
    {
        //Atributos

        private string cargo;        
        private int codigo_hora;
        private string horas;
        private string passwordUsuario;
        double sueldo;


        // Getters y Setters
        public string getPasswordUsuario()
        {
            return this.passwordUsuario;
        }

        public void setPasswordUsuario(string password)
        {
            this.passwordUsuario = password;
        }

        public string getCargo()
        {
            return cargo;
        }


        public void setCargo(string cargo)
        {
            this.cargo = cargo;
        }

        public int getCodigo_hora()
        {
            return codigo_hora;
        }


        public void setCodigo_hora(int codigo)
        {
            codigo_hora = codigo;
        }


        public string getHoras()
        {
            return horas;
        }

        public void setHoras(string horas)
        {
            this.horas = horas;
        }


        public double getSueldo()
        {
            return this.sueldo;
        }

        public void setSueldo(double sueldo)
        {
            this.sueldo = sueldo;
        }
        
        
        
        //Constructor

        public usuario()
            : base()
        {
            this.setCargo(cargo);
            this.setCodigo_hora(codigo_hora);
        }

        
        
        //Metodos


        conexion_bd con = new conexion_bd();

        //Nuevo Usuario
        public void guardarUsuario(usuario usuario)
        {
            try
            {
                string query = "INSERT INTO usuario VALUES('" + usuario.getCedula() + "','" + usuario.getNombre() + "','" + usuario.getApellido() + "','" + usuario.getDireccion() + "','" + usuario.getTelefono() + "','" + true + "','" + usuario.getPasswordUsuario() + "','" + usuario.getSueldo() + "','" + usuario.getCargo() + "')";
                con.EjecutarExecuteReader(query);
            }
            catch (NpgsqlException exception)
            {
                Console.Write(exception.ErrorSql);
            }
        }

        //Borrar Lógicamente un Usuario

        public void deleteUsuario(string cedula)
        {
            string query = "UPDATE Usuario SET estado_per = FALSE WHERE cedula_per = '" + cedula + "'";
            con.EjecutarExecuteNonQuery(query);
        }


        // Actualizar Datos Usuario

        public void actualizarUsuario(usuario usuario)
        {
            string query = "UPDATE usuario SET nombre_per = '" + usuario.getNombre() +"',apellido_per ='"+usuario.getApellido()+"', direccion_per='"+usuario.getDireccion()+"', telefono_per = '"+usuario.getTelefono()+"', sueldo_usu = '"+usuario.getSueldo()+"' , tipo_usu = '"+usuario.getCargo()+"' WHERE cedula_per = '"+usuario.getCedula()+"'  ";
            con.EjecutarExecuteNonQuery(query);
        }



        //Inicio de Sesion
        public DataTable logueo(string cedula, string pass)
        {
            DataTable dt_usu = new DataTable();           
            string sql = ("SELECT * FROM usuario WHERE cedula_per='"+cedula+"' AND contrase_usu='"+pass+"'");
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
            consulta.Fill(dt_usu);
            return dt_usu;
        }

        public DataSet cargar_usuario_rol()
        {
            DataSet dt_usu = new DataSet();
            try
            {
                string sql = ("SELECT * FROM usuario WHERE estado_per = TRUE");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(dt_usu, "usuario");
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return dt_usu;
        }


        public DataTable datosATablaUsuario()
        {
            DataTable dt_usu = new DataTable();
            try
            {
                string sql = ("SELECT * FROM usuario WHERE estado_per = TRUE");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(dt_usu);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return dt_usu;
        }


        public DataTable busquedaPorFiltros(string nombreTabla, string filtro)
        {
            DataTable dt_usu = new DataTable();
            try
            {
                string sql = ("SELECT * FROM usuario WHERE "+nombreTabla+" LIKE '%"+filtro+"%'");
                NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
                consulta.Fill(dt_usu);
            }
            catch (NpgsqlException e)
            {
                MessageBox.Show("" + e);
            }
            return dt_usu;
        }



        //guarda la hora de llegada del usuario
        public void guardar_hora_llegada(string hora,int n)
        {
            
            string sql = ("INSERT INTO horarios(codigo_horario,hora_llegada) VALUES ('" + n + "','"+hora+"')");
            con.EjecutarExecuteNonQuery(sql);
        }

        //guarda el detalle de usuario y hora
        public void guardar_detalle_horario(string cedula, int codigo)
        {
            
            string sql = ("INSERT INTO relacion_usuario_horario(cedula_usu,codigo_horario) VALUES('" + cedula + "','" + codigo + "')");
            con.EjecutarExecuteNonQuery(sql);
        }

       

        //guarda la hora de salida
        public void guardar_hora_salida(string hora, int codigo)
        {
            
            string sql = ("UPDATE horarios SET hora_salida='" + hora + "' WHERE codigo_horario=" + codigo + "");
            con.EjecutarExecuteNonQuery(sql);
        }

        //resta las horas entrada y salida, y devuelve las horas trabajadas
        public string horas_trabajadas(int codigo)
        {
            
            string sql = ("SELECT hora_salida-hora_llegada AS HORASTRABJO FROM horarios WHERE codigo_horario=" + codigo + "");
            string dato = con.EjecutarExecuteQuery(sql);
            return dato;
        }

        public void guardar_hora_extras(string hora, int codigo)
        {
            
            string sql = ("UPDATE horarios SET horas_trabajo='" + hora + "' WHERE codigo_horario=" + codigo + "");
            con.EjecutarExecuteNonQuery(sql);
        }

        //selecciona el sueldo,usuario y tiempo de trabajo de cada empleado
        public DataTable usuarioSueldoHorarioTiempoTrabajado()
        {
            DataTable cargar = new DataTable();
           
            string sql = ("SELECT CONCAT(usuario.nombre_usu,' ',usuario.apellido_usu) as Usuario,usuario.sueldo_usu as SUELDO_MENSUAL,SUM(horarios.horas_trabajo) as TIEMPO_TRABAJADO FROM usuario INNER JOIN(relacion_usuario_horario INNER JOIN horarios ON relacion_usuario_horario.codigo_horario=horarios.codigo_horario) ON usuario.cedula_usu=relacion_usuario_horario.cedula_usu WHERE usuario.tipo_usu <>'Administrador' GROUP BY CONCAT(usuario.nombre_usu,' ',usuario.apellido_usu),usuario.sueldo_usu");
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
            consulta.Fill(cargar);
            return cargar;
        }

                
    

        //suma horas,minutos y segundos de cada usuario
        public DataSet h_m_s(string ced)
        {
            DataSet cargar = new DataSet();
            
            string sql = ("SELECT SUM(EXTRACT(HOUR FROM horas_trabajo))as HORAS, SUM(EXTRACT(MINUTE FROM horas_trabajo))AS MINUTOS, SUM(EXTRACT(SECOND FROM horas_trabajo)) AS SEGUNDOS from horarios,relacion_usuario_horario WHERE horarios.codigo_horario=relacion_usuario_horario.codigo_horario AND relacion_usuario_horario.cedula_usu='" + ced + "'");
            NpgsqlDataAdapter consulta = con.EjecutarExecuteReader1(sql);
            consulta.Fill(cargar,"horas");
            return cargar;
        }

        
        
        public int contar_horarios()
        {
            
            string sql = ("SELECT MAX(codigo_horario) AS CONTAR_HORARIO FROM horarios");
            string dato = con.EjecutarExecuteQuery(sql);
            return int.Parse(dato);
        }

        
    }
}
