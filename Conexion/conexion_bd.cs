﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using Npgsql;

namespace fabisa.Conexion
{
    class conexion_bd
    {
        //OleDbConnection Conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0; Data Source=D:\\farmacia.accdb");
        //OleDbConnection conex = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0; Data Source=D:\\provincias.accdb");
        NpgsqlConnection Conexion = new NpgsqlConnection("Server=localhost;Port=5432;User Id=postgres;Password=admin;Database=GHBD");
        
        /// <summary>
        /// Metodo que sirve para ejecutar sentencias INSERT, UPDATE , DELETE
        /// </summary>
        /// <param name="sql"> recibe una sentencia sql y la ejecuta</param>
        public void EjecutarExecuteNonQuery(String sql)
        {
            try
            {
                Conexion.Open();
                NpgsqlCommand coman = new NpgsqlCommand(sql, Conexion);
                coman.ExecuteNonQuery();
                Conexion.Close();
            }catch(NpgsqlException ex)
            {
                Console.Write(ex.ErrorSql);
                Conexion.Close();
            }
            
        }

        //ExecuteReader: permite ejecutar sentencias query SELECT 
        public void EjecutarExecuteReader(String sql)
        {
            try
            {
                Conexion.Open();
                NpgsqlCommand comam = new NpgsqlCommand(sql, Conexion);
                comam.ExecuteReader();
                Conexion.Close();
            }
            catch (NpgsqlException ex)
            {
                Console.Write(ex.Message);
                Conexion.Close();
            }
        }
        //ExecuteReader: permite ejecutar sentencias query SELECT para leer varios registros
        
        public NpgsqlDataAdapter EjecutarExecuteReader1(String sql)
        {
                Conexion.Open();
                NpgsqlDataAdapter consulta = new NpgsqlDataAdapter(sql, Conexion);
                Conexion.Close();
                return consulta;    
                        
        }

        //EjecutarExecuteQuery: permite ejecutar sentencias query SELECT para hacer una consulta especifica            
        public string EjecutarExecuteQuery(String sql)
        {
            string dato = "";
            Conexion.Open();
            NpgsqlCommand consulta = new NpgsqlCommand(sql, Conexion);
            NpgsqlDataReader dr = consulta.ExecuteReader();
            if (dr.HasRows){
                dr.Read();
                dato = dr[0].ToString();
                Conexion.Close();
                if (dato != "")
                {
                    return dato;
                }
                else { return "0"; }
            }
            else {
                return "0";
            }
        }
    }
}
