﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace fabisa.Reportes
{
    public partial class frm_reportesFiltro : Form
    {
        public frm_reportesFiltro(dsReporte ds)

        {
            InitializeComponent();
            ReporteConFiltros rp = new ReporteConFiltros();
            rp.SetDataSource(ds);
            crystalReporte.ReportSource = rp;
            
        }
    }
}
