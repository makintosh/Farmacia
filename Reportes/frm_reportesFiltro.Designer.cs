﻿namespace fabisa.Reportes
{
    partial class frm_reportesFiltro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_reportesFiltro));
            this.crystalReporte = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crystalReporte
            // 
            this.crystalReporte.ActiveViewIndex = -1;
            this.crystalReporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReporte.DisplayGroupTree = false;
            this.crystalReporte.DisplayStatusBar = false;
            this.crystalReporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReporte.Location = new System.Drawing.Point(0, 0);
            this.crystalReporte.Name = "crystalReporte";
            this.crystalReporte.SelectionFormula = "";
            this.crystalReporte.Size = new System.Drawing.Size(660, 510);
            this.crystalReporte.TabIndex = 0;
            this.crystalReporte.ViewTimeSelectionFormula = "";
            // 
            // frm_reportesFiltro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 510);
            this.Controls.Add(this.crystalReporte);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_reportesFiltro";
            this.Text = "REPORTES POR FECHA";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReporte;
    }
}