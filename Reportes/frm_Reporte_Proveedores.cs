﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fabisa.Reportes
{
    public partial class frm_Reporte_Proveedores : Form
    {
        public frm_Reporte_Proveedores(dsReporte ds)
        {
            InitializeComponent();
            Reportes_Proveedores reporteProveedor = new Reportes_Proveedores();
            reporteProveedor.SetDataSource(ds);
            crystalReportProveedor.ReportSource = reporteProveedor;
        }

    }
}
