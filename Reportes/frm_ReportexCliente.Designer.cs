﻿namespace fabisa.Reportes
{
    partial class frm_ReportexCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ReportexCliente));
            this.crystalReportClie = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crystalReportClie
            // 
            this.crystalReportClie.ActiveViewIndex = -1;
            this.crystalReportClie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportClie.DisplayGroupTree = false;
            this.crystalReportClie.DisplayStatusBar = false;
            this.crystalReportClie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportClie.Location = new System.Drawing.Point(0, 0);
            this.crystalReportClie.Name = "crystalReportClie";
            this.crystalReportClie.SelectionFormula = "";
            this.crystalReportClie.Size = new System.Drawing.Size(755, 470);
            this.crystalReportClie.TabIndex = 0;
            this.crystalReportClie.ViewTimeSelectionFormula = "";
            // 
            // frm_ReportexCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 470);
            this.Controls.Add(this.crystalReportClie);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_ReportexCliente";
            this.Text = "REPORTE POR CLIENTE";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportClie;
    }
}