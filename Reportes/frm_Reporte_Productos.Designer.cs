﻿namespace fabisa.Reportes
{
    partial class frm_Reporte_Productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Reporte_Productos));
            this.crystalReportProducto = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crystalReportProducto
            // 
            this.crystalReportProducto.ActiveViewIndex = -1;
            this.crystalReportProducto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportProducto.DisplayGroupTree = false;
            this.crystalReportProducto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportProducto.Location = new System.Drawing.Point(0, 0);
            this.crystalReportProducto.Name = "crystalReportProducto";
            this.crystalReportProducto.SelectionFormula = "";
            this.crystalReportProducto.Size = new System.Drawing.Size(885, 485);
            this.crystalReportProducto.TabIndex = 0;
            this.crystalReportProducto.ViewTimeSelectionFormula = "";
            // 
            // frm_Reporte_Productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 485);
            this.Controls.Add(this.crystalReportProducto);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Reporte_Productos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "REPORTE DE PRODUCTOS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportProducto;
    }
}