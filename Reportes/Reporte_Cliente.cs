﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fabisa.Reportes
{
    public partial class Reporte_Cliente : Form
    {
        public Reporte_Cliente(dsReporte ds)
        {
            InitializeComponent();
            Reporte_Clientes rp = new Reporte_Clientes();
            rp.SetDataSource(ds);
            this.crystalReportViewer1.ReportSource = rp;
        }
    }
}
