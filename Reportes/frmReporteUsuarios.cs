﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fabisa.Reportes
{
    public partial class frmReporteUsuarios : Form
    {
        public frmReporteUsuarios(dsReporte ds)
        {
            InitializeComponent();
            ReporteUsuarios rpUsuarios = new ReporteUsuarios();
            rpUsuarios.SetDataSource(ds);
            crystalReportUsuario.ReportSource = rpUsuarios;
        }
    }
}
