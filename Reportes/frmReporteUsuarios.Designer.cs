﻿namespace fabisa.Reportes
{
    partial class frmReporteUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReporteUsuarios));
            this.crystalReportUsuario = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crystalReportUsuario
            // 
            this.crystalReportUsuario.ActiveViewIndex = -1;
            this.crystalReportUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportUsuario.DisplayGroupTree = false;
            this.crystalReportUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportUsuario.Location = new System.Drawing.Point(0, 0);
            this.crystalReportUsuario.Name = "crystalReportUsuario";
            this.crystalReportUsuario.SelectionFormula = "";
            this.crystalReportUsuario.Size = new System.Drawing.Size(609, 433);
            this.crystalReportUsuario.TabIndex = 0;
            this.crystalReportUsuario.ViewTimeSelectionFormula = "";
            // 
            // frmReporteUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 433);
            this.Controls.Add(this.crystalReportUsuario);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmReporteUsuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "REPORTE USUARIOS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportUsuario;
    }
}