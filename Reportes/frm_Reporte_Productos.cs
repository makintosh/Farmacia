﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fabisa.Reportes
{
    public partial class frm_Reporte_Productos : Form
    {
        public frm_Reporte_Productos(dsReporte ds)
        {
            InitializeComponent();
            Reporte_Productos rproductos = new Reporte_Productos();
            rproductos.SetDataSource(ds);
            crystalReportProducto.ReportSource = rproductos;
        }
    }
}
