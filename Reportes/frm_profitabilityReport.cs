﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace fabisa.Reportes
{
    public partial class frm_profitabilityReport : Form
    {
        public frm_profitabilityReport(dsReporte ds) {
            InitializeComponent();
            ProfitabilityReport rp = new ProfitabilityReport();
            rp.SetDataSource(ds);
            crystalReporte.ReportSource = rp;
        }
    }
}
