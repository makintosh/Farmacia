﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fabisa.Reportes
{
    public partial class frm_ReportexCliente : Form
    {
        public frm_ReportexCliente(dsReporte ds)
        {
            InitializeComponent();
            ReporteFacturaxCliente frmReportexCliente = new ReporteFacturaxCliente();
            frmReportexCliente.SetDataSource(ds);
            crystalReportClie.ReportSource = frmReportexCliente;
        }
    }
}
