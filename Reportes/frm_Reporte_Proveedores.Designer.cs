﻿namespace fabisa.Reportes
{
    partial class frm_Reporte_Proveedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Reporte_Proveedores));
            this.crystalReportProveedor = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crystalReportProveedor
            // 
            this.crystalReportProveedor.ActiveViewIndex = -1;
            this.crystalReportProveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportProveedor.DisplayGroupTree = false;
            this.crystalReportProveedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportProveedor.Location = new System.Drawing.Point(0, 0);
            this.crystalReportProveedor.Name = "crystalReportProveedor";
            this.crystalReportProveedor.SelectionFormula = "";
            this.crystalReportProveedor.Size = new System.Drawing.Size(674, 495);
            this.crystalReportProveedor.TabIndex = 0;
            this.crystalReportProveedor.ViewTimeSelectionFormula = "";
            // 
            // frm_Reporte_Proveedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 495);
            this.Controls.Add(this.crystalReportProveedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Reporte_Proveedores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "REPORTE DE PROVEEDORES";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportProveedor;
    }
}