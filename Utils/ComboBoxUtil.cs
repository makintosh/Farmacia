﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace fabisa.Utils
{
    class ComboBoxUtil {

        /// <summary>
        /// Adds a new row to a DataTable
        /// </summary>
        /// <param name="data"></param>
        public static void addNewRowToDataTableAtZero(DataTable data)
        {
            DataRow newRow = data.NewRow();
            newRow[0] = "--Seleccione--";
            newRow[1] = "-1";
            data.Rows.InsertAt(newRow, 0);
        } 
    }
}
