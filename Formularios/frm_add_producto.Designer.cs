﻿namespace fabisa.Formularios
{
    partial class frm_add_producto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_add_producto));
            this.btguardarycerrar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.gbproducto = new System.Windows.Forms.GroupBox();
            this.textBoxCodPer = new System.Windows.Forms.TextBox();
            this.comboBoxMarca = new System.Windows.Forms.ComboBox();
            this.comboBoxTalla = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxTipoProducto = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtpreciocompra = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.check = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdescripcion = new System.Windows.Forms.TextBox();
            this.txtPrecioVenta = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gbproducto.SuspendLayout();
            this.SuspendLayout();
            // 
            // btguardarycerrar
            // 
            this.btguardarycerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btguardarycerrar.Image = ((System.Drawing.Image)(resources.GetObject("btguardarycerrar.Image")));
            this.btguardarycerrar.Location = new System.Drawing.Point(184, 319);
            this.btguardarycerrar.Name = "btguardarycerrar";
            this.btguardarycerrar.Size = new System.Drawing.Size(144, 54);
            this.btguardarycerrar.TabIndex = 12;
            this.btguardarycerrar.Text = "Guardar";
            this.btguardarycerrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btguardarycerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btguardarycerrar, "Guardar");
            this.btguardarycerrar.UseVisualStyleBackColor = true;
            this.btguardarycerrar.Click += new System.EventHandler(this.btguardarycerrar_Click);
            // 
            // gbproducto
            // 
            this.gbproducto.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gbproducto.Controls.Add(this.textBoxCodPer);
            this.gbproducto.Controls.Add(this.comboBoxMarca);
            this.gbproducto.Controls.Add(this.comboBoxTalla);
            this.gbproducto.Controls.Add(this.label9);
            this.gbproducto.Controls.Add(this.comboBoxTipoProducto);
            this.gbproducto.Controls.Add(this.label6);
            this.gbproducto.Controls.Add(this.txtpreciocompra);
            this.gbproducto.Controls.Add(this.label8);
            this.gbproducto.Controls.Add(this.check);
            this.gbproducto.Controls.Add(this.label7);
            this.gbproducto.Controls.Add(this.label1);
            this.gbproducto.Controls.Add(this.label2);
            this.gbproducto.Controls.Add(this.txtdescripcion);
            this.gbproducto.Controls.Add(this.txtPrecioVenta);
            this.gbproducto.Controls.Add(this.label3);
            this.gbproducto.Controls.Add(this.label5);
            this.gbproducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbproducto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbproducto.Location = new System.Drawing.Point(21, 31);
            this.gbproducto.Name = "gbproducto";
            this.gbproducto.Size = new System.Drawing.Size(462, 272);
            this.gbproducto.TabIndex = 13;
            this.gbproducto.TabStop = false;
            this.gbproducto.Text = "Producto";
            // 
            // textBoxCodPer
            // 
            this.textBoxCodPer.Location = new System.Drawing.Point(154, 28);
            this.textBoxCodPer.MaxLength = 10;
            this.textBoxCodPer.Name = "textBoxCodPer";
            this.textBoxCodPer.Size = new System.Drawing.Size(100, 22);
            this.textBoxCodPer.TabIndex = 7;
            // 
            // comboBoxMarca
            // 
            this.comboBoxMarca.FormattingEnabled = true;
            this.comboBoxMarca.Location = new System.Drawing.Point(154, 90);
            this.comboBoxMarca.Name = "comboBoxMarca";
            this.comboBoxMarca.Size = new System.Drawing.Size(229, 24);
            this.comboBoxMarca.TabIndex = 9;
            // 
            // comboBoxTalla
            // 
            this.comboBoxTalla.FormattingEnabled = true;
            this.comboBoxTalla.Location = new System.Drawing.Point(154, 152);
            this.comboBoxTalla.Name = "comboBoxTalla";
            this.comboBoxTalla.Size = new System.Drawing.Size(229, 24);
            this.comboBoxTalla.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 160);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "Unidad de medida:";
            // 
            // comboBoxTipoProducto
            // 
            this.comboBoxTipoProducto.FormattingEnabled = true;
            this.comboBoxTipoProducto.Location = new System.Drawing.Point(154, 58);
            this.comboBoxTipoProducto.Name = "comboBoxTipoProducto";
            this.comboBoxTipoProducto.Size = new System.Drawing.Size(229, 24);
            this.comboBoxTipoProducto.TabIndex = 8;
            this.comboBoxTipoProducto.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTipoProducto_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 16);
            this.label6.TabIndex = 17;
            this.label6.Text = "Tipo de Mercancia :";
            // 
            // txtpreciocompra
            // 
            this.txtpreciocompra.Location = new System.Drawing.Point(154, 184);
            this.txtpreciocompra.Name = "txtpreciocompra";
            this.txtpreciocompra.Size = new System.Drawing.Size(73, 22);
            this.txtpreciocompra.TabIndex = 13;
            this.txtpreciocompra.Text = "0.00";
            this.txtpreciocompra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpreciocompra_KeyPress_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 16;
            this.label8.Text = "Precio de Compra:";
            // 
            // check
            // 
            this.check.AutoSize = true;
            this.check.Location = new System.Drawing.Point(154, 244);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(15, 14);
            this.check.TabIndex = 15;
            this.check.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 247);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Iva:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Código :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Descripcion :";
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtdescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescripcion.Location = new System.Drawing.Point(154, 122);
            this.txtdescripcion.MaxLength = 50;
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Size = new System.Drawing.Size(229, 22);
            this.txtdescripcion.TabIndex = 10;
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPrecioVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioVenta.Location = new System.Drawing.Point(154, 214);
            this.txtPrecioVenta.MaxLength = 10;
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.Size = new System.Drawing.Size(73, 22);
            this.txtPrecioVenta.TabIndex = 14;
            this.txtPrecioVenta.Text = "0.00";
            this.txtPrecioVenta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioVenta_KeyPress_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Marca :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Precio de Venta :";
            // 
            // frm_add_producto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(504, 386);
            this.Controls.Add(this.gbproducto);
            this.Controls.Add(this.btguardarycerrar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_add_producto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PRODUCTO";
            this.Load += new System.EventHandler(this.frm_add_producto_Load);
            this.gbproducto.ResumeLayout(false);
            this.gbproducto.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btguardarycerrar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox gbproducto;
        private System.Windows.Forms.TextBox textBoxCodPer;
        private System.Windows.Forms.ComboBox comboBoxMarca;
        private System.Windows.Forms.ComboBox comboBoxTalla;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxTipoProducto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtpreciocompra;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox check;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtdescripcion;
        private System.Windows.Forms.TextBox txtPrecioVenta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
    }
}