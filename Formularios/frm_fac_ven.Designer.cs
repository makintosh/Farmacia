﻿namespace fabisa.Formularios
{
    partial class frm_fac_ven
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_fac_ven));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.txtnumerofac = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtcedula = new System.Windows.Forms.TextBox();
            this.txtcliente = new System.Windows.Forms.TextBox();
            this.txttel = new System.Windows.Forms.TextBox();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.btbuscar_cli = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btbuscar_pro = new System.Windows.Forms.Button();
            this.txtproducto = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tablaproducto = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtfecha = new System.Windows.Forms.DateTimePicker();
            this.btfactura = new System.Windows.Forms.Button();
            this.btnuevfac = new System.Windows.Forms.Button();
            this.btborrar = new System.Windows.Forms.Button();
            this.btverfacturas = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btreporte = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.txtiva = new System.Windows.Forms.TextBox();
            this.txtsubtotal = new System.Windows.Forms.TextBox();
            this.txtivacero = new System.Windows.Forms.TextBox();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.gpvalores = new System.Windows.Forms.GroupBox();
            this.textboxdes = new System.Windows.Forms.TextBox();
            this.txtdescuento = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.toolTipFactura = new System.Windows.Forms.ToolTip(this.components);
            this.btnAnular = new System.Windows.Forms.Button();
            this.lbAnular = new System.Windows.Forms.Label();
            this.txtitems = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSearchInvoice = new System.Windows.Forms.Button();
            this.txtSearchInvoice = new System.Windows.Forms.TextBox();
            this.comboBoxEmployee = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablaproducto)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.gpvalores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescuento)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "N° :";
            // 
            // txtnumerofac
            // 
            this.txtnumerofac.Location = new System.Drawing.Point(67, 13);
            this.txtnumerofac.Name = "txtnumerofac";
            this.txtnumerofac.Size = new System.Drawing.Size(119, 22);
            this.txtnumerofac.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(259, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Cliente:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Cédula :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Telefono :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(259, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Direccion :";
            // 
            // txtcedula
            // 
            this.txtcedula.Location = new System.Drawing.Point(101, 19);
            this.txtcedula.MaxLength = 13;
            this.txtcedula.Name = "txtcedula";
            this.txtcedula.Size = new System.Drawing.Size(131, 22);
            this.txtcedula.TabIndex = 7;
            this.txtcedula.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter_txtcedcliente);
            this.txtcedula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcedula_KeyPress);
            // 
            // txtcliente
            // 
            this.txtcliente.Location = new System.Drawing.Point(327, 19);
            this.txtcliente.Name = "txtcliente";
            this.txtcliente.Size = new System.Drawing.Size(227, 22);
            this.txtcliente.TabIndex = 8;
            // 
            // txttel
            // 
            this.txttel.Location = new System.Drawing.Point(101, 62);
            this.txttel.Name = "txttel";
            this.txttel.Size = new System.Drawing.Size(131, 22);
            this.txttel.TabIndex = 9;
            this.txttel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttel_KeyPress);
            // 
            // txtdireccion
            // 
            this.txtdireccion.Location = new System.Drawing.Point(327, 62);
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(227, 22);
            this.txtdireccion.TabIndex = 10;
            // 
            // btbuscar_cli
            // 
            this.btbuscar_cli.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar_cli.Image")));
            this.btbuscar_cli.Location = new System.Drawing.Point(618, 21);
            this.btbuscar_cli.Name = "btbuscar_cli";
            this.btbuscar_cli.Size = new System.Drawing.Size(56, 51);
            this.btbuscar_cli.TabIndex = 11;
            this.btbuscar_cli.Tag = "";
            this.btbuscar_cli.Text = ".....";
            this.toolTipFactura.SetToolTip(this.btbuscar_cli, "Buscar Cliente");
            this.btbuscar_cli.UseVisualStyleBackColor = true;
            this.btbuscar_cli.Click += new System.EventHandler(this.btbuscar_cli_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Fecha :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btbuscar_cli);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtdireccion);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txttel);
            this.groupBox1.Controls.Add(this.txtcedula);
            this.groupBox1.Controls.Add(this.txtcliente);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Location = new System.Drawing.Point(31, 110);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(687, 103);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cliente";
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(560, 21);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(52, 51);
            this.btnAdd.TabIndex = 12;
            this.toolTipFactura.SetToolTip(this.btnAdd, "Añadir Nuevo Cliente");
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.groupBox2.Controls.Add(this.btbuscar_pro);
            this.groupBox2.Controls.Add(this.txtproducto);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox2.Location = new System.Drawing.Point(31, 223);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(687, 84);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Producto";
            // 
            // btbuscar_pro
            // 
            this.btbuscar_pro.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar_pro.Image")));
            this.btbuscar_pro.Location = new System.Drawing.Point(618, 21);
            this.btbuscar_pro.Name = "btbuscar_pro";
            this.btbuscar_pro.Size = new System.Drawing.Size(56, 51);
            this.btbuscar_pro.TabIndex = 12;
            this.btbuscar_pro.Text = ".....";
            this.toolTipFactura.SetToolTip(this.btbuscar_pro, "Buscar Producto");
            this.btbuscar_pro.UseVisualStyleBackColor = true;
            this.btbuscar_pro.Click += new System.EventHandler(this.btbuscar_pro_Click);
            // 
            // txtproducto
            // 
            this.txtproducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproducto.Location = new System.Drawing.Point(101, 32);
            this.txtproducto.MaxLength = 150;
            this.txtproducto.Name = "txtproducto";
            this.txtproducto.Size = new System.Drawing.Size(453, 22);
            this.txtproducto.TabIndex = 3;
            this.txtproducto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter_txtproducto);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Producto :";
            // 
            // tablaproducto
            // 
            this.tablaproducto.AllowUserToAddRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.tablaproducto.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.tablaproducto.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tablaproducto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tablaproducto.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablaproducto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.tablaproducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tablaproducto.DefaultCellStyle = dataGridViewCellStyle13;
            this.tablaproducto.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tablaproducto.Location = new System.Drawing.Point(97, 334);
            this.tablaproducto.Name = "tablaproducto";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablaproducto.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.tablaproducto.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.tablaproducto.Size = new System.Drawing.Size(621, 150);
            this.tablaproducto.TabIndex = 16;
            this.tablaproducto.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.tablaproducto_CellEndEdit);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.groupBox3.Controls.Add(this.dtfecha);
            this.groupBox3.Controls.Add(this.txtnumerofac);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox3.Location = new System.Drawing.Point(428, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 76);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // dtfecha
            // 
            this.dtfecha.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtfecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtfecha.Location = new System.Drawing.Point(65, 43);
            this.dtfecha.Name = "dtfecha";
            this.dtfecha.Size = new System.Drawing.Size(215, 20);
            this.dtfecha.TabIndex = 13;
            // 
            // btfactura
            // 
            this.btfactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btfactura.Image = ((System.Drawing.Image)(resources.GetObject("btfactura.Image")));
            this.btfactura.Location = new System.Drawing.Point(31, 389);
            this.btfactura.Name = "btfactura";
            this.btfactura.Size = new System.Drawing.Size(62, 50);
            this.btfactura.TabIndex = 26;
            this.toolTipFactura.SetToolTip(this.btfactura, "Guardar");
            this.btfactura.UseVisualStyleBackColor = true;
            this.btfactura.Click += new System.EventHandler(this.btfactura_Click);
            // 
            // btnuevfac
            // 
            this.btnuevfac.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnuevfac.Image = ((System.Drawing.Image)(resources.GetObject("btnuevfac.Image")));
            this.btnuevfac.Location = new System.Drawing.Point(97, 523);
            this.btnuevfac.Name = "btnuevfac";
            this.btnuevfac.Size = new System.Drawing.Size(55, 57);
            this.btnuevfac.TabIndex = 27;
            this.toolTipFactura.SetToolTip(this.btnuevfac, "Nueva Factura");
            this.btnuevfac.UseVisualStyleBackColor = true;
            this.btnuevfac.Click += new System.EventHandler(this.btnuevfac_Click);
            // 
            // btborrar
            // 
            this.btborrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btborrar.Image = ((System.Drawing.Image)(resources.GetObject("btborrar.Image")));
            this.btborrar.Location = new System.Drawing.Point(31, 334);
            this.btborrar.Name = "btborrar";
            this.btborrar.Size = new System.Drawing.Size(62, 49);
            this.btborrar.TabIndex = 30;
            this.toolTipFactura.SetToolTip(this.btborrar, "Borrar");
            this.btborrar.UseVisualStyleBackColor = true;
            this.btborrar.Click += new System.EventHandler(this.btborrar_Click);
            // 
            // btverfacturas
            // 
            this.btverfacturas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btverfacturas.Image = ((System.Drawing.Image)(resources.GetObject("btverfacturas.Image")));
            this.btverfacturas.Location = new System.Drawing.Point(158, 523);
            this.btverfacturas.Name = "btverfacturas";
            this.btverfacturas.Size = new System.Drawing.Size(57, 57);
            this.btverfacturas.TabIndex = 31;
            this.toolTipFactura.SetToolTip(this.btverfacturas, "Ver Facturas");
            this.btverfacturas.UseVisualStyleBackColor = true;
            this.btverfacturas.Click += new System.EventHandler(this.btverfacturas_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(90, 705);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 13);
            this.label18.TabIndex = 42;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(143, 705);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 13);
            this.label20.TabIndex = 44;
            // 
            // btreporte
            // 
            this.btreporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btreporte.Image = ((System.Drawing.Image)(resources.GetObject("btreporte.Image")));
            this.btreporte.Location = new System.Drawing.Point(291, 523);
            this.btreporte.Name = "btreporte";
            this.btreporte.Size = new System.Drawing.Size(61, 57);
            this.btreporte.TabIndex = 45;
            this.toolTipFactura.SetToolTip(this.btreporte, "Reporte");
            this.btreporte.UseVisualStyleBackColor = true;
            this.btreporte.Click += new System.EventHandler(this.btreporte_Click);
            // 
            // btcancelar
            // 
            this.btcancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(31, 445);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(62, 48);
            this.btcancelar.TabIndex = 46;
            this.toolTipFactura.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // txtiva
            // 
            this.txtiva.Location = new System.Drawing.Point(94, 74);
            this.txtiva.Name = "txtiva";
            this.txtiva.Size = new System.Drawing.Size(100, 22);
            this.txtiva.TabIndex = 49;
            this.txtiva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtsubtotal
            // 
            this.txtsubtotal.Location = new System.Drawing.Point(94, 18);
            this.txtsubtotal.Name = "txtsubtotal";
            this.txtsubtotal.Size = new System.Drawing.Size(100, 22);
            this.txtsubtotal.TabIndex = 50;
            this.txtsubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtivacero
            // 
            this.txtivacero.Location = new System.Drawing.Point(94, 101);
            this.txtivacero.Name = "txtivacero";
            this.txtivacero.Size = new System.Drawing.Size(100, 22);
            this.txtivacero.TabIndex = 51;
            this.txtivacero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttotal
            // 
            this.txttotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotal.Location = new System.Drawing.Point(94, 128);
            this.txttotal.Multiline = true;
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(100, 23);
            this.txttotal.TabIndex = 52;
            this.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 53;
            this.label1.Text = "Subtotal:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 16);
            this.label11.TabIndex = 54;
            this.label11.Text = "Iva 12%:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 109);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 16);
            this.label12.TabIndex = 55;
            this.label12.Text = "Iva 0%:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 16);
            this.label13.TabIndex = 56;
            this.label13.Text = "Total:";
            // 
            // gpvalores
            // 
            this.gpvalores.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gpvalores.Controls.Add(this.textboxdes);
            this.gpvalores.Controls.Add(this.txtdescuento);
            this.gpvalores.Controls.Add(this.label14);
            this.gpvalores.Controls.Add(this.label1);
            this.gpvalores.Controls.Add(this.label13);
            this.gpvalores.Controls.Add(this.txtiva);
            this.gpvalores.Controls.Add(this.label12);
            this.gpvalores.Controls.Add(this.txtsubtotal);
            this.gpvalores.Controls.Add(this.label11);
            this.gpvalores.Controls.Add(this.txtivacero);
            this.gpvalores.Controls.Add(this.txttotal);
            this.gpvalores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpvalores.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gpvalores.Location = new System.Drawing.Point(521, 490);
            this.gpvalores.Name = "gpvalores";
            this.gpvalores.Size = new System.Drawing.Size(197, 161);
            this.gpvalores.TabIndex = 57;
            this.gpvalores.TabStop = false;
            // 
            // textboxdes
            // 
            this.textboxdes.Location = new System.Drawing.Point(143, 46);
            this.textboxdes.Name = "textboxdes";
            this.textboxdes.Size = new System.Drawing.Size(51, 22);
            this.textboxdes.TabIndex = 62;
            this.textboxdes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textboxdes.Visible = false;
            // 
            // txtdescuento
            // 
            this.txtdescuento.Location = new System.Drawing.Point(94, 46);
            this.txtdescuento.Name = "txtdescuento";
            this.txtdescuento.Size = new System.Drawing.Size(43, 22);
            this.txtdescuento.TabIndex = 61;
            this.txtdescuento.ValueChanged += new System.EventHandler(this.txtdescuento_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(2, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 16);
            this.label14.TabIndex = 61;
            this.label14.Text = "Descuento %:";
            // 
            // btnAnular
            // 
            this.btnAnular.Image = ((System.Drawing.Image)(resources.GetObject("btnAnular.Image")));
            this.btnAnular.Location = new System.Drawing.Point(221, 523);
            this.btnAnular.Name = "btnAnular";
            this.btnAnular.Size = new System.Drawing.Size(63, 57);
            this.btnAnular.TabIndex = 59;
            this.btnAnular.Text = "ANULAR";
            this.btnAnular.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTipFactura.SetToolTip(this.btnAnular, "Anular Factura");
            this.btnAnular.UseVisualStyleBackColor = true;
            this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
            // 
            // lbAnular
            // 
            this.lbAnular.AutoSize = true;
            this.lbAnular.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnular.ForeColor = System.Drawing.Color.Red;
            this.lbAnular.Location = new System.Drawing.Point(28, 611);
            this.lbAnular.Name = "lbAnular";
            this.lbAnular.Size = new System.Drawing.Size(0, 24);
            this.lbAnular.TabIndex = 60;
            // 
            // txtitems
            // 
            this.txtitems.Location = new System.Drawing.Point(135, 496);
            this.txtitems.Name = "txtitems";
            this.txtitems.Size = new System.Drawing.Size(48, 20);
            this.txtitems.TabIndex = 62;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(94, 501);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 61;
            this.label8.Text = "Items:";
            // 
            // btnSearchInvoice
            // 
            this.btnSearchInvoice.Location = new System.Drawing.Point(164, 22);
            this.btnSearchInvoice.Name = "btnSearchInvoice";
            this.btnSearchInvoice.Size = new System.Drawing.Size(75, 23);
            this.btnSearchInvoice.TabIndex = 63;
            this.btnSearchInvoice.Text = "Buscar";
            this.btnSearchInvoice.UseVisualStyleBackColor = true;
            this.btnSearchInvoice.Click += new System.EventHandler(this.btnSearchInvoice_Click);
            // 
            // txtSearchInvoice
            // 
            this.txtSearchInvoice.Location = new System.Drawing.Point(32, 25);
            this.txtSearchInvoice.Name = "txtSearchInvoice";
            this.txtSearchInvoice.Size = new System.Drawing.Size(126, 20);
            this.txtSearchInvoice.TabIndex = 64;
            // 
            // comboBoxEmployee
            // 
            this.comboBoxEmployee.FormattingEnabled = true;
            this.comboBoxEmployee.Location = new System.Drawing.Point(32, 74);
            this.comboBoxEmployee.Name = "comboBoxEmployee";
            this.comboBoxEmployee.Size = new System.Drawing.Size(219, 21);
            this.comboBoxEmployee.TabIndex = 65;
            this.comboBoxEmployee.SelectionChangeCommitted += new System.EventHandler(this.comboBoxEmployee_SelectionChangeCommitted);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(29, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 14;
            this.label10.Text = "Vendedor:";
            // 
            // frm_fac_ven
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(746, 654);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.comboBoxEmployee);
            this.Controls.Add(this.txtSearchInvoice);
            this.Controls.Add(this.btnSearchInvoice);
            this.Controls.Add(this.txtitems);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbAnular);
            this.Controls.Add(this.btnAnular);
            this.Controls.Add(this.gpvalores);
            this.Controls.Add(this.btcancelar);
            this.Controls.Add(this.btreporte);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.btverfacturas);
            this.Controls.Add(this.btborrar);
            this.Controls.Add(this.btnuevfac);
            this.Controls.Add(this.btfactura);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.tablaproducto);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_fac_ven";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FACTURA VENTA";
            this.Load += new System.EventHandler(this.facturaventa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablaproducto)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gpvalores.ResumeLayout(false);
            this.gpvalores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescuento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnumerofac;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtcedula;
        private System.Windows.Forms.TextBox txtcliente;
        private System.Windows.Forms.TextBox txttel;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.Button btbuscar_cli;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtproducto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btbuscar_pro;
        private System.Windows.Forms.DataGridView tablaproducto;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btfactura;
        private System.Windows.Forms.Button btnuevfac;
        private System.Windows.Forms.Button btborrar;
        private System.Windows.Forms.Button btverfacturas;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btreporte;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.DateTimePicker dtfecha;
        private System.Windows.Forms.TextBox txtiva;
        private System.Windows.Forms.TextBox txtsubtotal;
        private System.Windows.Forms.TextBox txtivacero;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox gpvalores;
        private System.Windows.Forms.ToolTip toolTipFactura;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnAnular;
        private System.Windows.Forms.Label lbAnular;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown txtdescuento;
        private System.Windows.Forms.TextBox textboxdes;
        private System.Windows.Forms.TextBox txtitems;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSearchInvoice;
        private System.Windows.Forms.TextBox txtSearchInvoice;
        private System.Windows.Forms.ComboBox comboBoxEmployee;
        private System.Windows.Forms.Label label10;
    }
}