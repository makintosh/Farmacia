﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Reportes;
using fabisa.Formularios;

namespace fabisa.Formularios
{
    public partial class frm_principal : Form
    {
        usuario obj_usu = new usuario();
        
        
        public frm_principal(usuario refUsuario)
        {
            InitializeComponent();
            obj_usu.setCargo(refUsuario.getCargo());
            obj_usu.setNombre(refUsuario.getNombre());
            obj_usu.setApellido(refUsuario.getApellido());
            obj_usu.setCedula(refUsuario.getCedula());
            obj_usu.setCodigo_hora(refUsuario.getCodigo_hora());
            labelUsuario.Text = obj_usu.getNombre() + " " + obj_usu.getApellido();
        }  

        
        private void principal_Load(object sender, EventArgs e)
        {
                        
            if (obj_usu.getCargo().Equals("USUARIO"))
            {
                usuarioToolStripMenuItem.Enabled = false;
            }
            else 
            {
            }
        }     
           
             
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Application.Exit();           
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_cliente);
            if (frm != null)
            {
                // si la instacia ya exise la trae al primer plano
                frm.BringToFront();
                return;
            }
            frm_cliente frm_gestioncliente = new frm_cliente();
            frm_gestioncliente.Show();  
        }

        private void proveedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_proveedor);
            if (frm != null)
            {
                // si la instacia ya exise la trae al primer plano
                frm.BringToFront();
                return;
            }
            frm_proveedor frm_prov = new frm_proveedor();
            frm_prov.Show();
        }

        private void productoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_producto);
            if (frm != null)
            {
                // si la instacia ya exise la trae al primer plano
                frm.BringToFront();
                return;
            }
            frm_producto frm_producto = new frm_producto(obj_usu.getCargo());           
            frm_producto.Show();

        }

        private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_usuario);
            if (frm != null)
            {
                // si la instacia ya exise la trae al primer plano
                frm.BringToFront();
                return;
            }
            
            frm_usuario rol = new frm_usuario();
            rol.Show();

        }

        private void ventaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_fac_ven);
            if (frm != null)
            {
                // si la instacia ya exise la trae al primer plano
                frm.BringToFront();
                return;
            }
            frm_fac_ven fac = new frm_fac_ven(obj_usu.getCedula());
            fac.Show();
        }

        private void compraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_fac_com);
            if (frm != null)
            {
                // si la instacia ya exise la trae al primer plano
                frm.BringToFront();
                return;
            }
            frm_fac_com frmcompra = new frm_fac_com(obj_usu);
            frmcompra.Show();

        }

        private void acercaDeSISGENToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_AcercaDe frmAce = new frm_AcercaDe();
            frmAce.ShowDialog();
        }


        private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_buscar_producto);
            if (frm != null)
            {
                // si la instacia ya exise la trae al primer plano
                frm.BringToFront();
                return;
            }
            frm_buscar_producto frmBuscarProducto = new frm_buscar_producto(true);
            frmBuscarProducto.Show();
        }

        private void frm_principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void rentabilidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is frm_profitability_report);
            if (frm != null)
            {
                frm.BringToFront();
                return;
            }
            frm_profitability_report frmProfitabilityReport = new frm_profitability_report();
            frmProfitabilityReport.Show();
        }    

       
    }
}
