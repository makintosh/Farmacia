﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Formularios;

namespace fabisa.Formularios
{
    public partial class frm_fac_com : Form
    {
        public frm_fac_com(usuario user)
        {
            InitializeComponent();
            obj_usu = user;

        }

        // instancias de clases....

        fac_compra obj_facc = new fac_compra();
        producto obj_pro = new producto();        
        usuario obj_usu = new usuario();
        
        //
        DataTable dt_ruc = new DataTable();
        DataTable dt_producto = new DataTable();

        DataSet dtFacturaCompra = new DataSet();
        DataSet dt_prov = new DataSet();
        DataSet dt_pro = new DataSet();


        int contadorFactura = 0;
        int totalFacturas = 0;


        private void facturacompra_Load(object sender, EventArgs e)
        {
            dt_ruc = obj_facc.autocompletar_ruc();
            autocompletar_txtruc();
            dt_producto = obj_facc.autocompletar_producto_facturacompra();
            autocompletar_txtproducto();
            habilitar_botones_accion(false);
            btnuevfac.Enabled = true;
            btborrar.Enabled = false;
            btfactura.Enabled = false;
            btborrar.Enabled = false;
            btbuscar_pro.Enabled = false;
            btbuscar_prv.Enabled = false;
            btcancelar.Enabled = false;
            btnAddProveedor.Enabled = false;
            btnAddProducto.Enabled = false;
            btverfacturas.Enabled = true;
            cajas_solo_lectura(true);
        }

        //BUSCAR PROVEEDOR
        private void btbuscar_prv_Click(object sender, EventArgs e)
        {
            frm_buscar_proveedor busprv = new frm_buscar_proveedor();
            DialogResult resultado = busprv.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                txtruc.Text = busprv.getProveedor().getRuc();
                txtrazon.Text = busprv.getProveedor().getRazon();                
                txtdireccion.Text = busprv.getProveedor().getDireccion();
                txttel.Text = busprv.getProveedor().getTelefono();
                txtproducto.Focus();
            }
        }

        //BUSCAR PRODUCTO
        private void btbuscar_pro_Click(object sender, EventArgs e)
        {
            frm_buscar_producto buspro = new frm_buscar_producto(true);
            DialogResult resultado = buspro.ShowDialog();
            if (resultado == DialogResult.OK)
            {               
                
                obj_pro.setCodigo(buspro.getProducto().getCodigo()); // codigo
                obj_pro.setCodigoIdentificador(buspro.getProducto().getCodigoIdentificador());

                obj_pro.setDescripcion(buspro.getProducto().getDescripcion()+" "+buspro.getProducto().getTalla());
                obj_pro.setPrecioCompra( buspro.getProducto().getPrecioCompra());
                obj_pro.setIva(buspro.getProducto().getIva());
                enviarProductoAGrilla();
                dt_producto = obj_facc.autocompletar_producto_facturacompra();    
            }
            
        }




        int flag_factura = 0;


        private fac_compra facturacompra()
        {
            
            obj_facc.setFechaFactura(dtfecha.Value);
            obj_facc.setIvaCero(Convert.ToSingle(txtivacero.Text));
            obj_facc.setIva(Convert.ToSingle(txtiva.Text));
            obj_facc.setNumeroFactura( Convert.ToInt32(txtnumerofac.Text));
            obj_facc.setSubtotal(Convert.ToSingle(txtsubtotal.Text));
            obj_facc.setTotalFactura(Convert.ToSingle(txttotal.Text));
            obj_facc.setDescuento_fac(float.Parse(txtdescuento.Text));
            obj_facc.setNumeroFacturaReal(txtFacturaReal.Text);
            return obj_facc;
        }

        /// <summary>
        /// Persist invoice process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btfactura_Click(object sender, EventArgs e)
        {
            if (txtruc.Text.Length > 0 && txtsubtotal.Text.Length > 0)
            {

                for (int i = 0; i < tablaproducto.Rows.Count; i++)
                {
                    if (tablaproducto.Rows[i].Cells["cantidad"].Value == null)//controla si no hay cantidad en los productos
                    {
                        MessageBox.Show("Por favor ingresa la cantidad en el producto " + tablaproducto.Rows[i].Cells[1].Value.ToString());
                        tablaproducto.Focus();
                        tablaproducto.CurrentCell = tablaproducto["cantidad", i];
                        flag_factura = 1;
                        i = tablaproducto.Rows.Count;
                    }
                }

                if (flag_factura == 0)
                {
                     //guardar en tabla factura
                    obj_facc.guardar_factura_compra(facturacompra(),txtruc.Text, obj_usu.getCedula(),dtfecha.Value);
                    detalleFactura obj_detalle_factura = new detalleFactura();
                    //controlar el stock de cada producto y el detalle de cada factura /////
                    for (int t = 0; t < tablaproducto.Rows.Count; t++)
                    {
                        obj_pro.setCodigo(Convert.ToInt16(tablaproducto.Rows[t].Cells["codigo"].Value)); //codigo                        
                        obj_detalle_factura.setCantidad(Convert.ToInt16(tablaproducto.Rows[t].Cells["cantidad"].Value)); // quantity
                        obj_detalle_factura.setCurrentPrice(double.Parse(tablaproducto.Rows[t].Cells["precio"].Value.ToString())); // product price
                        obj_detalle_factura.setTotal(double.Parse(tablaproducto.Rows[t].Cells["total"].Value.ToString())); // total
                        obj_facc.guardar_detalle_factura_compra(obj_detalle_factura.getTotal(),  
                            obj_pro.getCodigo(), 
                            obj_detalle_factura.getCantidad(), 
                            obj_facc.getNumeroFactura(), 
                            obj_detalle_factura.getCurrentPrice()); // persist in detalle_factura_compra
                        obj_pro.actualizarPrecioCompra(obj_detalle_factura.getCurrentPrice(), 
                            obj_pro.getCodigo()); // updates product's purchase price
                    }
                    tablaproducto.ReadOnly = true;
                    dtFacturaCompra = obj_facc.cargar_factura_compra();
                    totalFacturas = dtFacturaCompra.Tables["factura_compra"].Rows.Count;
                    contadorFactura = totalFacturas - 1;                    
                    habilitar_botones_accion(true);
                    cajas_solo_lectura(true);
                }

            }
            else {
                MessageBox.Show("Verificar que todos los campos esten llenos",
                    "Validación",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

        }

        /////////

        private void habilitar_botones_accion(bool t)
        {
            btnuevfac.Enabled = t;
            btverfacturas.Enabled = t;
            btAnular.Enabled = t;
            btfactura.Enabled = !t;
            btborrar.Enabled = !t;
            btcancelar.Enabled = !t;
            btbuscar_pro.Enabled = !t;
            btnAddProducto.Enabled = !t;
            btnAddProveedor.Enabled = !t;
            btbuscar_prv.Enabled = !t;
        }

        private void cajas_solo_lectura(bool t)
        {
            txtnumerofac.ReadOnly = t;
            txtFacturaReal.ReadOnly = t;
            dtfecha.Enabled = !t;
            txtitems.ReadOnly = t;
            txtrazon.ReadOnly = t;
            txtruc.ReadOnly = t;
            txtdireccion.ReadOnly = t;
            txttel.ReadOnly = t;
               
            txtproducto.ReadOnly = t;
            txtsubtotal.ReadOnly = t;
            txtiva.ReadOnly = t;
            txtivacero.ReadOnly = t;
            txtdescuento.ReadOnly = t;
            txttotal.ReadOnly = t;
        }



        //BOTON BORRAR
        private void btborrar_Click(object sender, EventArgs e)
        {
            if (tablaproducto.RowCount != 1)
            {
                tablaproducto.Rows.Remove(tablaproducto.CurrentRow);                
                suma();
                
            }

            else 
            {
                btborrar.Enabled = false;
                btfactura.Enabled = false;
                tablaproducto.Rows.Remove(tablaproducto.CurrentRow);
                suma();
                txtproducto.Focus();
                //MessageBox.Show("NO HAY PRODUCTOS EN LISTA", "VERIFICA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            txtitems.Text = tablaproducto.Rows.Count.ToString();
        }



        //LIMPIAR CAJAS
        public void limpiar_cajas_factura_compra()
        {

            txtruc.Clear();
            txtrazon.Clear();
            txtdireccion.Clear();            
            txttel.Clear();
            //txtcodp.Clear();
            txtproducto.Clear();
            //txtprecio.Clear();
            txtsubtotal.Clear();
            txtivacero.Clear();
            txttotal.Clear();
            txtiva.Clear();
            txtFacturaReal.Clear();
            txtdescuento.Clear();
            txtitems.Clear();

        }


        


        // NUEVA FACTURA
        private void btnuevfac_Click(object sender, EventArgs e)
        {
            tablaproducto.ReadOnly = false;
            tablaproducto.DataSource = null;
            tablaproducto.Rows.Clear();
            cajas_solo_lectura(false);
            limpiar_cajas_factura_compra();
            dibujar_tabla_producto();
            habilitar_botones_accion(false);
            btfactura.Enabled = false;
            btborrar.Enabled = false;
            txtnumerofac.Text = (obj_facc.max_fac_com() + 1).ToString();
            dtfecha.Value = DateTime.Now;
            txtitems.Text = tablaproducto.Rows.Count.ToString();
            txtruc.Focus();
        }

        // AUTOCOMPLETAR RUC
        public void autocompletar_txtruc()
        {
            txtruc.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtruc.AutoCompleteSource = AutoCompleteSource.CustomSource;

            foreach (DataRow row in dt_ruc.Rows)
            {
                txtruc.AutoCompleteCustomSource.Add(row["ruc_prv"].ToString());
            }

        }

        //AUTOCOMPLETAR PRODUCTO
        public void autocompletar_txtproducto()
        {
            txtproducto.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtproducto.AutoCompleteSource = AutoCompleteSource.CustomSource;

            foreach (DataRow row in dt_producto.Rows)
            {
                txtproducto.AutoCompleteCustomSource.Add(row["PRODUCTO"].ToString());
            }
        }

        //completa los datos del proveedor
        private void completar_prov(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dt_prov = obj_facc.completar_proveedor(txtruc.Text);
                if (dt_prov.Tables["proveedor"].Rows.Count != 0)
                {
                    txtrazon.Text = dt_prov.Tables["proveedor"].Rows[0]["razon_social_prv"].ToString();
                    txttel.Text = dt_prov.Tables["proveedor"].Rows[0]["telefono_prv"].ToString();                    
                    txtdireccion.Text = dt_prov.Tables["proveedor"].Rows[0]["direccion_prv"].ToString();
                    txtproducto.Focus();
                    dt_prov.Tables["proveedor"].Rows.Clear();
                }
                else
                {
                    MessageBox.Show(txtrazon.Text+ " NO SE ENCUENTRA EN LA BASE DE DATOS","ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    txtruc.Clear();
                }

            }
        }


        private void enviarProductoAGrilla()
        {
            btborrar.Enabled = true;
            btfactura.Enabled = true;
            
            
            agrilla();
            tablaproducto.Focus();

                   
        } /// fin de proceso enviar a grilla de datos

        private void agrilla()
        {
            int flag = 0;
            if (tablaproducto.Rows.Count == 0)
            {
                tablaproducto.Rows.Add(obj_pro.getCodigo(),obj_pro.getCodigoIdentificador(), obj_pro.getDescripcion(), null, obj_pro.getPrecioCompra().ToString("0.0000"), null,null,obj_pro.getIva());
                tablaproducto.CurrentCell = tablaproducto["cantidad", 0];
                
            }

            else
            {
                for (int y = 0; y < tablaproducto.Rows.Count; y++)
                {

                    if (Convert.ToInt16(tablaproducto.Rows[y].Cells["codigo"].Value) == obj_pro.getCodigo())
                    {
                        MessageBox.Show("EL PRODUCTO YA SE ENCUENTRA EN LA LISTA", "VERIFICA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        txtproducto.Focus();
                        flag = 1;
                    }

                }

                if (flag == 0)
                {
                    tablaproducto.Rows.Add(obj_pro.getCodigo(), obj_pro.getCodigoIdentificador(), obj_pro.getDescripcion(), null, obj_pro.getPrecioCompra().ToString("0.0000"), null, null, obj_pro.getIva());
                    tablaproducto.CurrentCell = tablaproducto["cantidad", tablaproducto.RowCount - 1];
                    
                }

            }
            txtitems.Text = tablaproducto.Rows.Count.ToString();
        }
        
        //envia los productos a la grilla de datos
        private void completar_producto(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                dt_pro = obj_facc.completar_producto_factura_compra(txtproducto.Text);

                if (dt_pro.Tables["producto"].Rows.Count > 0)// si el dataset esta vacio no hay el producto
                {
                    btborrar.Enabled = true;
                    btfactura.Enabled = true;
                    obj_pro.setCodigo(short.Parse(dt_pro.Tables["producto"].Rows[0]["Codigo"].ToString()));
                    obj_pro.setPrecioCompra(double.Parse(dt_pro.Tables["producto"].Rows[0]["Precio"].ToString()));
                    obj_pro.setIva(bool.Parse(dt_pro.Tables["producto"].Rows[0]["iva_pro"].ToString()));
                    obj_pro.setDescripcion(txtproducto.Text);
                    obj_pro.setCodigoIdentificador(dt_pro.Tables["producto"].Rows[0]["unic_ode"].ToString());
                    tablaproducto.Focus();
                    agrilla();
                    
                }

                else
                {
                    MessageBox.Show(txtproducto.Text+" NO SE ENCUENTRA EN LA BASE DE DATOS","ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    txtproducto.Focus();
                }

                //txtcodp.Clear();
                //txtprecio.Clear();
                txtproducto.Clear();


            }
        }


        public void loadInvoice(int numero_factura)
        {
            try
            {
                tablaproducto.DataSource = null;
                tablaproducto.Columns.Clear();
                txtnumerofac.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["numero"].ToString();
                txtruc.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["ruc"].ToString();
                txtrazon.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["nombre"].ToString();
                txttel.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["telefono"].ToString();
                txtdireccion.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["direccion"].ToString();
                dtfecha.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["fecha"].ToString();

                tablaproducto.DataSource = obj_facc.cargar_detalle_factura_compra(int.Parse(txtnumerofac.Text));

                txtsubtotal.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["subtotal"].ToString();
                txtiva.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["iva"].ToString();
                txtivacero.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["ivacero"].ToString();
                txttotal.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["total"].ToString();
                txtdescuento.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["descuento"].ToString();
                txtFacturaReal.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["numero_fac_real"].ToString();

                lbAnular.Text = dtFacturaCompra.Tables["factura_compra"].Rows[numero_factura]["estado"].ToString();
                if (lbAnular.Text.Equals("False"))
                {
                    btAnular.Enabled = false;
                }
                else
                {
                    btAnular.Enabled = true;
                }

                tablaproducto.Columns[0].Width = 50;
                tablaproducto.Columns[1].Width = 250;
                tablaproducto.Columns[2].Width = 50;

                tablaproducto.ReadOnly = true;
                txtitems.Text = tablaproducto.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void suma()
        {

            obj_facc.setSubtotal(0);
            obj_facc.setIva(0);
            obj_facc.setDescuento_fac(0);
           

            for (int i = 0; i < tablaproducto.RowCount; i++)//subtotal
            {
                obj_facc.setSubtotal(Convert.ToSingle(tablaproducto.Rows[i].Cells["total"].Value) + obj_facc.getSubtotal());// acumulador del subtotal              
                obj_facc.setDescuento_fac(float.Parse(tablaproducto.Rows[i].Cells["descuento"].Value.ToString()) + obj_facc.getDescuento_fac());
                
                if (bool.Parse(tablaproducto.Rows[i].Cells["iva"].Value.ToString()))
                {
                    obj_facc.setIva(((Convert.ToSingle(tablaproducto.Rows[i].Cells["total"].Value) *12)/100) + obj_facc.getIva()); // iva de cada producto que lo lleve
                }

            }

            //txtivacero.Text = 0.ToString();
            obj_facc.setIvaCero(0);            
            obj_facc.setTotalFactura(obj_facc.getSubtotal()  + obj_facc.getIva());//total
            //            
            txtsubtotal.Text = obj_facc.getSubtotal().ToString("0.00");          
            txtivacero.Text = obj_facc.getIvaCero().ToString("0.00");
            txtiva.Text = obj_facc.getIva().ToString("0.00");
            txtdescuento.Text = obj_facc.getDescuento_fac().ToString("0.00");
            txttotal.Text = obj_facc.getTotalFactura().ToString("0.00");
        }

        private void dibujar_tabla_producto() {
            tablaproducto.Columns.Add("codigo", "codigo"); //0
            tablaproducto.Columns.Add("id", "codigo");//1
            tablaproducto.Columns.Add("producto", "producto");//2
            tablaproducto.Columns.Add("cantidad", "cantidad");//3
            tablaproducto.Columns.Add("precio", "precio");//4
            tablaproducto.Columns.Add("descuento", "descuento %");//5
            tablaproducto.Columns.Add("total", "total");            //6
            tablaproducto.Columns.Add("iva", "iva");//77
            
            tablaproducto.Columns["id"].Width = 50;
            tablaproducto.Columns["producto"].Width = 250;
            tablaproducto.Columns["cantidad"].Width = 50;
            tablaproducto.Columns["descuento"].Width = 80;
            tablaproducto.Columns["precio"].Width = 80;
            tablaproducto.Columns["total"].Width = 80;
            
            tablaproducto.Columns["iva"].Visible = false;
            tablaproducto.Columns["codigo"].Visible = false;
            
            tablaproducto.Columns["total"].ReadOnly = true;
            tablaproducto.Columns["codigo"].ReadOnly = true;
            tablaproducto.Columns["producto"].ReadOnly = true;
        }

        private void tablaproducto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (tablaproducto.CurrentCell.ColumnIndex == 4) // columna precio               
                {
                    double newPrice = double.Parse(tablaproducto.CurrentRow.Cells["precio"].Value.ToString());
                    int cantidad = int.Parse(tablaproducto.CurrentRow.Cells["cantidad"].Value.ToString());
                    tablaproducto.CurrentRow.Cells["total"].Value = newPrice * cantidad;
                    suma(); 
                }

                if (tablaproducto.CurrentCell.ColumnIndex == 3 || tablaproducto.CurrentCell.ColumnIndex == 5) // columna cantidad y descuento
                {
                    if (Convert.ToInt16(tablaproducto.CurrentRow.Cells["cantidad"].Value) > 0)//para que funcione el evento
                    {
                        tablaproducto.CurrentRow.Cells["descuento"].Value = ((Convert.ToSingle(tablaproducto.CurrentRow.Cells["total"].Value) * Convert.ToSingle(tablaproducto.CurrentRow.Cells["descuento"].Value)) / 100).ToString("0.0000");// descuento
                        tablaproducto.CurrentRow.Cells["total"].Value = ((Convert.ToInt16(tablaproducto.CurrentRow.Cells["cantidad"].Value) * Convert.ToSingle(tablaproducto.CurrentRow.Cells["precio"].Value)) - (Convert.ToSingle(tablaproducto.CurrentRow.Cells["descuento"].Value))).ToString("0.00");
                        tablaproducto.CurrentCell = tablaproducto["precio", tablaproducto.CurrentRow.Index];
                        tablaproducto.Focus();
                        suma();
                        txtproducto.Clear();
                    }
                    else
                    {
                        tablaproducto.CurrentCell.Value = null;
                        MessageBox.Show("LA CANTIDAD DEBE SER MAYOR A 0", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tablaproducto.CurrentCell = tablaproducto["cantidad", tablaproducto.CurrentRow.Index];
                        tablaproducto.Focus();
                    }
                }
            }
            catch
            {
                MessageBox.Show("INGRESAR SOLO NUMEROS", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                tablaproducto.CurrentCell = tablaproducto["cantidad", tablaproducto.CurrentRow.Index];
                // tablaproducto.CurrentCell.Value = null;
                tablaproducto.Focus();
                
            }
        }

        private void btcancelar_Click(object sender, EventArgs e)
        {
            limpiar_cajas_factura_compra();
            habilitar_botones_accion(false);
            btborrar.Enabled = false;
            btfactura.Enabled = false;
            btborrar.Enabled = false;
            btbuscar_pro.Enabled = false;
            btbuscar_prv.Enabled = false;
            btnuevfac.Enabled = true;
            btcancelar.Enabled = false;
            btnAddProducto.Enabled = false;
            btnAddProveedor.Enabled = false;
            txtnumerofac.Clear();
            tablaproducto.Columns.Clear();
            cajas_solo_lectura(true);
        }

        private void btverfacturas_Click(object sender, EventArgs e)
        {
            frm_facturas_compradas frmCompradas = new frm_facturas_compradas();
            DialogResult resultado = frmCompradas.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                searchInvoiceById(frmCompradas.numeroFactura);
            }

        }

        private void lbcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnAddProducto_Click(object sender, EventArgs e)
        {
            frm_add_producto frm = new frm_add_producto();
            DialogResult resultado = frm.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                
                obj_pro.setCodigo(frm.objProducto.getCodigo());
                obj_pro.setCodigoIdentificador(frm.objProducto.getCodigoIdentificador());
                obj_pro.setPrecioCompra(frm.objProducto.getPrecioCompra());
                obj_pro.setDescripcion(frm.objProducto.getDescripcion());
                obj_pro.setIva(frm.objProducto.getIva());                
                this.enviarProductoAGrilla();
                dt_producto = obj_facc.autocompletar_producto_facturacompra();
                autocompletar_txtproducto();
            }

        }

        private void btnAddProveedor_Click(object sender, EventArgs e)
        {
            frm_add_proveedor frmaddprov = new frm_add_proveedor();
            DialogResult resultado = frmaddprov.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                txtruc.Text = frmaddprov.getProveedor().getRuc();
                txtrazon.Text = frmaddprov.getProveedor().getRazon();
                txttel.Text = frmaddprov.getProveedor().getTelefono();
                txtdireccion.Text = frmaddprov.getProveedor().getDireccion();
                txtproducto.Focus();
            }
        }

        private void btAnular_Click(object sender, EventArgs e)
        {
             // se cambio el codigo de anular 
            DialogResult anular = MessageBox.Show("Desea Anular la Factura ", "ANULAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (anular == DialogResult.Yes)
            {

                obj_facc.eliminarFacturaCompra(int.Parse(txtnumerofac.Text));
                dtFacturaCompra = obj_facc.cargar_factura_compra();
                contadorFactura = dtFacturaCompra.Tables["factura_compra"].Rows.Count;
                if (contadorFactura > 0)
                {
                    loadInvoice(0);
                    cajas_solo_lectura(true);
                }
                else
                {
                    limpiar_cajas_factura_compra();
                    cajas_solo_lectura(true);
                    tablaproducto.DataSource = null;
                    tablaproducto.Columns.Clear();
                    tablaproducto.ReadOnly = true;

                    //habilitar_botones_accion(false);
                    btnuevfac.Enabled = true;
                    btverfacturas.Enabled = false;
                    btAnular.Enabled = false;
                    btfactura.Enabled = false;
                    btborrar.Enabled = false;
                    btcancelar.Enabled = false;
                    btbuscar_pro.Enabled = false;
                    btnAddProducto.Enabled = false;
                    btnAddProveedor.Enabled = false;
                    btbuscar_prv.Enabled = false;
                }

                //lbAnular.Text = "False";
                btAnular.Enabled = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchInvoice_Click(object sender, EventArgs e)
        {
            try {
                searchInvoiceById(int.Parse(txtSearchInvoice.Text));
            }
            catch
            {
                txtSearchInvoice.Text = "";
                txtSearchInvoice.Focus();
                MessageBox.Show("Ingrese el número de la factura",
                    "Validación",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Get invoice by numero_fac (PRIMARY KEY)
        /// </summary>
        /// <param name="invoiceId"></param>
        private void searchInvoiceById(int invoiceId)
        {
            dtFacturaCompra = obj_facc.getInvoiceById(invoiceId);
            if (dtFacturaCompra.Tables[0].Rows.Count > 0) {
                loadInvoice(0);
            }
            else
            {
                MessageBox.Show("No existen datos",
                "Información",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            }
        }
              
    }
}