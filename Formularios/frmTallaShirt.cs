﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frmTallaShirt : Form
    {
        // variables globales
        string id = "", descripcion = "" ;
        int tipo = 0, marca = 0;
        bool tienda;
        public producto[] p;


        public frmTallaShirt(string id , int tipo , int marca , string descripcion , bool tienda)
        {
            InitializeComponent();
            this.id = id;
            this.descripcion = descripcion;
            this.tipo = tipo;
            this.marca = marca;
            this.tienda = tienda;
        }


        public frmTallaShirt()        
        { 
        }

        private void frmTallaShirt_Load(object sender, EventArgs e)
        {
            enableTextBoxes(false);  // desahabilita los textboxes
        }


        private void enableTextBoxes(bool t)
        {
            textBoxSMALL.Enabled = t;
            textBoxLARGE.Enabled = t;
            textBoxMEDIUM.Enabled = t;
            textBoxPvpSmall.Enabled = t;
            textBoxPvpLarge.Enabled = t;
            textBoxPvpMedium.Enabled = t;
        }

        #region
        private void checkBoxSmall_Click(object sender, EventArgs e)
        {
            if (checkBoxSmall.Checked)
            {
                textBoxSMALL.Enabled = true;
                textBoxPvpSmall.Enabled = true;
                textBoxSMALL.Focus();
            }
            else
            {
                textBoxSMALL.Enabled = false;
                textBoxPvpSmall.Enabled = false;
                textBoxSMALL.Clear();
            }
        }

        private void checkBoxMedium_Click(object sender, EventArgs e)
        {
            if (checkBoxMedium.Checked)
            {
                textBoxMEDIUM.Enabled = true;
                textBoxPvpMedium.Enabled = true;
                textBoxMEDIUM.Focus();
            }
            else
            {
                textBoxMEDIUM.Enabled = false;
                textBoxPvpMedium.Enabled = false;
                textBoxMEDIUM.Clear();
            }
        }

        private void checkBoxLarge_Click(object sender, EventArgs e)
        {
            if (checkBoxLarge.Checked)
            {
                textBoxLARGE.Enabled = true;
                textBoxPvpLarge.Enabled = true;
                textBoxLARGE.Focus();
            }
            else
            {
                textBoxLARGE.Enabled = false;
                textBoxPvpLarge.Enabled = false;
                textBoxLARGE.Clear();
            }
        }
        #endregion

        private void buttonAceptar_Click(object sender, EventArgs e)
        {
            short contador=0 , contadorStock = 0 , contadorPvp = 0;
            
            string [] talla = new string[3];
            short [] stock;
            double [] pvp;


            foreach (Control control in this.Controls)
            {
                if (control is CheckBox)
                {
                    CheckBox c = (CheckBox)control;
                    if (c.Checked)
                    {
                        talla[contador] = c.Text;
                        contador++;
                    }                 
                }
            }
            
            stock = new short[contador];
            pvp = new double[contador];

            foreach(Control control in this.Controls)
            {
                
                if (control is TextBox)
                {
                    TextBox t = (TextBox)control;
                    if (t.Enabled && (t.Tag.ToString() == "atock") && t.Text != "" )
                    {
                      
                        stock[contadorStock] = short.Parse(t.Text);
                        contadorStock++;
                    }

                    if (t.Enabled && (t.Tag.ToString() == "pvp") && t.Text != "")
                    {                        
                        pvp[contadorPvp] = double.Parse(t.Text);
                        contadorPvp++;
                    }
                    
                }
            }

            p = new producto[contador]; //crea el array de producto 
           // producto pro = new producto();

            for (short i = 0; i < p.Length; i++)
            {
                p[i] = new producto();
                if (i == 0)
                    p[i].setCodigo(p[i].numeroMayor() + 1 );//codigobd
                else
                    p[i].setCodigo(p[i].numeroMayor() + (1+i));
                p[i].setCodigoIdentificador(id);// id
                p[i].setDescripcion(descripcion);//descripcion
                p[i].setMarca(marca);//marca 
                p[i].setTipoProducto(tipo);//tipo
                p[i].setTalla(talla[i]);//talla
                p[i].setUnidades(stock[i]);//stock                
                p[i].setPrecioVenta(pvp[(p.Length-1) - i]); //pvp
                p[i].setTienda(tienda);
                
            }
            this.DialogResult = DialogResult.OK;

        }

        #region
        private void textBoxPvpSmall_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void textBoxPvpMedium_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void textBoxPvpLarge_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void textBoxSMALL_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            if (char.IsNumber(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }
        #endregion


    }
}
