﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Reportes;

namespace fabisa.Formularios
{
    public partial class frm_buscar_proveedor : Form
    {
        public frm_buscar_proveedor()
        {
            InitializeComponent();
        }


        private Proveedor proveedor;
        public int numero = 0;




        public Proveedor getProveedor()
        {
            return this.proveedor;
        }

        public void setProveedor(Proveedor proveedor)
        {
            this.proveedor = proveedor;
        }

        
        private void buscarprov_Load(object sender, EventArgs e)
        {
            cargarprov();
            txtcri.Select();
        }


        public void cargarprov()
        {
            proveedor = new Proveedor();
            datosprov.DataSource = proveedor.seleccionarpro();
            if (datosprov.RowCount > 0)
            {
                datosprov.Columns[0].HeaderText = "RUC";
                datosprov.Columns[0].Width = 100;
                datosprov.Columns[1].HeaderText = "Razón Social";
                datosprov.Columns[1].Width = 150;
                datosprov.Columns[2].HeaderText = "Telefono";
                datosprov.Columns[2].Width = 75;
                datosprov.Columns[3].HeaderText = "Dirección";
                datosprov.Columns[3].Width = 300;
                datosprov.Columns[4].HeaderText = "Ciudad";
                datosprov.Columns[4].Width = 100;
                datosprov.Columns["estado_prv"].Visible = false;
                cmbprov.Text = "RAZON SOCIAL";
                txtcri.Focus();
            }
            else 
            {
                MessageBox.Show("NO SE HAN REGISTRADO PROVEEDORES", "VERIFICAR", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void txtcri_TextChanged(object sender, EventArgs e)
        {
            if (cmbprov.Text.Equals("RUC"))
            {
                datosprov.DataSource = proveedor.buscarxFiltros(txtcri.Text,"ruc_prv");
            }
            if (cmbprov.Text.Equals("RAZON SOCIAL"))
            {
                datosprov.DataSource = proveedor.buscarxFiltros(txtcri.Text,"razon_social_prv");
            }
        }

       

        private void btcancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void seleccionarProveedor()
        {
            if (datosprov.Rows.Count > 0)
            {
                proveedor.setRuc(datosprov.CurrentRow.Cells[0].Value.ToString());
                proveedor.setRazon(datosprov.CurrentRow.Cells[1].Value.ToString());
                proveedor.setTelefono(datosprov.CurrentRow.Cells[2].Value.ToString());
                proveedor.setDireccion(datosprov.CurrentRow.Cells[3].Value.ToString());
                proveedor.setCiudad(datosprov.CurrentRow.Cells[4].Value.ToString());
                numero = this.index(proveedor.getRuc());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
 
        }

        private void btaceptar_Click(object sender, EventArgs e)
        {
            seleccionarProveedor();
        }

       

        private void cmbprov_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtcri.Focus();
        }

        private void btreporte_Click(object sender, EventArgs e)
        {
           dsReporte ds = new dsReporte();
            byte contadorProveedor = 0;
      
            while (contadorProveedor < datosprov.Rows.Count)
            {
                ds.Tables["dtProveedor"].Rows.Add(datosprov.Rows[contadorProveedor].Cells["ruc_prv"].Value, datosprov.Rows[contadorProveedor].Cells["razon_social_prv"].Value, datosprov.Rows[contadorProveedor].Cells["telefono_prv"].Value, datosprov.Rows[contadorProveedor].Cells["direccion_prv"].Value, datosprov.Rows[contadorProveedor].Cells["ciudad_prv"].Value);                
                contadorProveedor++;

            }

            frm_Reporte_Proveedores frmRProveedor = new frm_Reporte_Proveedores(ds);
            frmRProveedor.ShowDialog();
        }


        
        
        
        
        
        
        private int index(string ruc)
        {
            int index = 0;
            for (int i = 0; i < proveedor.seleccionarpro().Rows.Count; i++)
            {
                if (ruc.Equals(proveedor.seleccionarpro().Rows[i][0].ToString()))
                {
                    return index = i;
                   // i = obj_prov.seleccionarpro().Rows.Count;
                }

            }
            return index;
        }


    }
}
