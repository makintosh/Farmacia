﻿namespace fabisa.Formularios
{
    partial class frm_usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_usuario));
            this.btnuevo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtapellido = new System.Windows.Forms.TextBox();
            this.btmodificar = new System.Windows.Forms.Button();
            this.btguardar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcedula = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtsueldo = new System.Windows.Forms.TextBox();
            this.btatras = new System.Windows.Forms.Button();
            this.btsig = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.gpusuario = new System.Windows.Forms.GroupBox();
            this.comboBoxcargo = new System.Windows.Forms.ComboBox();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txttelefono = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btbuscar = new System.Windows.Forms.Button();
            this.bteliminar = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btinicio = new System.Windows.Forms.Button();
            this.btfin = new System.Windows.Forms.Button();
            this.lbRegistro = new System.Windows.Forms.Label();
            this.gpusuario.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnuevo
            // 
            this.btnuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnuevo.Image")));
            this.btnuevo.Location = new System.Drawing.Point(32, 35);
            this.btnuevo.Name = "btnuevo";
            this.btnuevo.Size = new System.Drawing.Size(60, 56);
            this.btnuevo.TabIndex = 10;
            this.toolTip1.SetToolTip(this.btnuevo, "Nuevo");
            this.btnuevo.UseVisualStyleBackColor = true;
            this.btnuevo.Click += new System.EventHandler(this.btnuevo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nombre:";
            // 
            // txtnombre
            // 
            this.txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnombre.Location = new System.Drawing.Point(129, 58);
            this.txtnombre.MaxLength = 50;
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(224, 22);
            this.txtnombre.TabIndex = 2;
            this.txtnombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnombre_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Apellido";
            // 
            // txtapellido
            // 
            this.txtapellido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtapellido.Location = new System.Drawing.Point(129, 94);
            this.txtapellido.MaxLength = 50;
            this.txtapellido.Name = "txtapellido";
            this.txtapellido.Size = new System.Drawing.Size(224, 22);
            this.txtapellido.TabIndex = 3;
            this.txtapellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtapellido_KeyPress);
            // 
            // btmodificar
            // 
            this.btmodificar.Image = ((System.Drawing.Image)(resources.GetObject("btmodificar.Image")));
            this.btmodificar.Location = new System.Drawing.Point(32, 157);
            this.btmodificar.Name = "btmodificar";
            this.btmodificar.Size = new System.Drawing.Size(60, 60);
            this.btmodificar.TabIndex = 12;
            this.toolTip1.SetToolTip(this.btmodificar, "Modificar");
            this.btmodificar.UseVisualStyleBackColor = true;
            this.btmodificar.Click += new System.EventHandler(this.btmodificar_Click);
            // 
            // btguardar
            // 
            this.btguardar.Image = ((System.Drawing.Image)(resources.GetObject("btguardar.Image")));
            this.btguardar.Location = new System.Drawing.Point(32, 96);
            this.btguardar.Name = "btguardar";
            this.btguardar.Size = new System.Drawing.Size(60, 57);
            this.btguardar.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btguardar, "Guardar");
            this.btguardar.UseVisualStyleBackColor = true;
            this.btguardar.Click += new System.EventHandler(this.btguardar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Cedula:";
            // 
            // txtcedula
            // 
            this.txtcedula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcedula.Location = new System.Drawing.Point(129, 27);
            this.txtcedula.MaxLength = 10;
            this.txtcedula.Name = "txtcedula";
            this.txtcedula.Size = new System.Drawing.Size(131, 22);
            this.txtcedula.TabIndex = 1;
            this.txtcedula.Validated += new System.EventHandler(this.txtcedula_Validated);
            this.txtcedula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcedula_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(298, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Sueldo:";
            this.label5.Visible = false;
            // 
            // txtsueldo
            // 
            this.txtsueldo.Location = new System.Drawing.Point(356, 259);
            this.txtsueldo.MaxLength = 5;
            this.txtsueldo.Name = "txtsueldo";
            this.txtsueldo.Size = new System.Drawing.Size(85, 22);
            this.txtsueldo.TabIndex = 8;
            this.txtsueldo.Visible = false;
            this.txtsueldo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsueldo_KeyPress);
            // 
            // btatras
            // 
            this.btatras.Image = ((System.Drawing.Image)(resources.GetObject("btatras.Image")));
            this.btatras.Location = new System.Drawing.Point(270, 357);
            this.btatras.Name = "btatras";
            this.btatras.Size = new System.Drawing.Size(60, 53);
            this.btatras.TabIndex = 16;
            this.toolTip1.SetToolTip(this.btatras, "Atras");
            this.btatras.UseVisualStyleBackColor = true;
            this.btatras.Click += new System.EventHandler(this.btatras_Click);
            // 
            // btsig
            // 
            this.btsig.Image = ((System.Drawing.Image)(resources.GetObject("btsig.Image")));
            this.btsig.Location = new System.Drawing.Point(336, 358);
            this.btsig.Name = "btsig";
            this.btsig.Size = new System.Drawing.Size(60, 51);
            this.btsig.TabIndex = 17;
            this.toolTip1.SetToolTip(this.btsig, "Siguiente");
            this.btsig.UseVisualStyleBackColor = true;
            this.btsig.Click += new System.EventHandler(this.btsig_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 16);
            this.label9.TabIndex = 6;
            this.label9.Text = "Cargo:";
            // 
            // gpusuario
            // 
            this.gpusuario.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gpusuario.Controls.Add(this.comboBoxcargo);
            this.gpusuario.Controls.Add(this.txtpassword);
            this.gpusuario.Controls.Add(this.label7);
            this.gpusuario.Controls.Add(this.txttelefono);
            this.gpusuario.Controls.Add(this.label6);
            this.gpusuario.Controls.Add(this.txtdireccion);
            this.gpusuario.Controls.Add(this.label3);
            this.gpusuario.Controls.Add(this.label1);
            this.gpusuario.Controls.Add(this.txtnombre);
            this.gpusuario.Controls.Add(this.label2);
            this.gpusuario.Controls.Add(this.txtapellido);
            this.gpusuario.Controls.Add(this.txtsueldo);
            this.gpusuario.Controls.Add(this.label9);
            this.gpusuario.Controls.Add(this.label5);
            this.gpusuario.Controls.Add(this.label4);
            this.gpusuario.Controls.Add(this.txtcedula);
            this.gpusuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpusuario.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gpusuario.Location = new System.Drawing.Point(110, 41);
            this.gpusuario.Name = "gpusuario";
            this.gpusuario.Size = new System.Drawing.Size(447, 302);
            this.gpusuario.TabIndex = 18;
            this.gpusuario.TabStop = false;
            this.gpusuario.Text = "Usuario";
            // 
            // comboBoxcargo
            // 
            this.comboBoxcargo.FormattingEnabled = true;
            this.comboBoxcargo.Items.AddRange(new object[] {
            "ADMINISTRADOR",
            "USUARIO"});
            this.comboBoxcargo.Location = new System.Drawing.Point(129, 192);
            this.comboBoxcargo.Name = "comboBoxcargo";
            this.comboBoxcargo.Size = new System.Drawing.Size(168, 24);
            this.comboBoxcargo.TabIndex = 19;
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(129, 233);
            this.txtpassword.MaxLength = 10;
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = '*';
            this.txtpassword.Size = new System.Drawing.Size(85, 22);
            this.txtpassword.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 16);
            this.label7.TabIndex = 18;
            this.label7.Text = "Password:";
            // 
            // txttelefono
            // 
            this.txttelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttelefono.Location = new System.Drawing.Point(129, 159);
            this.txttelefono.MaxLength = 11;
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(131, 22);
            this.txttelefono.TabIndex = 5;
            this.txttelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelefono_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 16;
            this.label6.Text = "Telefono:";
            // 
            // txtdireccion
            // 
            this.txtdireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdireccion.Location = new System.Drawing.Point(129, 128);
            this.txtdireccion.MaxLength = 50;
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(224, 22);
            this.txtdireccion.TabIndex = 4;
            this.txtdireccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdireccion_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "Direccion:";
            // 
            // btbuscar
            // 
            this.btbuscar.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar.Image")));
            this.btbuscar.Location = new System.Drawing.Point(32, 221);
            this.btbuscar.Name = "btbuscar";
            this.btbuscar.Size = new System.Drawing.Size(60, 59);
            this.btbuscar.TabIndex = 13;
            this.toolTip1.SetToolTip(this.btbuscar, "Buscar");
            this.btbuscar.UseVisualStyleBackColor = true;
            this.btbuscar.Click += new System.EventHandler(this.btbuscar_Click);
            // 
            // bteliminar
            // 
            this.bteliminar.Image = ((System.Drawing.Image)(resources.GetObject("bteliminar.Image")));
            this.bteliminar.Location = new System.Drawing.Point(32, 286);
            this.bteliminar.Name = "bteliminar";
            this.bteliminar.Size = new System.Drawing.Size(60, 50);
            this.bteliminar.TabIndex = 14;
            this.toolTip1.SetToolTip(this.bteliminar, "Eliminar");
            this.bteliminar.UseVisualStyleBackColor = true;
            this.bteliminar.Click += new System.EventHandler(this.bteliminar_Click);
            // 
            // btcancelar
            // 
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(32, 342);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(60, 57);
            this.btcancelar.TabIndex = 15;
            this.toolTip1.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // btinicio
            // 
            this.btinicio.Image = ((System.Drawing.Image)(resources.GetObject("btinicio.Image")));
            this.btinicio.Location = new System.Drawing.Point(204, 359);
            this.btinicio.Name = "btinicio";
            this.btinicio.Size = new System.Drawing.Size(60, 51);
            this.btinicio.TabIndex = 19;
            this.toolTip1.SetToolTip(this.btinicio, "Inicio");
            this.btinicio.UseVisualStyleBackColor = true;
            this.btinicio.Click += new System.EventHandler(this.btinicio_Click);
            // 
            // btfin
            // 
            this.btfin.Image = ((System.Drawing.Image)(resources.GetObject("btfin.Image")));
            this.btfin.Location = new System.Drawing.Point(402, 359);
            this.btfin.Name = "btfin";
            this.btfin.Size = new System.Drawing.Size(60, 50);
            this.btfin.TabIndex = 20;
            this.toolTip1.SetToolTip(this.btfin, "Fin");
            this.btfin.UseVisualStyleBackColor = true;
            this.btfin.Click += new System.EventHandler(this.btfin_Click);
            // 
            // lbRegistro
            // 
            this.lbRegistro.AutoSize = true;
            this.lbRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRegistro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbRegistro.Location = new System.Drawing.Point(439, 16);
            this.lbRegistro.Name = "lbRegistro";
            this.lbRegistro.Size = new System.Drawing.Size(131, 24);
            this.lbRegistro.TabIndex = 21;
            this.lbRegistro.Text = "Usuario 0 de 0";
            // 
            // frm_usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(666, 434);
            this.Controls.Add(this.lbRegistro);
            this.Controls.Add(this.btfin);
            this.Controls.Add(this.btinicio);
            this.Controls.Add(this.btcancelar);
            this.Controls.Add(this.bteliminar);
            this.Controls.Add(this.btbuscar);
            this.Controls.Add(this.gpusuario);
            this.Controls.Add(this.btsig);
            this.Controls.Add(this.btatras);
            this.Controls.Add(this.btguardar);
            this.Controls.Add(this.btmodificar);
            this.Controls.Add(this.btnuevo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_usuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GESTION USUARIO";
            this.Load += new System.EventHandler(this.rolempleado_Load);
            this.gpusuario.ResumeLayout(false);
            this.gpusuario.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnuevo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtapellido;
        private System.Windows.Forms.Button btmodificar;
        private System.Windows.Forms.Button btguardar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcedula;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtsueldo;
        private System.Windows.Forms.Button btatras;
        private System.Windows.Forms.Button btsig;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gpusuario;
        private System.Windows.Forms.Button btbuscar;
        private System.Windows.Forms.Button bteliminar;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.TextBox txttelefono;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxcargo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btinicio;
        private System.Windows.Forms.Button btfin;
        private System.Windows.Forms.Label lbRegistro;
    }
}