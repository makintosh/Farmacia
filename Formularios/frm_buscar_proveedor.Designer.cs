﻿namespace fabisa.Formularios
{
    partial class frm_buscar_proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_buscar_proveedor));
            this.txtcri = new System.Windows.Forms.TextBox();
            this.datosprov = new System.Windows.Forms.DataGridView();
            this.btcancelar = new System.Windows.Forms.Button();
            this.btaceptar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbprov = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btreporte = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.datosprov)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtcri
            // 
            this.txtcri.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcri.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcri.Location = new System.Drawing.Point(120, 56);
            this.txtcri.Name = "txtcri";
            this.txtcri.Size = new System.Drawing.Size(171, 22);
            this.txtcri.TabIndex = 2;
            this.txtcri.TextChanged += new System.EventHandler(this.txtcri_TextChanged);
            // 
            // datosprov
            // 
            this.datosprov.AllowUserToAddRows = false;
            this.datosprov.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.datosprov.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.datosprov.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.datosprov.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.datosprov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosprov.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.datosprov.Location = new System.Drawing.Point(35, 150);
            this.datosprov.Name = "datosprov";
            this.datosprov.ReadOnly = true;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.datosprov.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.datosprov.Size = new System.Drawing.Size(612, 298);
            this.datosprov.TabIndex = 3;
            // 
            // btcancelar
            // 
            this.btcancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(477, 17);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(58, 61);
            this.btcancelar.TabIndex = 4;
            this.toolTip1.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // btaceptar
            // 
            this.btaceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btaceptar.Image = ((System.Drawing.Image)(resources.GetObject("btaceptar.Image")));
            this.btaceptar.Location = new System.Drawing.Point(412, 17);
            this.btaceptar.Name = "btaceptar";
            this.btaceptar.Size = new System.Drawing.Size(59, 61);
            this.btaceptar.TabIndex = 5;
            this.toolTip1.SetToolTip(this.btaceptar, "Aceptar");
            this.btaceptar.UseVisualStyleBackColor = true;
            this.btaceptar.Click += new System.EventHandler(this.btaceptar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(40, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Criterio:";
            // 
            // cmbprov
            // 
            this.cmbprov.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbprov.FormattingEnabled = true;
            this.cmbprov.Items.AddRange(new object[] {
            "RAZON SOCIAL",
            "RUC"});
            this.cmbprov.Location = new System.Drawing.Point(120, 19);
            this.cmbprov.Name = "cmbprov";
            this.cmbprov.Size = new System.Drawing.Size(171, 24);
            this.cmbprov.TabIndex = 7;
            this.cmbprov.SelectionChangeCommitted += new System.EventHandler(this.cmbprov_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(40, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Buscar:";
            // 
            // btreporte
            // 
            this.btreporte.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btreporte.Image = ((System.Drawing.Image)(resources.GetObject("btreporte.Image")));
            this.btreporte.Location = new System.Drawing.Point(541, 19);
            this.btreporte.Name = "btreporte";
            this.btreporte.Size = new System.Drawing.Size(53, 59);
            this.btreporte.TabIndex = 9;
            this.toolTip1.SetToolTip(this.btreporte, "Reporte");
            this.btreporte.UseVisualStyleBackColor = true;
            this.btreporte.Click += new System.EventHandler(this.btreporte_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.groupBox1.Controls.Add(this.btreporte);
            this.groupBox1.Controls.Add(this.cmbprov);
            this.groupBox1.Controls.Add(this.txtcri);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btcancelar);
            this.groupBox1.Controls.Add(this.btaceptar);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(35, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(612, 101);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            // 
            // frm_buscar_proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(680, 475);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.datosprov);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_buscar_proveedor";
            this.Opacity = 0.95;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BUSCAR PROVEEDOR";
            this.Load += new System.EventHandler(this.buscarprov_Load);
            ((System.ComponentModel.ISupportInitialize)(this.datosprov)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtcri;
        private System.Windows.Forms.DataGridView datosprov;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.Button btaceptar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbprov;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btreporte;
    }
}