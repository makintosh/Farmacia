﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frmTallaPant : Form
    {
        // variables globales
        string id = "", descripcion = "";
        int tipo = 0, marca = 0;
        bool t;
        public producto[] p;

        public frmTallaPant(string id, int tipo, int marca, string descripcion, bool tienda)
        {
            InitializeComponent();
            this.id = id;
            this.descripcion = descripcion;
            this.tipo = tipo;
            this.marca = marca;
            this.t = tienda;
        }

        private void frmTallaPant_Load(object sender, EventArgs e)
        {
            stateTextBoxes(false);
        }

        /// <summary>
        /// Habilita o Deshabilita TextBoxes
        /// </summary>
        /// <param name="t"></param>
        private void stateTextBoxes(bool t)
        {
            foreach (Control control in this.Controls)
            {
                if (control is TextBox)
                {
                    TextBox tb = (TextBox)control; //castea
                    tb.Enabled = t;
                }
            }
        }

        private void buttonAceptar_Click(object sender, EventArgs e)
        {
            short contador = 0, contadorStock = 0, contadorPvp = 0;

            string[] talla = new string[3];
            short[] stock;
            double[] pvp;


            foreach (Control control in this.Controls)
            {
                if (control is CheckBox)
                {
                    CheckBox c = (CheckBox)control;
                    if (c.Checked)
                    {
                        talla[contador] = c.Text;
                        contador++;
                    }
                }
            }

            stock = new short[contador];
            pvp = new double[contador];

            foreach (Control control in this.Controls)
            {

                if (control is TextBox)
                {
                    TextBox t = (TextBox)control;
                    if (t.Enabled && (t.Tag.ToString() == "atock") && t.Text != "")
                    {

                        stock[contadorStock] = short.Parse(t.Text);
                        contadorStock++;
                    }

                    if (t.Enabled && (t.Tag.ToString() == "pvp") && t.Text != "")
                    {
                        pvp[contadorPvp] = double.Parse(t.Text);
                        contadorPvp++;
                    }

                }
            }

            p = new producto[contador]; //crea el array de producto 
            // producto pro = new producto();

            for (short i = 0; i < p.Length; i++)
            {
                p[i] = new producto();
                if (i == 0)
                    p[i].setCodigo(p[i].numeroMayor() + 1);//codigobd
                else
                    p[i].setCodigo(p[i].numeroMayor() + (1 + i));
                p[i].setCodigoIdentificador(id);// id
                p[i].setDescripcion(descripcion);//descripcion
                p[i].setMarca(marca);//marca 
                p[i].setTipoProducto(tipo);//tipo
                p[i].setTalla(talla[i]);//talla
                p[i].setUnidades(stock[i]);//stock
                p[i].setPrecioVenta(pvp[i]); //pvp
                p[i].setTienda(t);
            }
            this.DialogResult = DialogResult.OK;
        }

        #region
        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                
                textBox1.Enabled = true;
                textBox7.Enabled = true;
                textBox1.Focus();
            }
            else 
            {
                textBox1.Clear();
                textBox1.Enabled = false;
                textBox7.Enabled = false;
                textBox7.Clear();
            }
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                
                textBox2.Enabled = true;
                textBox8.Enabled = true;
                textBox2.Focus();
            }
            else
            {
                textBox2.Clear();
                textBox2.Enabled = false;
                textBox8.Enabled = false;
                textBox8.Clear();
            }
        }

        private void checkBox3_Click(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                
                textBox3.Enabled = true;
                textBox9.Enabled = true;
                textBox3.Focus();
            }
            else
            {
                textBox3.Clear();
                textBox3.Enabled = false;
                textBox9.Enabled = false;
                textBox9.Clear();
            }
        }

        private void checkBox4_Click(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                
                textBox4.Enabled = true;
                textBox10.Enabled = true;
                textBox4.Focus();
            }
            else
            {
                textBox4.Clear();
                textBox4.Enabled = false;
                textBox10.Enabled = false;
                textBox10.Clear();
            }
        }

        private void checkBox5_Click(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                
                textBox5.Enabled = true;
                textBox11.Enabled = true;
                textBox5.Focus();
            }
            else
            {
                textBox5.Clear();
                textBox5.Enabled = false;
                textBox11.Enabled = false;
                textBox11.Clear();
            }
        }

        private void checkBox6_Click(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
            {
                
                textBox6.Enabled = true;
                textBox12.Enabled = true;
                textBox6.Focus();
            }
            else
            {
                textBox6.Clear();
                textBox6.Enabled = false;
                textBox12.Enabled = false;
                textBox12.Clear();
            }
        }


        private void validarStock(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void validarPVP(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        #endregion

    }
}
