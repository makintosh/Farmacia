﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.ProviderBase;
using fabisa.Aplicacion;
using fabisa.Reportes;

namespace fabisa.Formularios
{
    public partial class frm_cliente : Form
    {
        
                   
               
        public frm_cliente()
        {
            InitializeComponent();          
            habilitar_textos(false);            
            btguardar.Enabled = false;
            btcancelar.Enabled = false;
        }

        cliente obj_cli = new cliente();
        DataTable dt_cliente = new DataTable();
        DataSet dsCliente;
        int contadorCliente = 0;
        int bandera = 0;
        int totalClientes = 0;

        private void gestioncliente_Load(object sender, EventArgs e)
        {
            dsCliente = obj_cli.cargar_cliente();
            totalClientes = dsCliente.Tables["cliente"].Rows.Count;
            cargar_cliente(contadorCliente);
            lbRegistro.Text = "Cliente "+(contadorCliente+1)+" de "+totalClientes;
            
        }

       public void cargar_cliente(int numero)//cargar al momento de iniciar la gestion
        {

           
            if (dsCliente.Tables["cliente"].Rows.Count > 0)
            {
                txtcedulacli.Text = dsCliente.Tables["cliente"].Rows[numero]["cedula_per"].ToString();
                txtnombrecli.Text = dsCliente.Tables["cliente"].Rows[numero]["nombre_per"].ToString();
                txtapellidocli.Text = dsCliente.Tables["cliente"].Rows[numero]["apellido_per"].ToString();
                txttelefonocli.Text = dsCliente.Tables["cliente"].Rows[numero]["telefono_per"].ToString();
                txtdireccioncli.Text = dsCliente.Tables["cliente"].Rows[numero]["direccion_per"].ToString();
                
            }
            else
            {
                bteliminar.Enabled = false;
                btmodificar.Enabled = false;
                btbuscar.Enabled = false;
                habilitar_botones_navegacion(false);


            }
       }

       private void habilitar_botones_navegacion(bool t)
       {
           btinicio.Enabled = t;
           btsig.Enabled = t;
           btatras.Enabled = t;
           btfin.Enabled = t;
       }
       
       

       
        
        //NUEVO CLIENTE NGRESAR ///////////////////
        private void btnuevo_Click(object sender, EventArgs e)
       {
           habilitar_textos(true);
           limpiar_cajas();
           bandera = 1;
           txtcedulacli.Focus();
           btnuevo.Enabled = false;
           btmodificar.Enabled = false;
           bteliminar.Enabled = false;          
           btbuscar.Enabled = false;
           btguardar.Enabled=true;
           btcancelar.Enabled = true;
           habilitar_botones_navegacion(false);
           
       }

       //GUARDAR//////////////////////////
        private void btguardar_Click(object sender, EventArgs e)
       {

           if ((txtcedulacli.Text!="") && (txtnombrecli.Text!=""))
           {
              
               if (bandera == 0)//guardar modificar
               {
                   obj_cli.actualizar(nuevoCliente());                   
                   habilitar_textos(false);
                   btguardar.Enabled = false;
                   btcancelar.Enabled = false;
                   btnuevo.Enabled = true;
                   btmodificar.Enabled = true;
                   bteliminar.Enabled = true;                                      
                   btbuscar.Enabled = true;
                   habilitar_botones_navegacion(true);
                   
                   //cargar_cliente();
                   
               }
               if ((bandera == 1))//guardar nuevo
               {

                   bandera = 0;
                   obj_cli.guardar(nuevoCliente());
                   dsCliente = obj_cli.cargar_cliente();
                   habilitar_textos(false);                                     
                   btguardar.Enabled = false;
                   btcancelar.Enabled = false;
                   btnuevo.Enabled = true;
                   btmodificar.Enabled = true;
                   bteliminar.Enabled = true;                                      
                   btbuscar.Enabled = true;
                   habilitar_botones_navegacion(true);
                   totalClientes = dsCliente.Tables["cliente"].Rows.Count;
                   contadorCliente = totalClientes - 1;
                   lbRegistro.Text = "Cliente " + (contadorCliente + 1) + " de " + totalClientes;
                   
               }
           }
           else 
           {
               
               MessageBox.Show("INGRESO DE DATOS INCORRECTO", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
               //limpiar_cajas_mod();
               txtcedulacli.Focus();
           }
       }

        
        
        //ELIMINAR /////////////////////
        private void bteliminar_Click(object sender, EventArgs e)
        {

            obj_cli.setNombre(txtnombrecli.Text);
            obj_cli.setApellido(txtapellidocli.Text);
            DialogResult resultado;

            String elim = "ESTÁ SEGURO QUE DESEA ELIMINAR AL CLIENTE " + obj_cli.getNombre() + " " + obj_cli.getApellido();
            resultado = MessageBox.Show(elim, "ELIMINAR", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (resultado == System.Windows.Forms.DialogResult.Yes)
            {
                obj_cli.eliminar(txtcedulacli.Text,false);
                limpiar_cajas();
                dsCliente = obj_cli.cargar_cliente();
                totalClientes = dsCliente.Tables["cliente"].Rows.Count;
                contadorCliente=0;
                cargar_cliente(contadorCliente);
                lbRegistro.Text = "Cliente " + (contadorCliente + 1) + " de " + totalClientes;
            }         

        }


        //MODIFICAR /////////////////////
        private void btmodificar_Click(object sender, EventArgs e)
        {
            
            habilitar_textos_mod(true);
            habilitar_botones_navegacion(false);
            bandera = 0;
            btmodificar.Enabled = false;
            bteliminar.Enabled = false;          
            btbuscar.Enabled = false;
            btguardar.Enabled = true;
            btnuevo.Enabled = false;
            btcancelar.Enabled = true;
        }

        //BUSCAR //////////////////////////////
        private void btbuscar_Click(object sender, EventArgs e)
        {
            
            frm_buscar_cliente frm_busqueda_cliente = new frm_buscar_cliente(0);
            frm_busqueda_cliente.ShowDialog();
            
                if (frm_busqueda_cliente.DialogResult == DialogResult.OK)
                {
                    txtcedulacli.Text = frm_busqueda_cliente.getCliente().getCedula();
                    txtnombrecli.Text = frm_busqueda_cliente.getCliente().getNombre();
                    txtapellidocli.Text = frm_busqueda_cliente.getCliente().getApellido();
                    txttelefonocli.Text = frm_busqueda_cliente.getCliente().getTelefono();
                    txtdireccioncli.Text = frm_busqueda_cliente.getCliente().getDireccion();
                    contadorCliente = frm_busqueda_cliente.numeroCliente;
                    lbRegistro.Text = "Cliente " + (contadorCliente + 1) + " de " + totalClientes;
                }
                
            
        }

        //CANCELAR ///////////////////////////////////////////////////
        private void btcancelar_Click(object sender, EventArgs e)
        {
            if (dsCliente.Tables["cliente"].Rows.Count > 0)
            {
                bandera = 0;
                limpiar_cajas();
                cargar_cliente(contadorCliente);
                habilitar_textos(false);
                btmodificar.Enabled = true;
                bteliminar.Enabled = true;
                btbuscar.Enabled = true;
                btguardar.Enabled = false;
                btnuevo.Enabled = true;
                btcancelar.Enabled = false;
                habilitar_botones_navegacion(true);
            }

            else 
            {
                btmodificar.Enabled = false;
                btcancelar.Enabled = false;
                btbuscar.Enabled = false;
                btguardar.Enabled = false;
                btnuevo.Enabled = true;
                habilitar_botones_navegacion(false);
                habilitar_textos(false);
            }
        }
       
        
        private cliente nuevoCliente()
        {
          
           obj_cli.setNombre(txtnombrecli.Text.ToString());
           obj_cli.setApellido(txtapellidocli.Text.ToString());
           obj_cli.setCedula(txtcedulacli.Text.ToString());
           obj_cli.setTelefono(txttelefonocli.Text.ToString());
           obj_cli.setDireccion(txtdireccioncli.Text.ToString());
           obj_cli.setEstado(true); 
           return obj_cli;
 
       }
        
       //cajas ////////////////////////////////////////////////////////////////////////                 
        public void limpiar_cajas()
       {
           txtapellidocli.Clear();
           txtcedulacli.Clear();
           txtdireccioncli.Clear();
           txtnombrecli.Clear();
           txttelefonocli.Clear();
       }

        public void limpiar_cajas_mod()
        {
            txtapellidocli.Clear();            
            txtdireccioncli.Clear();
            txtnombrecli.Clear();
            txttelefonocli.Clear();
        }

        public void habilitar_textos_mod(Boolean c)
        {
            
            txtnombrecli.ReadOnly = !c;
            txtapellidocli.ReadOnly = !c;
            txtdireccioncli.ReadOnly = !c;
            txttelefonocli.ReadOnly = !c;
        }


        public void habilitar_textos(Boolean c)
        {
            txtcedulacli.ReadOnly = !c;
            txtnombrecli.ReadOnly = !c;
            txtapellidocli.ReadOnly = !c;
            txtdireccioncli.ReadOnly = !c;
            txttelefonocli.ReadOnly = !c;
        }
       ////////////////////////////////////////////////////////

      
                
        //validaciones de cajas de texto//////////////////////////////////////////
        private void txtcedulacli_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtnombrecli_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtapellidocli_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txttelefonocli_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar)||(e.KeyChar==8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtdireccioncli_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        
        //validacion de digitos correctos en la cedula
        private void txtcedulacli_Validated(object sender, EventArgs e)
        {
            if (bandera == 1)
            {
                if (txtcedulacli.Text.Length >= 10 && txtcedulacli.Text.Length <=13 )
                {
                    dt_cliente = obj_cli.buscarXFiltros(txtcedulacli.Text,"cedula_per");

                    if (dt_cliente.Rows.Count == 1)
                    {
                        MessageBox.Show("El CLIENTE "+dt_cliente.Rows[0]["cedula_per"].ToString()+ " YA SE ENCUENTRA EN NUESTRA BASE DE DATOS");
                        txtcedulacli.Clear();
                        txtcedulacli.Focus();
                    }
                }
                
            }
        }

     

        private void btsig_Click(object sender, EventArgs e)
        {

        
            
            contadorCliente++;

            if (contadorCliente < totalClientes)
            {
                cargar_cliente(contadorCliente);
                lbRegistro.Text = "Cliente " + (contadorCliente + 1) + " de " + totalClientes;
              
            }
            else
            {
                contadorCliente--;
            }
           
        }

        private void btfin_Click(object sender, EventArgs e)
        {
            contadorCliente = totalClientes-1;
            cargar_cliente(contadorCliente);            
            lbRegistro.Text = "Cliente " + totalClientes + " de " + totalClientes;
        }

        private void btinicio_Click(object sender, EventArgs e)
        {
            contadorCliente = 0;
            cargar_cliente(contadorCliente);
            
            lbRegistro.Text = "Cliente " + (contadorCliente + 1) + " de " + totalClientes;
            
        }

        private void btatras_Click(object sender, EventArgs e)
        {
           
            contadorCliente--;

            if (contadorCliente >= 0)
            {
                cargar_cliente(contadorCliente);
                lbRegistro.Text = "Cliente " + (contadorCliente + 1) + " de " + totalClientes;              
            }
            else
            {
                contadorCliente++;
            }
        }



        private void logicaBotonesNavegacion(bool t)
        {
            btatras.Enabled = t;
            btinicio.Enabled = t;
            btsig.Enabled = !t;
            btfin.Enabled = !t;
 
        }    
           
    }//public partial class
}//ultimo
