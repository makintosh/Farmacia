﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frmCargarImagen : Form
    {
        producto producto = new producto();
        //string[] rutaImagen;
        
        

        public frmCargarImagen(int codigo , string producto)
        {
            InitializeComponent();

            this.producto.setCodigo(codigo);
            this.producto.setDescripcion(producto);
        }


        private string[] getRuta(int codigo)
        {
            DataSet ds = new DataSet();
            int contador = 0;
            ds = producto.cargarImagenes(codigo);
            contador = ds.Tables["imagen"].Rows.Count;
            string [] ruta = new string[contador]; 

            for (short i = 0 ; i < contador ; i++)
            {
                ruta[i] = ds.Tables["imagen"].Rows[i]["ruta"].ToString();
            }

            return ruta;
        }





        /// <summary>
        /// Carga las Imagenes a los Picture Box
        /// </summary>
        /// <param name="ruta"></param>
        /// <param name="imagen"></param>
        private void cargarImagenes( string ruta , PictureBox imagen)
        {
            imagen.Image = Image.FromFile(ruta);
            imagen.SizeMode = PictureBoxSizeMode.StretchImage;
            //imagen.BorderStyle = BorderStyle.Fixed3D;
           // imagen.Name = ruta.FileName;
        }

        /// <summary>
        /// Recorre todos los Picture Box dentro de este control
        /// </summary>
        /// <param name="ruta"></param>
        private void recorrer(string [] ruta)
        {
            short contador = 0;
            foreach (Control c in this.Controls)
            {
                
                if (c is GroupBox)
                {
                    foreach (Control q in c.Controls)
                    {
                        if (q is PictureBox)
                        {
                            PictureBox pic = (PictureBox)q;

                            if (pic.Tag.ToString().Equals("secundario") && contador < ruta.Length)
                            {
                                cargarImagenes(ruta[contador], pic);
                                contador++;
                            }
                       
                        }
                    }
                }

            }
        }

        private void frmCargarImagen_Load(object sender, EventArgs e)
        {
            try
            {
                recorrer(this.getRuta(producto.getCodigo()));
                pictureBoxPrincipal.Image = pictureBox4.Image;
                pictureBoxPrincipal.SizeMode = PictureBoxSizeMode.StretchImage;
                labelNombreProducto.Text = producto.getDescripcion();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            pictureBoxPrincipal.Image = pictureBox1.Image;
            pictureBoxPrincipal.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            pictureBoxPrincipal.Image = pictureBox2.Image;
            pictureBoxPrincipal.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            pictureBoxPrincipal.Image = pictureBox3.Image;
            pictureBoxPrincipal.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            pictureBoxPrincipal.Image = pictureBox4.Image;
            pictureBoxPrincipal.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        #endregion
        
    }
}
