﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Formularios;
using fabisa.Reportes;

namespace fabisa
{
    public partial class frm_buscar_cliente : Form
    {
        public frm_buscar_cliente(int flag)
        {
            InitializeComponent();
            flagParaAdd = flag;
            txtcriterio.Select();
        }



        private cliente obj_cli;
        int flagParaAdd;
        public int numeroCliente;



        public cliente getCliente()
        {
            return obj_cli;

        }

        public void setCliente(cliente cli)
        {
            this.obj_cli = cli;
        }
        
        private void busquedacliente_Load(object sender, EventArgs e)
        {
            obj_cli = new cliente();
            grilladatos.DataSource = obj_cli.seleccionartodo();
            if (grilladatos.RowCount > 0)
            {
                grilladatos.Columns[4].HeaderText = "Telefono";
                grilladatos.Columns[0].HeaderText = "Cédula";
                grilladatos.Columns[1].HeaderText = "Nombre";
                grilladatos.Columns[2].HeaderText = "Apellido";
                grilladatos.Columns[3].HeaderText = "Dirección";
                grilladatos.Columns[1].Width = 200;
                grilladatos.Columns[3].Width = 200;
                combocriterio.Text = "APELLIDO";
            }
            else 
            {
                MessageBox.Show("NO SE HAN REGISTRADO CLIENTE", "VERIFICAR", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        
       
        
        private void seleccionar_dato()
        {
            if (grilladatos.RowCount > 0)
            {
                obj_cli.setCedula(grilladatos.CurrentRow.Cells["cedula_per"].Value.ToString());
                obj_cli.setNombre(grilladatos.CurrentRow.Cells["nombre_per"].Value.ToString());
                obj_cli.setApellido(grilladatos.CurrentRow.Cells["apellido_per"].Value.ToString());
                obj_cli.setTelefono(grilladatos.CurrentRow.Cells["telefono_per"].Value.ToString());
                obj_cli.setDireccion(grilladatos.CurrentRow.Cells["direccion_per"].Value.ToString());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else {
                MessageBox.Show("CLIENTE NO ENCONTRADO", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

       

        private void btcancelar_Click(object sender, EventArgs e)
        {
            
            this.Close();           
            
        }

        private void btaceptar_Click(object sender, EventArgs e)
        {
            seleccionar_dato();
        }

        private void txtcriterio_TextChanged_1(object sender, EventArgs e)
        {
            if (combocriterio.Text.Equals("CEDULA"))
            {
                grilladatos.DataSource = obj_cli.buscarXFiltros(txtcriterio.Text,"cedula_per");
            }
            if (combocriterio.Text.Equals("APELLIDO"))
            {
                grilladatos.DataSource = obj_cli.buscarXFiltros(txtcriterio.Text,"apellido_per");
            }
        }

        

        private void btreporte_Click(object sender, EventArgs e)
        {

            dsReporte ds = new dsReporte();
            //cliente[] cliente = new cliente[grilladatos.Rows.Count];

            for (int i = 0; i < grilladatos.Rows.Count; i++)
            {
                ds.Tables["dtCliente"].Rows.Add(grilladatos.Rows[i].Cells["nombre_per"].Value, grilladatos.Rows[i].Cells["apellido_per"].Value, grilladatos.Rows[i].Cells["telefono_per"].Value, grilladatos.Rows[i].Cells["direccion_per"].Value, grilladatos.Rows[i].Cells["cedula_per"].Value);
               // cliente[i] = this.nuevoCliente(i);
            }
            Reporte_Cliente reporte = new Reporte_Cliente(ds);
            reporte.ShowDialog();
 
        }

        private cliente nuevoCliente(int index)
        {
            obj_cli = new cliente();
            obj_cli.setCedula(grilladatos.Rows[index].Cells["cedula_per"].Value.ToString());
            obj_cli.setNombre(grilladatos.Rows[index].Cells["nombre_per"].Value.ToString());
            obj_cli.setApellido(grilladatos.Rows[index].Cells["apellido_per"].Value.ToString());
            return obj_cli;
        }

        private void combocriterio_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtcriterio.Focus();
        }
       
    }
}
