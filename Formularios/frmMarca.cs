﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frmMarca : Form
    {
        public frmMarca()
        {
            InitializeComponent();
        }

        // Variables Globales
        producto p;

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            p = new producto();
            p.setNombreMarca(textBoxMarca.Text);
            p.guardarMarca(p.getNombreMarca());
            p = null;
            cargar();
            textBoxMarca.Clear();
            textBoxMarca.Focus();
        }

        private void frmMarca_Load(object sender, EventArgs e)
        {
            cargar();
        }


        private void cargar()
        {
            p = new producto();
            dataGridViewMarca.DataSource = p.obtenerMarca();
            dataGridViewMarca.Columns[1].Visible = false;
            dataGridViewMarca.Columns[0].Width = 150;

            //DataGridViewButtonColumn button = new DataGridViewButtonColumn();
            //button.HeaderText = "Accion";
            //dataGridViewMarca.Columns.Add(button);            
            //asignarNombresAndText();
        }

        private void asignarNombresAndText()
        {
            foreach (DataGridViewRow row in dataGridViewMarca.Rows)
            {
                DataGridViewCell cell = row.Cells[2];
                cell.Value = "Eliminar";
            }
        }

        private void dataGridViewMarca_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                p = new producto();
                p.setNombreMarca(dataGridViewMarca.CurrentRow.Cells[0].Value.ToString());
                p.setMarca(int.Parse(dataGridViewMarca.CurrentRow.Cells[1].Value.ToString()));
                p.editarMarca(p.getNombreMarca() , p.getMarca());
            }            
        }

        private void frmMarca_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

     


        
    }
}
