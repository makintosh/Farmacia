﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using System.Data.OleDb;

namespace fabisa.Formularios
{
    public partial class frm_proveedor : Form
    {
        public frm_proveedor()
        {
            InitializeComponent();
            

        }

        Proveedor obj_prov = new Proveedor();
        DataSet dsProveedor;
        int flag_guardar=0;
        int contadorProveedor = 0;
        int totalProveedor = 0;
        
        private void gestionprovee_Load(object sender, EventArgs e)
        {         
            dsProveedor = obj_prov.cargar_proveedor();
            totalProveedor = dsProveedor.Tables["prov"].Rows.Count;
            habilitar_cajas(false);
            btguardar.Enabled = false;
            btcancelar.Enabled = false;
            //txtruc.Enabled = false;
            cargar_proveedor(0);
            lbRegistro.Text = "Proveedor "+(contadorProveedor+1)+" de "+totalProveedor;
        }

        
        public void cargar_proveedor(int numero)
        {

            
            if (dsProveedor.Tables["prov"].Rows.Count > 0)
            {
                txtruc.Text = dsProveedor.Tables["prov"].Rows[numero][0].ToString();
                txtrazon.Text = dsProveedor.Tables["prov"].Rows[numero][1].ToString();
                txttel.Text = dsProveedor.Tables["prov"].Rows[numero][2].ToString();
                txtdireccion.Text = dsProveedor.Tables["prov"].Rows[numero][3].ToString();
                txtciudad.Text = dsProveedor.Tables["prov"].Rows[numero][4].ToString();
                
            }
            else 
            {
                btcancelar.Enabled = false;
                btguardar.Enabled = false;
                btmodificar.Enabled = false;
                bteliminar.Enabled = false;
                habilitar_botones_navegacion(false);

            }
        }

        /// <summary>
        /// Logica de Botones y Cajas de texto.
        /// </summary>
        /// <param name="t"></param>

        private void habilitar_botones_navegacion(bool t)
        {
            btinicio.Enabled = t;
            btsig.Enabled = t;
            btatras.Enabled = t;
            btfin.Enabled = t;
        }
       


        public void limpiar_cajas()
        {
            txtciudad.Clear();
            txtrazon.Clear();            
            txttel.Clear();
            txtdireccion.Clear();            
            txtruc.Clear();
        }

        public void habilitar_cajas(Boolean c)
        {
            txtciudad.ReadOnly = !c;
            txtdireccion.ReadOnly = !c;
            txtrazon.ReadOnly = !c;
            txttel.ReadOnly = !c;            
            txtruc.ReadOnly = !c;
        }



        private void habilitar_botones_accion(bool t)
        {
            btnuevo.Enabled = t;
            btguardar.Enabled = !t;
            btmodificar.Enabled = t;
            bteliminar.Enabled = t;
            btbuscar.Enabled = t;
            btcancelar.Enabled = !t;
        }



        private Proveedor prov()
        {
            Proveedor prov = new Proveedor();
            prov.setRuc(txtruc.Text);
            prov.setRazon(txtrazon.Text);           
            prov.setCiudad(txtciudad.Text);
            prov.setDireccion(txtdireccion.Text);
            prov.setTelefono(txttel.Text);
            return prov;
        }
        
        //BOTONES

        private void btnuevo_Click(object sender, EventArgs e)
        {
            limpiar_cajas();            
            txtrazon.Focus();
            habilitar_cajas(true);
            habilitar_botones_accion(false);
            flag_guardar = 1;
           
        }

       
        //guardar /////////////////////////////////////////////////////////////
        private void btguardar_Click(object sender, EventArgs e)
        {
            if (txtruc.Text != "" && txtrazon.Text != "")
            {
                if (flag_guardar == 1) // guarda
                {

                    obj_prov.guardar(prov());
                    dsProveedor = obj_prov.cargar_proveedor();
                    totalProveedor = dsProveedor.Tables["prov"].Rows.Count;
                    habilitar_botones_navegacion(true);
                    habilitar_botones_accion(true);
                    habilitar_cajas(false);
                    flag_guardar = 0;
                }


                if (flag_guardar == 0) // modifica
                {

                    obj_prov.actulizar(prov());
                    dsProveedor = obj_prov.cargar_proveedor();
                    habilitar_botones_accion(true);
                    habilitar_botones_navegacion(true);
                    habilitar_cajas(false);
                }
            }
            else
            {
                MessageBox.Show("INGRESO DE DATOS INCORRECTO ES NECESARIO QUE ESTEN LLENOS LOS CAMPOS DE RUC Y RAZON SOCIAL","",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            
        }

        private void btmodificar_Click(object sender, EventArgs e)
        {
            flag_guardar = 0;            
            habilitar_cajas(true);
            txtruc.ReadOnly = true;
            habilitar_botones_accion(false);
            habilitar_botones_navegacion(false);
        }

        private void btbuscar_Click(object sender, EventArgs e)
        {
            
            frm_buscar_proveedor busprov = new frm_buscar_proveedor();
            busprov.ShowDialog();         
            
                if (busprov.DialogResult == DialogResult.OK)
                {
                    txtruc.Text = busprov.getProveedor().getRuc();
                    txtrazon.Text = busprov.getProveedor().getRazon();
                    txttel.Text = busprov.getProveedor().getTelefono();
                    txtdireccion.Text = busprov.getProveedor().getDireccion();
                    txtciudad.Text = busprov.getProveedor().getCiudad();
                    contadorProveedor = busprov.numero;
                    lbRegistro.Text = "Proveedor "+(contadorProveedor+1)+" de "+totalProveedor;

                }
            
        }

        private void btsalir_Click(object sender, EventArgs e)
        {
            this.Close();
         
        }

        private void btcancelar_Click(object sender, EventArgs e)
        {
            habilitar_cajas(false);                       
            habilitar_botones_navegacion(true);
            habilitar_botones_accion(true);
            limpiar_cajas();
            cargar_proveedor(contadorProveedor);
          
        }

        
        private void bteliminar_Click(object sender, EventArgs e)
        {
            DialogResult eliminar = MessageBox.Show("¿Esta seguro que desea eliminar al proveedor " + txtrazon.Text + " ?", "", MessageBoxButtons.YesNo);
            if (eliminar == System.Windows.Forms.DialogResult.Yes)
            {
                obj_prov.eliminar(txtruc.Text);
                limpiar_cajas();

                dsProveedor = obj_prov.cargar_proveedor();
                totalProveedor = dsProveedor.Tables["prov"].Rows.Count;
                
                contadorProveedor = 0;
                cargar_proveedor(contadorProveedor);

                lbRegistro.Text = "Proveedor "+(contadorProveedor+1)+" de "+totalProveedor;
            }
            else
            {
            }
        }



        //VALIDAR RUC PERSONA NATURAL O JURIDICA ////////////////////
        public void validar_ruc(String ruc)
        {

            Char[] vocales = new Char[13];        
            vocales = ruc.ToCharArray(); //convertir cadena de caracteres a vector          

         }

        
        
        private void txtruc_Validated(object sender, EventArgs e)
        {
            if (flag_guardar == 1)
            {

                if (txtruc.Text.Length == 13)
                {

                    if (obj_prov.buscarxFiltros(txtruc.Text, "ruc_prv").Rows.Count == 1)
                    {
                        MessageBox.Show("Ya esta registrado en la Base de Datos", "Verificar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtruc.Clear();
                        txtruc.Focus();
                    }

                }
                else
                {
                    if (txtruc.Text.Length > 0 && txtruc.Text.Length < 13)
                    {
                        int valida = 13 - txtruc.Text.Length;
                        MessageBox.Show("Error al ingresar el RUC, faltan " + valida.ToString() + " digitos");

                    }
                   
                }
            }
        }



        //validaciones cajas de texto ////////////////////////////////////////
        #region

        private void txtruc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtvendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtciudad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

       

        private void txttel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        #endregion


        ///// botones navegacion

        #region

        private void btsig_Click(object sender, EventArgs e)
        {
            contadorProveedor++;

            if (contadorProveedor < totalProveedor)
            {
                cargar_proveedor(contadorProveedor);
                lbRegistro.Text = "Proveedor " + (contadorProveedor + 1) + " de " + totalProveedor;
            }

            else
            {
                //MessageBox.Show("ULTIMO", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                contadorProveedor--;

            }

        }

        private void btfin_Click(object sender, EventArgs e)
        {
            contadorProveedor = totalProveedor-1;
            cargar_proveedor(contadorProveedor);
            lbRegistro.Text = "Proveedor " + (contadorProveedor + 1) + " de " + totalProveedor;
        }

        private void btinicio_Click(object sender, EventArgs e)
        {
            cargar_proveedor(0);
            contadorProveedor = 0;
            lbRegistro.Text = "Proveedor " + (contadorProveedor + 1) + " de " + totalProveedor;
        }

        private void btatras_Click(object sender, EventArgs e)
        {
            contadorProveedor--;
            if (contadorProveedor >= 0)
            {
                cargar_proveedor(contadorProveedor);
                lbRegistro.Text = "Proveedor " + (contadorProveedor + 1) + " de " + totalProveedor;
            }

            else
            {
                //MessageBox.Show("PRIMERO ", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                contadorProveedor = 0;
            }
        }

        #endregion




        ////////////////////////////////////////////////////////////////////////////////////////    
       
        
    }//penultimo
}//ultimo
