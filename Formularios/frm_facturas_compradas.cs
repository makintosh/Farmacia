﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Formularios;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frm_facturas_compradas : Form
    {
        public frm_facturas_compradas()
        {
            InitializeComponent();
        }


        // Variables e Instanciaciones
        fac_compra objFacturaCompra = new fac_compra();
        public int numeroFactura = 0;

        // Metodos 

        private void sumaTotalesFacturas()
        {
            decimal totalSumaFacturas=0;
            int numeroFilas = dgridFacturas.Rows.Count-1;

                while (numeroFilas >= 0)
                {
                    totalSumaFacturas = totalSumaFacturas + Convert.ToDecimal(dgridFacturas.Rows[numeroFilas].Cells["total"].Value);
                    numeroFilas--;
                }

            txttotal.Text = totalSumaFacturas.ToString("0.00");
            txtnum.Text = dgridFacturas.Rows.Count.ToString();
        }


        private void frm_facturas_compradas_Load(object sender, EventArgs e)
        {

            comboproveedor.DataSource = objFacturaCompra.mostrarProveedores();
            comboproveedor.DisplayMember = "proveedor";
            comboproveedor.ValueMember = "ruc";
            comboproveedor.Text = "";
            dgridFacturas.DataSource = objFacturaCompra.mostrar_facturas(); // carga las todas las facturas de compra emitidas
            dgridFacturas.Columns[1].Width = 200;
            sumaTotalesFacturas();
            
        }

        private void dtpdesde_ValueChanged(object sender, EventArgs e)
        {
            dgridFacturas.DataSource = objFacturaCompra.mostrar_facturasxfecha(dtpdesde.Value, dtphasta.Value);
            dgridFacturas.Columns[1].Width = 200;
            sumaTotalesFacturas();
        }

        private void dtphasta_ValueChanged(object sender, EventArgs e)
        {
            dgridFacturas.DataSource = objFacturaCompra.mostrar_facturasxfecha(dtpdesde.Value, dtphasta.Value);
            dgridFacturas.Columns[1].Width = 200;
            sumaTotalesFacturas();
        }

        private void comboproveedor_SelectionChangeCommitted(object sender, EventArgs e)
        {
            dgridFacturas.DataSource = objFacturaCompra.mostrarFacturasPorProveedor(comboproveedor.SelectedValue.ToString());
            sumaTotalesFacturas();

        }

        private void lbcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgridFacturas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            numeroFactura = int.Parse(dgridFacturas.CurrentRow.Cells[0].Value.ToString());
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

    }
}
