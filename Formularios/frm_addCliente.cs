﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frm_addCliente : Form
    {
        public frm_addCliente()
        {
            InitializeComponent();
            txtcedula.Select();
        }
        
        
        private cliente cliente = new cliente();


        public cliente getCliente()
        {
            return this.cliente;
        }


        private void btguardar1_Click(object sender, EventArgs e)
        {
            if (txtcedula.Text != "" && txtnombre.Text != "" && txtdireccion.Text != "")
            {
                cliente.guardar(guardarCliente());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else 
            {
                MessageBox.Show("FALTAN CAMPOS DE LLENAR", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        

        private void btguardar2_Click(object sender, EventArgs e)
        {
            if (txtcedula.Text != "" && txtnombre.Text != "" && txtapellido.Text != ""  && txtdireccion.Text != "")
            {
                cliente.guardar(guardarCliente());
                limpiarCajas();
                txtcedula.Focus();
            }
            else { MessageBox.Show("FALTAN CAMPOS DE LLENAR", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
        }

        
        private void limpiarCajas()
        {
            txtcedula.Clear();
            txtapellido.Clear();
            txtnombre.Clear();
            txttelefono.Clear();
            txtdireccion.Clear();
        }

        private cliente guardarCliente()
        {
                       
            cliente.setCedula(txtcedula.Text);
            cliente.setNombre(txtnombre.Text);
            cliente.setApellido(txtapellido.Text);
            cliente.setTelefono(txttelefono.Text);
            cliente.setDireccion(txtdireccion.Text);
            cliente.setEstado(true);
            return cliente;
        }

    

        private void frm_addCliente_Load(object sender, EventArgs e)
        {
            
        }

        private void txtcedula_Validated(object sender, EventArgs e)
        {
            if (txtcedula.Text.Length >= 10 && txtcedula.Text.Length <= 13)
            {


                if (cliente.buscarXFiltros(txtcedula.Text, "cedula_per").Rows.Count == 1)
                {
                    MessageBox.Show("YA SE ENCUENTRA EN NUESTRA BASE DE DATOS");
                    txtcedula.Clear();
                    txtcedula.Focus();
                }
            }
            else
            {
                txtcedula.Clear();
                txtcedula.Focus();
            }
        }

        private void txtcedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txttelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        
       
    }
}
