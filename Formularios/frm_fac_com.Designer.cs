﻿namespace fabisa.Formularios
{
    partial class frm_fac_com
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_fac_com));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btborrar = new System.Windows.Forms.Button();
            this.btnuevfac = new System.Windows.Forms.Button();
            this.btfactura = new System.Windows.Forms.Button();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.txtiva = new System.Windows.Forms.TextBox();
            this.txtivacero = new System.Windows.Forms.TextBox();
            this.txtsubtotal = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tablaproducto = new System.Windows.Forms.DataGridView();
            this.gproducto = new System.Windows.Forms.GroupBox();
            this.btnAddProducto = new System.Windows.Forms.Button();
            this.btbuscar_pro = new System.Windows.Forms.Button();
            this.txtproducto = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gpproveedor = new System.Windows.Forms.GroupBox();
            this.btnAddProveedor = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btbuscar_prv = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txttel = new System.Windows.Forms.TextBox();
            this.txtrazon = new System.Windows.Forms.TextBox();
            this.txtruc = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtdescuento = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFacturaReal = new System.Windows.Forms.TextBox();
            this.dtfecha = new System.Windows.Forms.DateTimePicker();
            this.txtnumerofac = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbAnular = new System.Windows.Forms.Label();
            this.btAnular = new System.Windows.Forms.Button();
            this.txtitems = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btverfacturas = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtSearchInvoice = new System.Windows.Forms.TextBox();
            this.btnSearchInvoice = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tablaproducto)).BeginInit();
            this.gproducto.SuspendLayout();
            this.gpproveedor.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btborrar
            // 
            this.btborrar.Image = ((System.Drawing.Image)(resources.GetObject("btborrar.Image")));
            this.btborrar.Location = new System.Drawing.Point(36, 340);
            this.btborrar.Name = "btborrar";
            this.btborrar.Size = new System.Drawing.Size(60, 53);
            this.btborrar.TabIndex = 48;
            this.toolTip1.SetToolTip(this.btborrar, "Borrar");
            this.btborrar.UseVisualStyleBackColor = true;
            this.btborrar.Click += new System.EventHandler(this.btborrar_Click);
            // 
            // btnuevfac
            // 
            this.btnuevfac.Image = ((System.Drawing.Image)(resources.GetObject("btnuevfac.Image")));
            this.btnuevfac.Location = new System.Drawing.Point(111, 537);
            this.btnuevfac.Name = "btnuevfac";
            this.btnuevfac.Size = new System.Drawing.Size(59, 60);
            this.btnuevfac.TabIndex = 45;
            this.toolTip1.SetToolTip(this.btnuevfac, "Nueva");
            this.btnuevfac.UseVisualStyleBackColor = true;
            this.btnuevfac.Click += new System.EventHandler(this.btnuevfac_Click);
            // 
            // btfactura
            // 
            this.btfactura.Image = ((System.Drawing.Image)(resources.GetObject("btfactura.Image")));
            this.btfactura.Location = new System.Drawing.Point(36, 399);
            this.btfactura.Name = "btfactura";
            this.btfactura.Size = new System.Drawing.Size(60, 57);
            this.btfactura.TabIndex = 44;
            this.toolTip1.SetToolTip(this.btfactura, "Guardar");
            this.btfactura.UseVisualStyleBackColor = true;
            this.btfactura.Click += new System.EventHandler(this.btfactura_Click);
            // 
            // txttotal
            // 
            this.txttotal.Location = new System.Drawing.Point(89, 121);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(105, 22);
            this.txttotal.TabIndex = 43;
            this.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtiva
            // 
            this.txtiva.Location = new System.Drawing.Point(89, 68);
            this.txtiva.Name = "txtiva";
            this.txtiva.Size = new System.Drawing.Size(105, 22);
            this.txtiva.TabIndex = 42;
            this.txtiva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtivacero
            // 
            this.txtivacero.Location = new System.Drawing.Point(89, 42);
            this.txtivacero.Name = "txtivacero";
            this.txtivacero.Size = new System.Drawing.Size(105, 22);
            this.txtivacero.TabIndex = 41;
            this.txtivacero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtsubtotal
            // 
            this.txtsubtotal.Location = new System.Drawing.Point(89, 16);
            this.txtsubtotal.Name = "txtsubtotal";
            this.txtsubtotal.Size = new System.Drawing.Size(105, 22);
            this.txtsubtotal.TabIndex = 40;
            this.txtsubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 16);
            this.label15.TabIndex = 39;
            this.label15.Text = "Total :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 16);
            this.label14.TabIndex = 38;
            this.label14.Text = "Iva 12% :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 16);
            this.label13.TabIndex = 37;
            this.label13.Text = "Iva 0%:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 16);
            this.label12.TabIndex = 36;
            this.label12.Text = "Subtotal :";
            // 
            // tablaproducto
            // 
            this.tablaproducto.AllowUserToAddRows = false;
            this.tablaproducto.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.tablaproducto.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.tablaproducto.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tablaproducto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablaproducto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.tablaproducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tablaproducto.DefaultCellStyle = dataGridViewCellStyle7;
            this.tablaproducto.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tablaproducto.Location = new System.Drawing.Point(111, 340);
            this.tablaproducto.Name = "tablaproducto";
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.tablaproducto.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.tablaproducto.Size = new System.Drawing.Size(621, 150);
            this.tablaproducto.TabIndex = 34;
            this.tablaproducto.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.tablaproducto_CellEndEdit);
            // 
            // gproducto
            // 
            this.gproducto.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gproducto.Controls.Add(this.btnAddProducto);
            this.gproducto.Controls.Add(this.btbuscar_pro);
            this.gproducto.Controls.Add(this.txtproducto);
            this.gproducto.Controls.Add(this.label9);
            this.gproducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gproducto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gproducto.Location = new System.Drawing.Point(28, 234);
            this.gproducto.Name = "gproducto";
            this.gproducto.Size = new System.Drawing.Size(704, 89);
            this.gproducto.TabIndex = 33;
            this.gproducto.TabStop = false;
            this.gproducto.Text = " Producto";
            // 
            // btnAddProducto
            // 
            this.btnAddProducto.Image = ((System.Drawing.Image)(resources.GetObject("btnAddProducto.Image")));
            this.btnAddProducto.Location = new System.Drawing.Point(571, 18);
            this.btnAddProducto.Name = "btnAddProducto";
            this.btnAddProducto.Size = new System.Drawing.Size(55, 55);
            this.btnAddProducto.TabIndex = 13;
            this.toolTip1.SetToolTip(this.btnAddProducto, "Añadir");
            this.btnAddProducto.UseVisualStyleBackColor = true;
            this.btnAddProducto.Click += new System.EventHandler(this.btnAddProducto_Click);
            // 
            // btbuscar_pro
            // 
            this.btbuscar_pro.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar_pro.Image")));
            this.btbuscar_pro.Location = new System.Drawing.Point(632, 18);
            this.btbuscar_pro.Name = "btbuscar_pro";
            this.btbuscar_pro.Size = new System.Drawing.Size(60, 55);
            this.btbuscar_pro.TabIndex = 12;
            this.btbuscar_pro.Text = ".....";
            this.toolTip1.SetToolTip(this.btbuscar_pro, "Buscar Producto");
            this.btbuscar_pro.UseVisualStyleBackColor = true;
            this.btbuscar_pro.Click += new System.EventHandler(this.btbuscar_pro_Click);
            // 
            // txtproducto
            // 
            this.txtproducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtproducto.Location = new System.Drawing.Point(101, 39);
            this.txtproducto.Name = "txtproducto";
            this.txtproducto.Size = new System.Drawing.Size(455, 22);
            this.txtproducto.TabIndex = 3;
            this.txtproducto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.completar_producto);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Producto :";
            // 
            // gpproveedor
            // 
            this.gpproveedor.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gpproveedor.Controls.Add(this.btnAddProveedor);
            this.gpproveedor.Controls.Add(this.label4);
            this.gpproveedor.Controls.Add(this.label3);
            this.gpproveedor.Controls.Add(this.btbuscar_prv);
            this.gpproveedor.Controls.Add(this.label5);
            this.gpproveedor.Controls.Add(this.txtdireccion);
            this.gpproveedor.Controls.Add(this.label6);
            this.gpproveedor.Controls.Add(this.txttel);
            this.gpproveedor.Controls.Add(this.txtrazon);
            this.gpproveedor.Controls.Add(this.txtruc);
            this.gpproveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpproveedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gpproveedor.Location = new System.Drawing.Point(28, 127);
            this.gpproveedor.Name = "gpproveedor";
            this.gpproveedor.Size = new System.Drawing.Size(704, 104);
            this.gpproveedor.TabIndex = 32;
            this.gpproveedor.TabStop = false;
            this.gpproveedor.Text = "Proveedor";
            // 
            // btnAddProveedor
            // 
            this.btnAddProveedor.Image = ((System.Drawing.Image)(resources.GetObject("btnAddProveedor.Image")));
            this.btnAddProveedor.Location = new System.Drawing.Point(571, 21);
            this.btnAddProveedor.Name = "btnAddProveedor";
            this.btnAddProveedor.Size = new System.Drawing.Size(55, 55);
            this.btnAddProveedor.TabIndex = 14;
            this.toolTip1.SetToolTip(this.btnAddProveedor, "Añadir");
            this.btnAddProveedor.UseVisualStyleBackColor = true;
            this.btnAddProveedor.Click += new System.EventHandler(this.btnAddProveedor_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Razón Social :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "RUC :";
            // 
            // btbuscar_prv
            // 
            this.btbuscar_prv.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar_prv.Image")));
            this.btbuscar_prv.Location = new System.Drawing.Point(632, 21);
            this.btbuscar_prv.Name = "btbuscar_prv";
            this.btbuscar_prv.Size = new System.Drawing.Size(60, 54);
            this.btbuscar_prv.TabIndex = 11;
            this.btbuscar_prv.Text = ".....";
            this.toolTip1.SetToolTip(this.btbuscar_prv, "Buscar Proveedor");
            this.btbuscar_prv.UseVisualStyleBackColor = true;
            this.btbuscar_prv.Click += new System.EventHandler(this.btbuscar_prv_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(306, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Telefono :";
            // 
            // txtdireccion
            // 
            this.txtdireccion.Location = new System.Drawing.Point(376, 53);
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(180, 22);
            this.txtdireccion.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(306, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Direccion :";
            // 
            // txttel
            // 
            this.txttel.Location = new System.Drawing.Point(376, 19);
            this.txttel.Name = "txttel";
            this.txttel.Size = new System.Drawing.Size(180, 22);
            this.txttel.TabIndex = 9;
            // 
            // txtrazon
            // 
            this.txtrazon.Location = new System.Drawing.Point(101, 53);
            this.txtrazon.Name = "txtrazon";
            this.txtrazon.Size = new System.Drawing.Size(183, 22);
            this.txtrazon.TabIndex = 8;
            // 
            // txtruc
            // 
            this.txtruc.Location = new System.Drawing.Point(101, 19);
            this.txtruc.MaxLength = 13;
            this.txtruc.Name = "txtruc";
            this.txtruc.Size = new System.Drawing.Size(183, 22);
            this.txtruc.TabIndex = 7;
            this.txtruc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.completar_prov);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.groupBox3.Controls.Add(this.txtdescuento);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtsubtotal);
            this.groupBox3.Controls.Add(this.txtivacero);
            this.groupBox3.Controls.Add(this.txttotal);
            this.groupBox3.Controls.Add(this.txtiva);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox3.Location = new System.Drawing.Point(530, 496);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(202, 158);
            this.groupBox3.TabIndex = 49;
            this.groupBox3.TabStop = false;
            // 
            // txtdescuento
            // 
            this.txtdescuento.Location = new System.Drawing.Point(89, 94);
            this.txtdescuento.Name = "txtdescuento";
            this.txtdescuento.Size = new System.Drawing.Size(105, 22);
            this.txtdescuento.TabIndex = 45;
            this.txtdescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 16);
            this.label11.TabIndex = 44;
            this.label11.Text = "Descuento:";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.txtFacturaReal);
            this.groupBox4.Controls.Add(this.dtfecha);
            this.groupBox4.Controls.Add(this.txtnumerofac);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox4.Location = new System.Drawing.Point(407, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(325, 111);
            this.groupBox4.TabIndex = 50;
            this.groupBox4.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 16);
            this.label16.TabIndex = 62;
            this.label16.Text = "Factura:";
            // 
            // txtFacturaReal
            // 
            this.txtFacturaReal.Location = new System.Drawing.Point(58, 77);
            this.txtFacturaReal.Name = "txtFacturaReal";
            this.txtFacturaReal.Size = new System.Drawing.Size(173, 22);
            this.txtFacturaReal.TabIndex = 61;
            // 
            // dtfecha
            // 
            this.dtfecha.Location = new System.Drawing.Point(58, 43);
            this.dtfecha.Name = "dtfecha";
            this.dtfecha.Size = new System.Drawing.Size(255, 22);
            this.dtfecha.TabIndex = 13;
            // 
            // txtnumerofac
            // 
            this.txtnumerofac.Location = new System.Drawing.Point(57, 13);
            this.txtnumerofac.Name = "txtnumerofac";
            this.txtnumerofac.Size = new System.Drawing.Size(37, 22);
            this.txtnumerofac.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "N° :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Fecha :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.txtSearchInvoice);
            this.panel1.Controls.Add(this.btnSearchInvoice);
            this.panel1.Controls.Add(this.lbAnular);
            this.panel1.Controls.Add(this.btAnular);
            this.panel1.Controls.Add(this.txtitems);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btverfacturas);
            this.panel1.Controls.Add(this.btcancelar);
            this.panel1.Controls.Add(this.gproducto);
            this.panel1.Controls.Add(this.gpproveedor);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.btfactura);
            this.panel1.Controls.Add(this.tablaproducto);
            this.panel1.Controls.Add(this.btborrar);
            this.panel1.Controls.Add(this.btnuevfac);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(762, 684);
            this.panel1.TabIndex = 57;
            // 
            // lbAnular
            // 
            this.lbAnular.AutoSize = true;
            this.lbAnular.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnular.ForeColor = System.Drawing.Color.Red;
            this.lbAnular.Location = new System.Drawing.Point(55, 617);
            this.lbAnular.Name = "lbAnular";
            this.lbAnular.Size = new System.Drawing.Size(0, 20);
            this.lbAnular.TabIndex = 62;
            // 
            // btAnular
            // 
            this.btAnular.Image = ((System.Drawing.Image)(resources.GetObject("btAnular.Image")));
            this.btAnular.Location = new System.Drawing.Point(244, 538);
            this.btAnular.Name = "btAnular";
            this.btAnular.Size = new System.Drawing.Size(59, 59);
            this.btAnular.TabIndex = 61;
            this.btAnular.Text = "ANULAR";
            this.btAnular.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btAnular.UseVisualStyleBackColor = true;
            this.btAnular.Click += new System.EventHandler(this.btAnular_Click);
            // 
            // txtitems
            // 
            this.txtitems.Location = new System.Drawing.Point(149, 496);
            this.txtitems.Name = "txtitems";
            this.txtitems.Size = new System.Drawing.Size(48, 20);
            this.txtitems.TabIndex = 59;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(108, 503);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 58;
            this.label1.Text = "Items:";
            // 
            // btverfacturas
            // 
            this.btverfacturas.Image = ((System.Drawing.Image)(resources.GetObject("btverfacturas.Image")));
            this.btverfacturas.Location = new System.Drawing.Point(176, 538);
            this.btverfacturas.Name = "btverfacturas";
            this.btverfacturas.Size = new System.Drawing.Size(62, 60);
            this.btverfacturas.TabIndex = 57;
            this.toolTip1.SetToolTip(this.btverfacturas, "Reportes");
            this.btverfacturas.UseVisualStyleBackColor = true;
            this.btverfacturas.Click += new System.EventHandler(this.btverfacturas_Click);
            // 
            // btcancelar
            // 
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(36, 462);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(60, 48);
            this.btcancelar.TabIndex = 56;
            this.toolTip1.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // txtSearchInvoice
            // 
            this.txtSearchInvoice.Location = new System.Drawing.Point(28, 30);
            this.txtSearchInvoice.Name = "txtSearchInvoice";
            this.txtSearchInvoice.Size = new System.Drawing.Size(126, 20);
            this.txtSearchInvoice.TabIndex = 66;
            // 
            // btnSearchInvoice
            // 
            this.btnSearchInvoice.Location = new System.Drawing.Point(160, 27);
            this.btnSearchInvoice.Name = "btnSearchInvoice";
            this.btnSearchInvoice.Size = new System.Drawing.Size(75, 23);
            this.btnSearchInvoice.TabIndex = 65;
            this.btnSearchInvoice.Text = "Buscar";
            this.btnSearchInvoice.UseVisualStyleBackColor = true;
            this.btnSearchInvoice.Click += new System.EventHandler(this.btnSearchInvoice_Click);
            // 
            // frm_fac_com
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 661);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_fac_com";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FACTURA COMPRA";
            this.Load += new System.EventHandler(this.facturacompra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tablaproducto)).EndInit();
            this.gproducto.ResumeLayout(false);
            this.gproducto.PerformLayout();
            this.gpproveedor.ResumeLayout(false);
            this.gpproveedor.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btborrar;
        private System.Windows.Forms.Button btnuevfac;
        private System.Windows.Forms.Button btfactura;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.TextBox txtiva;
        private System.Windows.Forms.TextBox txtivacero;
        private System.Windows.Forms.TextBox txtsubtotal;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView tablaproducto;
        private System.Windows.Forms.GroupBox gproducto;
        private System.Windows.Forms.Button btbuscar_pro;
        private System.Windows.Forms.TextBox txtproducto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gpproveedor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btbuscar_prv;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txttel;
        private System.Windows.Forms.TextBox txtrazon;
        private System.Windows.Forms.TextBox txtruc;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtnumerofac;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtfecha;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btverfacturas;
        private System.Windows.Forms.TextBox txtitems;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdescuento;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnAddProducto;
        private System.Windows.Forms.Button btnAddProveedor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFacturaReal;
        private System.Windows.Forms.Button btAnular;
        private System.Windows.Forms.Label lbAnular;
        private System.Windows.Forms.TextBox txtSearchInvoice;
        private System.Windows.Forms.Button btnSearchInvoice;
    }
}