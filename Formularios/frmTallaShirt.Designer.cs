﻿namespace fabisa.Formularios
{
    partial class frmTallaShirt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTallaShirt));
            this.checkBoxSmall = new System.Windows.Forms.CheckBox();
            this.checkBoxMedium = new System.Windows.Forms.CheckBox();
            this.checkBoxLarge = new System.Windows.Forms.CheckBox();
            this.textBoxSMALL = new System.Windows.Forms.TextBox();
            this.textBoxLARGE = new System.Windows.Forms.TextBox();
            this.textBoxMEDIUM = new System.Windows.Forms.TextBox();
            this.buttonAceptar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPvpSmall = new System.Windows.Forms.TextBox();
            this.textBoxPvpMedium = new System.Windows.Forms.TextBox();
            this.textBoxPvpLarge = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // checkBoxSmall
            // 
            this.checkBoxSmall.AutoSize = true;
            this.checkBoxSmall.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.checkBoxSmall.Location = new System.Drawing.Point(12, 41);
            this.checkBoxSmall.Name = "checkBoxSmall";
            this.checkBoxSmall.Size = new System.Drawing.Size(61, 17);
            this.checkBoxSmall.TabIndex = 2;
            this.checkBoxSmall.Text = "SMALL";
            this.checkBoxSmall.UseVisualStyleBackColor = true;
            this.checkBoxSmall.Click += new System.EventHandler(this.checkBoxSmall_Click);
            // 
            // checkBoxMedium
            // 
            this.checkBoxMedium.AutoSize = true;
            this.checkBoxMedium.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.checkBoxMedium.Location = new System.Drawing.Point(12, 80);
            this.checkBoxMedium.Name = "checkBoxMedium";
            this.checkBoxMedium.Size = new System.Drawing.Size(70, 17);
            this.checkBoxMedium.TabIndex = 3;
            this.checkBoxMedium.Text = "MEDIUM";
            this.checkBoxMedium.UseVisualStyleBackColor = true;
            this.checkBoxMedium.Click += new System.EventHandler(this.checkBoxMedium_Click);
            // 
            // checkBoxLarge
            // 
            this.checkBoxLarge.AutoSize = true;
            this.checkBoxLarge.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.checkBoxLarge.Location = new System.Drawing.Point(12, 122);
            this.checkBoxLarge.Name = "checkBoxLarge";
            this.checkBoxLarge.Size = new System.Drawing.Size(62, 17);
            this.checkBoxLarge.TabIndex = 4;
            this.checkBoxLarge.Text = "LARGE";
            this.checkBoxLarge.UseVisualStyleBackColor = true;
            this.checkBoxLarge.Click += new System.EventHandler(this.checkBoxLarge_Click);
            // 
            // textBoxSMALL
            // 
            this.textBoxSMALL.Location = new System.Drawing.Point(118, 38);
            this.textBoxSMALL.Name = "textBoxSMALL";
            this.textBoxSMALL.Size = new System.Drawing.Size(100, 20);
            this.textBoxSMALL.TabIndex = 5;
            this.textBoxSMALL.Tag = "atock";
            this.textBoxSMALL.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSMALL_KeyPress);
            // 
            // textBoxLARGE
            // 
            this.textBoxLARGE.Location = new System.Drawing.Point(118, 119);
            this.textBoxLARGE.Name = "textBoxLARGE";
            this.textBoxLARGE.Size = new System.Drawing.Size(100, 20);
            this.textBoxLARGE.TabIndex = 9;
            this.textBoxLARGE.Tag = "atock";
            this.textBoxLARGE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSMALL_KeyPress);
            // 
            // textBoxMEDIUM
            // 
            this.textBoxMEDIUM.Location = new System.Drawing.Point(118, 77);
            this.textBoxMEDIUM.Name = "textBoxMEDIUM";
            this.textBoxMEDIUM.Size = new System.Drawing.Size(100, 20);
            this.textBoxMEDIUM.TabIndex = 7;
            this.textBoxMEDIUM.Tag = "atock";
            this.textBoxMEDIUM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSMALL_KeyPress);
            // 
            // buttonAceptar
            // 
            this.buttonAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonAceptar.Location = new System.Drawing.Point(73, 169);
            this.buttonAceptar.Name = "buttonAceptar";
            this.buttonAceptar.Size = new System.Drawing.Size(261, 23);
            this.buttonAceptar.TabIndex = 11;
            this.buttonAceptar.Text = "Aceptar";
            this.buttonAceptar.UseVisualStyleBackColor = true;
            this.buttonAceptar.Click += new System.EventHandler(this.buttonAceptar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(143, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Stock";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(300, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "P.V.P";
            // 
            // textBoxPvpSmall
            // 
            this.textBoxPvpSmall.Location = new System.Drawing.Point(269, 39);
            this.textBoxPvpSmall.Name = "textBoxPvpSmall";
            this.textBoxPvpSmall.Size = new System.Drawing.Size(100, 20);
            this.textBoxPvpSmall.TabIndex = 6;
            this.textBoxPvpSmall.Tag = "pvp";
            this.textBoxPvpSmall.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPvpSmall_KeyPress);
            // 
            // textBoxPvpMedium
            // 
            this.textBoxPvpMedium.Location = new System.Drawing.Point(269, 77);
            this.textBoxPvpMedium.Name = "textBoxPvpMedium";
            this.textBoxPvpMedium.Size = new System.Drawing.Size(100, 20);
            this.textBoxPvpMedium.TabIndex = 8;
            this.textBoxPvpMedium.Tag = "pvp";
            this.textBoxPvpMedium.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPvpMedium_KeyPress);
            // 
            // textBoxPvpLarge
            // 
            this.textBoxPvpLarge.Location = new System.Drawing.Point(269, 119);
            this.textBoxPvpLarge.Name = "textBoxPvpLarge";
            this.textBoxPvpLarge.Size = new System.Drawing.Size(100, 20);
            this.textBoxPvpLarge.TabIndex = 10;
            this.textBoxPvpLarge.Tag = "pvp";
            this.textBoxPvpLarge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPvpLarge_KeyPress);
            // 
            // frmTallaShirt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(396, 204);
            this.Controls.Add(this.textBoxPvpLarge);
            this.Controls.Add(this.textBoxPvpMedium);
            this.Controls.Add(this.textBoxPvpSmall);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxSmall);
            this.Controls.Add(this.buttonAceptar);
            this.Controls.Add(this.checkBoxMedium);
            this.Controls.Add(this.textBoxSMALL);
            this.Controls.Add(this.textBoxMEDIUM);
            this.Controls.Add(this.textBoxLARGE);
            this.Controls.Add(this.checkBoxLarge);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmTallaShirt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TALLAS";
            this.Load += new System.EventHandler(this.frmTallaShirt_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxSmall;
        private System.Windows.Forms.CheckBox checkBoxMedium;
        private System.Windows.Forms.CheckBox checkBoxLarge;
        private System.Windows.Forms.TextBox textBoxSMALL;
        private System.Windows.Forms.TextBox textBoxLARGE;
        private System.Windows.Forms.TextBox textBoxMEDIUM;
        private System.Windows.Forms.Button buttonAceptar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPvpSmall;
        private System.Windows.Forms.TextBox textBoxPvpMedium;
        private System.Windows.Forms.TextBox textBoxPvpLarge;

    }
}