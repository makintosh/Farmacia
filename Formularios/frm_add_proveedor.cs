﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frm_add_proveedor : Form
    {
        public frm_add_proveedor()
        {
            InitializeComponent();
            txtRuc.Select();
            
        }


        private Proveedor proveedor = new Proveedor();


        public Proveedor getProveedor()
        {
            return proveedor;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtRuc.Text.Length > 0 && txtnombre.Text.Length > 0)
            {
                
                proveedor.guardar(this.nuevo());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Faltan campos de llenar ");
            }
        }


        private Proveedor nuevo()
        {
           // proveedor = new proveedor();
            proveedor.setRazon(txtnombre.Text);
            proveedor.setRuc(txtRuc.Text);
            proveedor.setDireccion(txtdireccion.Text);
            proveedor.setTelefono(txttelefono.Text);
            proveedor.setCiudad(txtCiudad.Text);
            return proveedor;
        }

        private void txtRuc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txttelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void frm_add_proveedor_Load(object sender, EventArgs e)
        {
            txtRuc.Focus();
        }

        private void txtRuc_Validated(object sender, EventArgs e)
        {
            if (txtRuc.Text.Length == 13)
            {

                if (proveedor.buscarxFiltros(txtRuc.Text, "ruc_prv").Rows.Count == 1)
                {
                    MessageBox.Show("Ya esta registrado en la Base de Datos", "Verificar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRuc.Clear();
                    txtRuc.Focus();
                }

            }
            else
            {
                if (txtRuc.Text.Length > 0 && txtRuc.Text.Length < 13)
                {
                    int valida = 13 - txtRuc.Text.Length;
                    MessageBox.Show("Error al ingresar el RUC, faltan " + valida.ToString() + " digitos");

                }

            }
        }




    }
}
