﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Reportes;
using fabisa.Utils;

namespace fabisa.Formularios
{
    public partial class frm_facturas_emitidas : Form
    {
        fac_venta obj_facv = new fac_venta();
        public int numeroFactura;
        private bool isFirstSearch = true;

        public frm_facturas_emitidas()
        {
            InitializeComponent();
        }

        private void verfacturas_Load(object sender, EventArgs e)
        {
            setClientData();
            setEmployeeData();
        }

        /// <summary>
        /// Sets numeroFactura and close the form
        /// </summary>
        private void seleccionar_facven_imp()
        {
            try {
                numeroFactura = int.Parse(this.cargarfacturas.CurrentRow.Cells[0].Value.ToString());
                this.DialogResult = DialogResult.OK;               
                this.Close();
            } catch {
                MessageBox.Show("No se puede cargar esta consulta");
            }
        }

        /// <summary>
        /// Sums all invoices
        /// </summary>
        private void totalFacturas()
        {
            double totalFactura = 0;
            int i = cargarfacturas.Rows.Count;
            while (i != 0) {
                totalFactura = totalFactura + double.Parse(cargarfacturas.Rows[i-1].Cells["total"].Value.ToString());
                --i;
            }
            txttotal.Text = totalFactura.ToString("0.00");
            txtnum.Text = cargarfacturas.Rows.Count.ToString();
        }
        
        /// <summary>
        /// On double click at invoice, loads it to factura_venta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cargarfacturas_DoubleClick(object sender, EventArgs e)
        {
            seleccionar_facven_imp();
        }

        /// <summary>
        /// Generate pdf report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btreporte_Click(object sender, EventArgs e)
        {
            if (cargarfacturas.Rows.Count > 0) {
                int i = 0;
                dsReporte ds = new dsReporte();
                while (i < cargarfacturas.RowCount) {
                    ds.Tables["dtFiltro"].Rows.Add(
                        cargarfacturas.Rows[i].Cells["numero"].Value,
                        cargarfacturas.Rows[i].Cells["vendedor"].Value +" - "+cargarfacturas.Rows[i].Cells["cliente"].Value, 
                        DateTime.Parse(cargarfacturas.Rows[i].Cells["fecha"].Value.ToString()).ToShortDateString(), 
                        cargarfacturas.Rows[i].Cells["total"].Value);
                    ++i;
                }
                ds.Tables["dtDetalle"].Rows.Add(null, null, null, null, txttotal.Text);
                frm_reportesFiltro frmRporte = new frm_reportesFiltro(ds);
                frmRporte.ShowDialog();
            }
            else {
                MessageBox.Show("No hay datos para generar el reporte", 
                    "Información", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);
            }
        }

        private void lbcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Sets employee data into comboBox vendedor
        /// </summary>
        private void setEmployeeData() {
            DataTable data = obj_facv.cargar_combo_usuario();
            ComboBoxUtil.addNewRowToDataTableAtZero(data);
            combovendedor.DataSource = data;
            combovendedor.ValueMember = "Cedula";
            combovendedor.DisplayMember = "Vendedor";
            combovendedor.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets client data into comboBox cliente
        /// </summary>
        private void setClientData() {
            DataTable data = obj_facv.cargar_combo_cliente();
            ComboBoxUtil.addNewRowToDataTableAtZero(data);
            combocliente.DataSource = data;
            combocliente.ValueMember = "Cedula";
            combocliente.DisplayMember = "Cliente";
            combocliente.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets table header column's name
        /// </summary>
        private void drawTableColumnsName() {
            cargarfacturas.Columns[0].HeaderText = "Nª";
            cargarfacturas.Columns[1].HeaderText = "Vendedor";
            cargarfacturas.Columns[2].HeaderText = "Cliente";
            cargarfacturas.Columns[3].HeaderText = "Fecha";
            cargarfacturas.Columns[4].HeaderText = "Total";
        }

        /// <summary>
        /// Searchs and set data for consolidated report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            
            cargarfacturas.DataSource = obj_facv.getConsolidatedReport(dateTimePicker1.Value.Date,
                dateTimePicker2.Value.Date,
                combovendedor.SelectedValue.ToString(),
                combocliente.SelectedValue.ToString());
            if (isFirstSearch) {
                drawTableColumnsName();
                isFirstSearch = false;
            }
            cargarfacturas.AutoResizeColumns();
            totalFacturas();
        }

    }
}
