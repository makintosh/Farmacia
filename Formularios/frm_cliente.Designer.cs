﻿namespace fabisa.Formularios
{
    partial class frm_cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_cliente));
            this.btnuevo = new System.Windows.Forms.Button();
            this.btguardar = new System.Windows.Forms.Button();
            this.btmodificar = new System.Windows.Forms.Button();
            this.bteliminar = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btbuscar = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcedulacli = new System.Windows.Forms.TextBox();
            this.txtnombrecli = new System.Windows.Forms.TextBox();
            this.txtapellidocli = new System.Windows.Forms.TextBox();
            this.txttelefonocli = new System.Windows.Forms.TextBox();
            this.txtdireccioncli = new System.Windows.Forms.TextBox();
            this.gpcliente = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btinicio = new System.Windows.Forms.Button();
            this.btatras = new System.Windows.Forms.Button();
            this.btsig = new System.Windows.Forms.Button();
            this.btfin = new System.Windows.Forms.Button();
            this.toolTipcli = new System.Windows.Forms.ToolTip(this.components);
            this.lbRegistro = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.gpcliente.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnuevo
            // 
            this.btnuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnuevo.Image")));
            this.btnuevo.Location = new System.Drawing.Point(3, 3);
            this.btnuevo.Name = "btnuevo";
            this.btnuevo.Size = new System.Drawing.Size(59, 57);
            this.btnuevo.TabIndex = 6;
            this.toolTipcli.SetToolTip(this.btnuevo, "Nuevo");
            this.btnuevo.UseVisualStyleBackColor = true;
            this.btnuevo.Click += new System.EventHandler(this.btnuevo_Click);
            // 
            // btguardar
            // 
            this.btguardar.Image = ((System.Drawing.Image)(resources.GetObject("btguardar.Image")));
            this.btguardar.Location = new System.Drawing.Point(3, 66);
            this.btguardar.Name = "btguardar";
            this.btguardar.Size = new System.Drawing.Size(59, 58);
            this.btguardar.TabIndex = 7;
            this.toolTipcli.SetToolTip(this.btguardar, "Guardar");
            this.btguardar.UseVisualStyleBackColor = true;
            this.btguardar.Click += new System.EventHandler(this.btguardar_Click);
            // 
            // btmodificar
            // 
            this.btmodificar.Image = ((System.Drawing.Image)(resources.GetObject("btmodificar.Image")));
            this.btmodificar.Location = new System.Drawing.Point(3, 130);
            this.btmodificar.Name = "btmodificar";
            this.btmodificar.Size = new System.Drawing.Size(59, 57);
            this.btmodificar.TabIndex = 8;
            this.toolTipcli.SetToolTip(this.btmodificar, "Modificar");
            this.btmodificar.UseVisualStyleBackColor = true;
            this.btmodificar.Click += new System.EventHandler(this.btmodificar_Click);
            // 
            // bteliminar
            // 
            this.bteliminar.Image = ((System.Drawing.Image)(resources.GetObject("bteliminar.Image")));
            this.bteliminar.Location = new System.Drawing.Point(3, 257);
            this.bteliminar.Name = "bteliminar";
            this.bteliminar.Size = new System.Drawing.Size(59, 57);
            this.bteliminar.TabIndex = 10;
            this.toolTipcli.SetToolTip(this.bteliminar, "Eliminar");
            this.bteliminar.UseVisualStyleBackColor = true;
            this.bteliminar.Click += new System.EventHandler(this.bteliminar_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.flowLayoutPanel1.Controls.Add(this.btnuevo);
            this.flowLayoutPanel1.Controls.Add(this.btguardar);
            this.flowLayoutPanel1.Controls.Add(this.btmodificar);
            this.flowLayoutPanel1.Controls.Add(this.btbuscar);
            this.flowLayoutPanel1.Controls.Add(this.bteliminar);
            this.flowLayoutPanel1.Controls.Add(this.btcancelar);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(35, 15);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(67, 370);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // btbuscar
            // 
            this.btbuscar.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar.Image")));
            this.btbuscar.Location = new System.Drawing.Point(3, 193);
            this.btbuscar.Name = "btbuscar";
            this.btbuscar.Size = new System.Drawing.Size(59, 58);
            this.btbuscar.TabIndex = 9;
            this.toolTipcli.SetToolTip(this.btbuscar, "Buscar");
            this.btbuscar.UseVisualStyleBackColor = true;
            this.btbuscar.Click += new System.EventHandler(this.btbuscar_Click);
            // 
            // btcancelar
            // 
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(3, 320);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(59, 48);
            this.btcancelar.TabIndex = 11;
            this.toolTipcli.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(26, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nombre :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(26, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Apellido :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(26, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Cedula :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(26, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Telefono :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(26, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Dirección :";
            // 
            // txtcedulacli
            // 
            this.txtcedulacli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcedulacli.Location = new System.Drawing.Point(109, 35);
            this.txtcedulacli.MaxLength = 13;
            this.txtcedulacli.Name = "txtcedulacli";
            this.txtcedulacli.Size = new System.Drawing.Size(151, 22);
            this.txtcedulacli.TabIndex = 1;
            this.txtcedulacli.Validated += new System.EventHandler(this.txtcedulacli_Validated);
            this.txtcedulacli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcedulacli_KeyPress);
            // 
            // txtnombrecli
            // 
            this.txtnombrecli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnombrecli.Location = new System.Drawing.Point(109, 70);
            this.txtnombrecli.MaxLength = 50;
            this.txtnombrecli.Name = "txtnombrecli";
            this.txtnombrecli.Size = new System.Drawing.Size(251, 22);
            this.txtnombrecli.TabIndex = 2;
            this.txtnombrecli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnombrecli_KeyPress);
            // 
            // txtapellidocli
            // 
            this.txtapellidocli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtapellidocli.Location = new System.Drawing.Point(109, 107);
            this.txtapellidocli.MaxLength = 50;
            this.txtapellidocli.Name = "txtapellidocli";
            this.txtapellidocli.Size = new System.Drawing.Size(251, 22);
            this.txtapellidocli.TabIndex = 3;
            this.txtapellidocli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtapellidocli_KeyPress);
            // 
            // txttelefonocli
            // 
            this.txttelefonocli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttelefonocli.Location = new System.Drawing.Point(109, 146);
            this.txttelefonocli.MaxLength = 11;
            this.txttelefonocli.Name = "txttelefonocli";
            this.txttelefonocli.Size = new System.Drawing.Size(151, 22);
            this.txttelefonocli.TabIndex = 4;
            this.txttelefonocli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelefonocli_KeyPress);
            // 
            // txtdireccioncli
            // 
            this.txtdireccioncli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdireccioncli.Location = new System.Drawing.Point(109, 184);
            this.txtdireccioncli.MaxLength = 300;
            this.txtdireccioncli.Name = "txtdireccioncli";
            this.txtdireccioncli.Size = new System.Drawing.Size(255, 22);
            this.txtdireccioncli.TabIndex = 5;
            this.txtdireccioncli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdireccioncli_KeyPress);
            // 
            // gpcliente
            // 
            this.gpcliente.Controls.Add(this.label3);
            this.gpcliente.Controls.Add(this.txtdireccioncli);
            this.gpcliente.Controls.Add(this.label1);
            this.gpcliente.Controls.Add(this.txttelefonocli);
            this.gpcliente.Controls.Add(this.label2);
            this.gpcliente.Controls.Add(this.txtapellidocli);
            this.gpcliente.Controls.Add(this.label4);
            this.gpcliente.Controls.Add(this.txtnombrecli);
            this.gpcliente.Controls.Add(this.label5);
            this.gpcliente.Controls.Add(this.txtcedulacli);
            this.gpcliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpcliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gpcliente.Location = new System.Drawing.Point(122, 52);
            this.gpcliente.Name = "gpcliente";
            this.gpcliente.Size = new System.Drawing.Size(447, 238);
            this.gpcliente.TabIndex = 17;
            this.gpcliente.TabStop = false;
            this.gpcliente.Text = "Cliente";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btinicio);
            this.flowLayoutPanel2.Controls.Add(this.btatras);
            this.flowLayoutPanel2.Controls.Add(this.btsig);
            this.flowLayoutPanel2.Controls.Add(this.btfin);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(222, 325);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(260, 60);
            this.flowLayoutPanel2.TabIndex = 28;
            // 
            // btinicio
            // 
            this.btinicio.Image = ((System.Drawing.Image)(resources.GetObject("btinicio.Image")));
            this.btinicio.Location = new System.Drawing.Point(3, 3);
            this.btinicio.Name = "btinicio";
            this.btinicio.Size = new System.Drawing.Size(51, 53);
            this.btinicio.TabIndex = 15;
            this.toolTipcli.SetToolTip(this.btinicio, "Primero");
            this.btinicio.UseVisualStyleBackColor = true;
            this.btinicio.Click += new System.EventHandler(this.btinicio_Click);
            // 
            // btatras
            // 
            this.btatras.Image = ((System.Drawing.Image)(resources.GetObject("btatras.Image")));
            this.btatras.Location = new System.Drawing.Point(60, 3);
            this.btatras.Name = "btatras";
            this.btatras.Size = new System.Drawing.Size(58, 53);
            this.btatras.TabIndex = 14;
            this.toolTipcli.SetToolTip(this.btatras, "Atras");
            this.btatras.UseVisualStyleBackColor = true;
            this.btatras.Click += new System.EventHandler(this.btatras_Click);
            // 
            // btsig
            // 
            this.btsig.Image = ((System.Drawing.Image)(resources.GetObject("btsig.Image")));
            this.btsig.Location = new System.Drawing.Point(124, 3);
            this.btsig.Name = "btsig";
            this.btsig.Size = new System.Drawing.Size(61, 53);
            this.btsig.TabIndex = 13;
            this.toolTipcli.SetToolTip(this.btsig, "Siguiente");
            this.btsig.UseVisualStyleBackColor = true;
            this.btsig.Click += new System.EventHandler(this.btsig_Click);
            // 
            // btfin
            // 
            this.btfin.Image = ((System.Drawing.Image)(resources.GetObject("btfin.Image")));
            this.btfin.Location = new System.Drawing.Point(191, 3);
            this.btfin.Name = "btfin";
            this.btfin.Size = new System.Drawing.Size(59, 53);
            this.btfin.TabIndex = 12;
            this.toolTipcli.SetToolTip(this.btfin, "Ultimo");
            this.btfin.UseVisualStyleBackColor = true;
            this.btfin.Click += new System.EventHandler(this.btfin_Click);
            // 
            // lbRegistro
            // 
            this.lbRegistro.AutoSize = true;
            this.lbRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRegistro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbRegistro.Location = new System.Drawing.Point(445, 15);
            this.lbRegistro.Name = "lbRegistro";
            this.lbRegistro.Size = new System.Drawing.Size(145, 25);
            this.lbRegistro.TabIndex = 16;
            this.lbRegistro.Text = "Cliente 0 de 0";
            // 
            // frm_cliente
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(645, 406);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.gpcliente);
            this.Controls.Add(this.lbRegistro);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_cliente";
            this.Opacity = 0.98;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GESTION CLIENTE";
            this.Load += new System.EventHandler(this.gestioncliente_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.gpcliente.ResumeLayout(false);
            this.gpcliente.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnuevo;
        private System.Windows.Forms.Button btguardar;
        private System.Windows.Forms.Button btmodificar;
        private System.Windows.Forms.Button bteliminar;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcedulacli;
        private System.Windows.Forms.TextBox txtnombrecli;
        private System.Windows.Forms.TextBox txtapellidocli;
        private System.Windows.Forms.TextBox txttelefonocli;
        private System.Windows.Forms.TextBox txtdireccioncli;
        private System.Windows.Forms.Button btbuscar;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.GroupBox gpcliente;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btinicio;
        private System.Windows.Forms.Button btatras;
        private System.Windows.Forms.Button btsig;
        private System.Windows.Forms.Button btfin;
        private System.Windows.Forms.ToolTip toolTipcli;
        private System.Windows.Forms.Label lbRegistro;

    }
}