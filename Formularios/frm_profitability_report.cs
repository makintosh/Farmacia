﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Reportes;
using fabisa.Utils;

namespace fabisa.Formularios
{
    public partial class frm_profitability_report : Form
    {
        fac_venta obj_facv = new fac_venta();
        CustomizedReport customizedReport = new CustomizedReport();

        public int numeroFactura;
        private bool isFirstSearch = true;

        public frm_profitability_report()
        {
            InitializeComponent();
        }

        private void verfacturas_Load(object sender, EventArgs e)
        {
            setEmployeeData();
            setProductData();
        }

        /// <summary>
        /// Sums all invoices
        /// </summary>
        private void sumInvoicesTotal()
        {
            txtnum.Text = dataGrid.Rows.Count.ToString();
        }
        

        /// <summary>
        /// Generate pdf report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btreporte_Click(object sender, EventArgs e)
        {
            if (dataGrid.Rows.Count > 0) {
                int i = 0;
                double sellPriceTotal = 0, purchasePriceTotal = 0, benefitTotal = 0;
                dsReporte ds = new dsReporte();
                while (i < dataGrid.RowCount) {
                    ds.Tables["dtProfitability"].Rows.Add(
                         DateTime.Parse(dataGrid.Rows[i].Cells["invoice_date"].Value.ToString()).ToShortDateString(),
                        dataGrid.Rows[i].Cells["invoice_number"].Value,
                        dataGrid.Rows[i].Cells["product_name"].Value,
                        dataGrid.Rows[i].Cells["sell_price"].Value,
                        dataGrid.Rows[i].Cells["purchase_price"].Value,
                        dataGrid.Rows[i].Cells["quantity"].Value,
                        dataGrid.Rows[i].Cells["benefit"].Value,
                        dataGrid.Rows[i].Cells["profitability"].Value);
                    sellPriceTotal += getValueAsDouble(dataGrid.Rows[i].Cells["sell_price"].Value);
                    purchasePriceTotal += getValueAsDouble(dataGrid.Rows[i].Cells["purchase_price"].Value);
                    benefitTotal += getValueAsDouble(dataGrid.Rows[i].Cells["benefit"].Value);
                    if (i == dataGrid.RowCount - 1) {
                        ds.Tables["dtProfitabilityTotals"].Rows.Add(
                            setPriceFormat(sellPriceTotal), 
                            setPriceFormat(purchasePriceTotal), 
                            setPriceFormat(benefitTotal));
                    }
                    ++i;
                }
                frm_profitabilityReport frmRporte = new frm_profitabilityReport(ds);
                frmRporte.Show();
            }
            else {
                MessageBox.Show("No hay datos para generar el reporte", 
                    "Información", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Set totals to report
        /// </summary>
        private void setTotals() {
            int i = 0;
            double sell = 0, purchase = 0;
            while (i < dataGrid.RowCount) {
                sell += getValueAsDouble(dataGrid.Rows[i].Cells["sell_price"].Value);
                purchase += getValueAsDouble(dataGrid.Rows[i].Cells["purchase_price"].Value);
                i++;
            }
            txtTotSell.Text = sell.ToString("0.00");
            txtTotPrch.Text = purchase.ToString("0.00");
            txtProfitability.Text = (sell - purchase).ToString("0.00");
        }

        /// <summary>
        /// Parse current value to double
        /// </summary>
        /// <param name="currentTotal"></param>
        /// <returns></returns>
        private double getValueAsDouble(object currentTotal) {
            return double.Parse(currentTotal.ToString());
        }

        private string setPriceFormat(double currentValue) {
            return currentValue.ToString("0.00");
        }

        private void lbcerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        /// <summary>
        /// Sets employee data into comboBox vendedor
        /// </summary>
        private void setEmployeeData() {
            DataTable data = obj_facv.cargar_combo_usuario();
            ComboBoxUtil.addNewRowToDataTableAtZero(data);
            comboBoxSeller.DataSource = data;
            comboBoxSeller.ValueMember = "Cedula";
            comboBoxSeller.DisplayMember = "Vendedor";
            comboBoxSeller.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets client data into comboBox cliente
        /// </summary>
        private void setProductData() {
            DataTable data = obj_facv.getProductData();
            ComboBoxUtil.addNewRowToDataTableAtZero(data);
            comboBoxProduct.DataSource = data;
            comboBoxProduct.ValueMember = "codigo";
            comboBoxProduct.DisplayMember = "producto";
            comboBoxProduct.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets table header column's name
        /// </summary>
        private void drawTableColumnsName() {
            // Hidden columns
            dataGrid.Columns[2].Visible = false; // product_code

            dataGrid.Columns[0].HeaderText = "Fecha";
            dataGrid.Columns[1].HeaderText = "N°";
            dataGrid.Columns[3].HeaderText = "Producto";
            dataGrid.Columns[4].HeaderText = "Precio Venta";
            dataGrid.Columns[5].HeaderText = "Precio Compra";
            dataGrid.Columns[6].HeaderText = "Unidades";
            dataGrid.Columns[7].HeaderText = "Ganancia";
            dataGrid.Columns[8].HeaderText = "Rentabilidad %";
        }

        /// <summary>
        /// Searchs and set data for consolidated report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            dataGrid.DataSource = customizedReport.getProfitabilityData(dateTimeFrom.Value.Date,
                dateTimeTo.Value.Date,
                comboBoxSeller.SelectedValue.ToString(),
                comboBoxProduct.SelectedValue.ToString());
            if (isFirstSearch) {
                drawTableColumnsName();
                isFirstSearch = false;
            }
            dataGrid.AutoResizeColumns();
            sumInvoicesTotal();
            setTotals();
        }

    }
}
