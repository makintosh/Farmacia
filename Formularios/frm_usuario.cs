﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Conexion;
using System.Data.OleDb;
using System.Data.ProviderBase;

namespace fabisa.Formularios
{
    public partial class frm_usuario : Form
    {
        public frm_usuario()
        {
            InitializeComponent();
        }


        // variables
        usuario obj_usu = new usuario();        
        DataSet dsUsuario = new DataSet();
        DataSet dsHoras = new DataSet();
        int contadorUsuario = 0;
        int totalUsuarios = 0;
        int banderaGuardar = 0;
      


        private void rolempleado_Load(object sender, EventArgs e)
        {
            dsUsuario = obj_usu.cargar_usuario_rol();
            cargar_usuario(contadorUsuario);
            totalUsuarios = dsUsuario.Tables["usuario"].Rows.Count;
            //datosempleados.Columns.Add("Tiempo", "Tiempo Trabajado");
            //datosempleados.Columns.Add("SUELDO", "Pago por Tiempo");
            //datosempleados.Rows.Add(1);            
            //sueldo_empleado();
            cajas_solo_lectura(true);
            botones_habilitados(true);
            lbRegistro.Text = "Usuario "+(contadorUsuario+1)+" de "+totalUsuarios;
        }

       
        public void cargar_usuario(int id)
        {
            
            if (dsUsuario.Tables["usuario"].Rows.Count > 0)
            {
               
                txtnombre.Text = dsUsuario.Tables["usuario"].Rows[id]["nombre_per"].ToString();
                txtapellido.Text = dsUsuario.Tables["usuario"].Rows[id]["apellido_per"].ToString();
                comboBoxcargo.Text = dsUsuario.Tables["usuario"].Rows[id]["tipo_usu"].ToString();
                txtcedula.Text = dsUsuario.Tables["usuario"].Rows[id]["cedula_per"].ToString();
                //      txtsueldo.Text = dsUsuario.Tables["usuario"].Rows[id]["sueldo_usu"].ToString();
                txtdireccion.Text = dsUsuario.Tables["usuario"].Rows[id]["direccion_per"].ToString();
                txttelefono.Text = dsUsuario.Tables["usuario"].Rows[id]["telefono_per"].ToString();
            }
            else
            {
                lbRegistro.Text = "Usuario " + (contadorUsuario) + " de " + totalUsuarios;
            }           
        }

        public void sueldo_empleado()
        {
            string tiempo;
            float sueldo,valorhora,valormin,valorseg,valortotal;
            int hh=0, mm=0, ss=0;
            dsHoras = obj_usu.h_m_s(txtcedula.Text);

            try
            {
                hh = Convert.ToInt16(dsHoras.Tables["horas"].Rows[0]["HORAS"]);
                mm = Convert.ToInt16(dsHoras.Tables["horas"].Rows[0]["MINUTOS"]);
                ss = Convert.ToInt16(dsHoras.Tables["horas"].Rows[0]["SEGUNDOS"]);

                if (ss > 60)
                {
                    mm = mm + 1;
                    ss = (ss - 60);
                }
                if (mm > 60)
                {
                    hh = hh + 1;
                    mm = (mm - 60);
                }

                tiempo = hh + " h :" + mm + " m :" + ss + " s";

                //datosempleados.Rows[0].Cells["Tiempo"].Value = tiempo;
                sueldo = Convert.ToSingle(txtsueldo.Text);
                valorhora = ((sueldo / 30) / 8) * 100;//valor de 1 hora en ctvs
                valormin = (valorhora / 60);//valor de 1 minuto en ctvs
                valorseg = (valormin / 60);//valor de 1 segundo en ctvs
                //MessageBox.Show(valorhora.ToString("0.00") + "  " + valormin.ToString("0.00") + "  " + valorseg.ToString("0.00"));

                valortotal = (valorhora * hh) + (valormin * mm) + (valorseg * ss);

                if (valortotal >= 100)
                {


                    //datosempleados.Rows[0].Cells["SUELDO"].Value = (valortotal / 100).ToString("0.00") + " U$D";
                }
                else
                {

                   // datosempleados.Rows[0].Cells["SUELDO"].Value = "0," + Convert.ToInt16(valortotal).ToString() + " ctvs";

                }
            }

            catch
            {
                //datosempleados.Rows.Clear();
            }             
                              
        }

        
        private void btsig_Click(object sender, EventArgs e)
        {
             contadorUsuario++;
            
            if (contadorUsuario >= dsUsuario.Tables["usuario"].Rows.Count)
            {
                contadorUsuario--;              
            }
            else
            {
                //datosempleados.Rows.Add(1);
                cargar_usuario(contadorUsuario);
                lbRegistro.Text = "Usuario " + (contadorUsuario + 1) + " de " + totalUsuarios;
                //sueldo_empleado();
            }
        }

        private void btatras_Click(object sender, EventArgs e)
        {
            contadorUsuario--;
            if(contadorUsuario < 0)
            {
                contadorUsuario = 0;
            }
            else
            {
                //datosempleados.Rows.Add(1);
                cargar_usuario(contadorUsuario);
                lbRegistro.Text = "Usuario " + (contadorUsuario + 1) + " de " + totalUsuarios;          
               // sueldo_empleado();
            }
        }

        
        
        private void btnuevo_Click(object sender, EventArgs e)
        {
            limpiar_cajas_texto();
            cajas_solo_lectura(false);
            txtcedula.Focus();
            botones_habilitados(false);
            banderaGuardar = 0;
        }


        /// <summary>
        /// Limpia todas las cajas de texto de Usuario
        /// </summary>
        private void limpiar_cajas_texto()
        {
            txtapellido.Clear();            
            txtcedula.Clear();
            txtnombre.Clear();
            txtsueldo.Clear();
            txtdireccion.Clear();
            txttelefono.Clear();
            txtpassword.Clear();
        }

        /// <summary>
        /// Activa solo lectura o escritura en los textbox
        /// </summary>
        /// <param name="t"></param>
        private void cajas_solo_lectura(bool t)
        {
            txtapellido.ReadOnly = t;
            comboBoxcargo.Enabled = !t;
            txtcedula.ReadOnly = t;
            txtnombre.ReadOnly = t;
            txtsueldo.ReadOnly = t;
            txtdireccion.ReadOnly = t;
            txttelefono.ReadOnly = t;
            txtpassword.ReadOnly = t;

 
        }

        /// <summary>
        /// Activa o Desactiva los botones
        /// </summary>
        /// <param name="t"></param>
        private void botones_habilitados(bool t)
        {
            btnuevo.Enabled = t;
            btmodificar.Enabled = t;
            btguardar.Enabled = !t;
            btbuscar.Enabled = t;
            bteliminar.Enabled = t;
            btcancelar.Enabled = !t;
            btinicio.Enabled = t;
            btatras.Enabled = t;
            btsig.Enabled = t;
            btfin.Enabled = t;
        }

        private void btguardar_Click(object sender, EventArgs e)
        {
            if (txtcedula.Text != "" && comboBoxcargo.Text != "")
            {
                if (banderaGuardar == 0)// guardar nuevo
                {
                    obj_usu.guardarUsuario(nuevoUsuario());
                    dsUsuario = obj_usu.cargar_usuario_rol();
                    totalUsuarios = dsUsuario.Tables["usuario"].Rows.Count;
                    contadorUsuario = totalUsuarios - 1;

                    botones_habilitados(true);
                    cajas_solo_lectura(true);

                    lbRegistro.Text = "Usuario " + (contadorUsuario + 1) + " de " + totalUsuarios;
                }
                else // modificar
                {
                    obj_usu.actualizarUsuario(nuevoUsuario());
                    dsUsuario = obj_usu.cargar_usuario_rol();
                    botones_habilitados(true);
                    cajas_solo_lectura(true);
 
                }

            }
            else 
            {
                MessageBox.Show("FALTAN CAMPOS DE LLENAR", "VERIFICACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private usuario nuevoUsuario()
        {
            obj_usu.setApellido(txtapellido.Text);
            obj_usu.setCedula(txtcedula.Text);
            obj_usu.setNombre(txtnombre.Text);
            obj_usu.setDireccion(txtdireccion.Text);
            obj_usu.setTelefono(txttelefono.Text);
            obj_usu.setCargo(comboBoxcargo.SelectedItem.ToString());
            obj_usu.setPasswordUsuario(txtpassword.Text);
            //obj_usu.setSueldo(double.Parse(txtsueldo.Text));
            obj_usu.setEstado(true);
            return obj_usu;

        }

        private void btmodificar_Click(object sender, EventArgs e)
        {
            banderaGuardar = 1;
            botones_habilitados(false);
            txtnombre.Focus();            
            cajas_solo_lectura(false);
            txtcedula.Enabled = false;
                 

        }

        private void bteliminar_Click(object sender, EventArgs e)
        {
            DialogResult eliminar = MessageBox.Show("ESTA SEGURO QUE DESEA ELIMINAR AL USUARIO " + txtnombre.Text + " " + txtapellido.Text + "", "VERIFICACIÓN", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (eliminar == DialogResult.OK)
            {
                obj_usu.deleteUsuario(txtcedula.Text);
                dsUsuario = obj_usu.cargar_usuario_rol();
                totalUsuarios = dsUsuario.Tables["usuario"].Rows.Count;
                contadorUsuario = totalUsuarios - 1;
                lbRegistro.Text = "Usuario " + (contadorUsuario + 1) + " de " + totalUsuarios;
            }
            else
            { }
            
        }

        private void txtcedula_Validated(object sender, EventArgs e)
        {
            if (banderaGuardar == 0)
            {
                if (txtcedula.Text.Length == 10)
                {
                    if (obj_usu.busquedaPorFiltros("cedula_per", txtcedula.Text).Rows.Count == 1)
                    {
                        MessageBox.Show("Ya se encuentra en a Base de Datos", "Verificar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtcedula.Clear();
                        txtcedula.Focus();
                    }
                }
                else 
                {
                    txtcedula.Clear();
                    txtcedula.Focus();
                }

            }
        }

        private void btcancelar_Click(object sender, EventArgs e)
        {
            
            limpiar_cajas_texto();
            cargar_usuario(contadorUsuario);
            cajas_solo_lectura(true);
            botones_habilitados(true);
            if (banderaGuardar == 1) { txtcedula.Enabled = true; }
            
        }

        private void btfin_Click(object sender, EventArgs e)
        {
            contadorUsuario = totalUsuarios - 1;
            cargar_usuario(contadorUsuario);
            lbRegistro.Text = "Usuario " + (contadorUsuario + 1) + " de " + totalUsuarios;
             
        }

        private void lbcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btbuscar_Click(object sender, EventArgs e)
        {
            frm_busqueda_usuario frmUsuario = new frm_busqueda_usuario();
            frmUsuario.ShowDialog();

            if (frmUsuario.DialogResult == DialogResult.OK)
            {
                txtcedula.Text = frmUsuario.objUsuario.getCedula();
                txtnombre.Text = frmUsuario.objUsuario.getNombre();
                txtapellido.Text = frmUsuario.objUsuario.getApellido();
                txtdireccion.Text = frmUsuario.objUsuario.getDireccion();
                txttelefono.Text = frmUsuario.objUsuario.getTelefono();
                //txtsueldo.Text = frmUsuario.objUsuario.getSueldo().ToString();
                comboBoxcargo.Text = frmUsuario.objUsuario.getCargo();
                contadorUsuario = frmUsuario.numeroUser;
            }
        }

        private void txtcedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
            banderaGuardar = 0;
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtapellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtdireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txttelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtsueldo_KeyPress(object sender, KeyPressEventArgs e)
        {
            String Aceptados = "0123456789.," + Convert.ToChar(8);

            if (Aceptados.Contains("" + e.KeyChar))
            { e.Handled = false; }
            else
            { e.Handled = true; }
        }

        private void btinicio_Click(object sender, EventArgs e)
        {
            contadorUsuario = 0;
            cargar_usuario(contadorUsuario);
            lbRegistro.Text = "Usuario " + (contadorUsuario + 1) + " de " + totalUsuarios;
        }

        
      
    



      

        
    }





    
}

        



