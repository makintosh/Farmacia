﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.ProviderBase;
using fabisa.Aplicacion;
using System.IO;
using System.Drawing.Imaging;

namespace fabisa.Formularios
{
    public partial class frm_producto : Form
    {
        
        //int ultimo=0;
        int flag_guardar = 0;//saber guardar , modificar o guardarVarios
        int contadorProducto = 0;
        int totalProducto = 0;
        usuario refUsuario;
        DataSet mibd;
        producto[] p; // array de productos para cuando son varios


        public frm_producto(string tipoUsuario)
        {
            InitializeComponent();
            refUsuario = new usuario();
            refUsuario.setCargo(tipoUsuario);
        }
                
        producto pro = new producto();

       
        /// <summary>
        /// Carga el producto de acuerdo a su codigo
        /// </summary>
        /// <param name="numero"></param>
        public void cargar_producto(int numero)
        {
            try
            {
                if (mibd.Tables["PRODUCTO"].Rows.Count > 0)
                {
                    check.Checked = false;
                    textBoxCodPer.Text = mibd.Tables["PRODUCTO"].Rows[numero]["unic_ode"].ToString();
                    txtcodigo.Text = mibd.Tables["PRODUCTO"].Rows[numero]["codigo"].ToString();
                    txtdescripcion.Text = mibd.Tables["PRODUCTO"].Rows[numero]["descripcion"].ToString();
                    comboBoxTipoProducto.Text = mibd.Tables["PRODUCTO"].Rows[numero]["tipo_nombre"].ToString();
                    comboBoxMarca.Text = mibd.Tables["PRODUCTO"].Rows[numero]["marca"].ToString();
                    comboBoxTalla.Text = mibd.Tables["PRODUCTO"].Rows[numero]["talla"].ToString();
                    txtprecioventa.Text = mibd.Tables["PRODUCTO"].Rows[numero]["p_venta"].ToString();
                    txtstock.Text = mibd.Tables["PRODUCTO"].Rows[numero]["stock"].ToString();                    
                    comboBoxLocalidad.SelectedIndex = 0;
                    if (bool.Parse(mibd.Tables["PRODUCTO"].Rows[numero]["iva_pro"].ToString())) {
                        check.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Llena los combo box tipoProducto e Marca
        /// </summary>
        public void fillComboBoxes()
        {
            ///////// TIPO PRODUCTO
            comboBoxTipoProducto.DataSource = pro.getProductType();
            comboBoxTipoProducto.DisplayMember = "tip_nom";
            comboBoxTipoProducto.ValueMember = "id";
            ///////// MARCA
            comboBoxMarca.DataSource = pro.getBrand();
            comboBoxMarca.DisplayMember = "mar_nom";
            comboBoxMarca.ValueMember = "id";
            // UNIT MEASUREMENT
            fillComboBoxTalla();
        }

       
       
        /// <summary>
        /// Logica de Botones y Cajas de Texto
        /// </summary>
        private void limpiar_cajas()
        {
            txtcodigo.Clear();
            txtdescripcion.Clear();
            textBoxCodPer.Clear();            
            //txtpreciocompra.Clear();
            txtprecioventa.Clear();
            txtstock.Clear();
        }

        private void limpiar_cajas_mod()
        {            
            txtdescripcion.Clear();
            //txtfabricante.Clear();
            txtstock.Clear();
            //txtpreciocompra.Clear();
            txtprecioventa.Clear();        
        }

        private void habilitar_textos_mod(Boolean c)
        {

            txtdescripcion.ReadOnly = c;
            textBoxCodPer.ReadOnly = c;
            txtstock.Enabled = !c;
            txtprecioventa.ReadOnly = c;
            check.Enabled = !c;
            comboBoxTipoProducto.Enabled = !c;
            comboBoxMarca.Enabled = !c;
            comboBoxTalla.Enabled = !c;
            comboBoxLocalidad.Enabled = !c;
        }


        private void habilitar_textos(Boolean c)
        {
            textBoxCodPer.ReadOnly = c;
            txtcodigo.ReadOnly = c;
            txtdescripcion.ReadOnly = c;
            txtprecioventa.ReadOnly = c;
           
            comboBoxMarca.Enabled = !c;
            comboBoxTipoProducto.Enabled = !c;
            comboBoxTalla.Enabled = !c;
            comboBoxLocalidad.Enabled = !c;
            check.Enabled = !c;
            txtstock.Enabled = !c;
        }


        private void habilitar_botones_accion(bool t)
        {
            btnuevo.Enabled = t;
            btguardar.Enabled = !t;
            btnMarca.Enabled = !t;
            btmodificar.Enabled = t;
            btcancelar.Enabled = !t;
            btbuscar.Enabled = t;
        }


        private void habilitar_botones_navegacion(bool t)
        {
            btinicio.Enabled = t;
            btatras.Enabled = t;
            btsig.Enabled = t;
            btfin.Enabled = t;
        }


        ///////////////////////////////////////////////////////

        /// <summary>
        /// Crea un Nuevo Producto
        /// </summary>
        /// <returns></returns>
        private producto nuevo_producto() {
            try
            {
                pro.setCodigoIdentificador(textBoxCodPer.Text);
                pro.setCodigo(short.Parse(txtcodigo.Text));
                pro.setDescripcion(txtdescripcion.Text.ToString());
                pro.setMarca(int.Parse(comboBoxMarca.SelectedValue.ToString()));
                pro.setTipoProducto(int.Parse(comboBoxTipoProducto.SelectedValue.ToString()));
                pro.setTalla(comboBoxTalla.Text);
                pro.setPrecioCompra(0);
                pro.setPrecioVenta(double.Parse(txtprecioventa.Text));
                pro.setUnidades(Convert.ToInt32(txtstock.Text));
                pro.setIva(check.Checked);
            }
            catch {
                MessageBox.Show("Todos los campos son necesarios", "Validación", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return pro;
        }

        /// <summary>
        /// Accion que Guarda o Modifica un Producto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btguardar_Click(object sender, EventArgs e)
        {
            if (flag_guardar == 0)//nuevo producto
            {
                if (txtdescripcion.Text != "" && txtstock.Text != "" && comboBoxMarca.Text != "")
                {
                    pro.guardar_producto(nuevo_producto()); //guardar producto
                    mibd = pro.cargar_productos();
                    totalProducto = mibd.Tables["PRODUCTO"].Rows.Count;
                    contadorProducto = totalProducto - 1;
                    habilitar_botones_accion(true);
                    habilitar_textos(true);
                    habilitar_botones_navegacion(true);
                    lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;

                }
                else 
                {
                    MessageBox.Show("Hay valores vacios por favor verifica");
                }

            }
            if (flag_guardar == 1)//modificar producto
            {
                if (txtdescripcion.Text != "" && txtstock.Text != "" && comboBoxMarca.Text != "")
                {
                    pro.actualizar_producto(nuevo_producto());
                    mibd = pro.cargar_productos();
                    habilitar_textos(true);
                    habilitar_botones_accion(true);
                    habilitar_botones_navegacion(true);
                    
                }
                else
                {
                    MessageBox.Show("Hay valores vacios por favor verifica");
                }
            }

            if (flag_guardar == 2) //guardar varios productos
            {
                for (short i = 0; i < p.Length; i++)
                {
                    p[i].guardar_producto(p[i]);
                }           
                mibd = pro.cargar_productos();
                habilitar_textos(true);
                habilitar_botones_accion(true);
                habilitar_botones_navegacion(true);
                totalProducto = mibd.Tables["PRODUCTO"].Rows.Count;
                contadorProducto = totalProducto - 1;
                cargar_producto(contadorProducto);
                lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;
            }
        }




        /// <summary>
        /// Evento Nuevo Producto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnuevo_Click(object sender, EventArgs e)
        {
            limpiar_cajas();
            habilitar_textos(false);
            habilitar_botones_accion(false);
            habilitar_botones_navegacion(false);
            txtcodigo.ReadOnly = true;
            stockState();
            txtcodigo.Text = Convert.ToString(pro.numeroMayor() + 1);
            txtstock.Text = "0";
            txtprecioventa.Text = "0";
            txtprecioventa.Enabled = true;            
            textBoxCodPer.Focus();
            fillComboBoxes();
            flag_guardar = 0;
        }

        //modificar producto/////////////////
        private void btmodificar_Click(object sender, EventArgs e)
        {
            habilitar_textos_mod(false);
            habilitar_botones_accion(false);
            habilitar_botones_navegacion(false);
            comboBoxTalla.DataSource = null;
            comboBoxTalla.Text = mibd.Tables["PRODUCTO"].Rows[contadorProducto]["talla"].ToString();
            textBoxCodPer.Focus();
            txtprecioventa.Enabled = true;
            stockState();
            flag_guardar = 1;
        }

        //boton cancelar////////////////////////////
        private void btcancelar_Click(object sender, EventArgs e)
        {
            limpiar_cajas();
            cargar_producto(contadorProducto);
            habilitar_textos(true);
            habilitar_botones_accion(true);
            habilitar_botones_navegacion(true);

        }

        

        private void btsalir_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btbuscar_Click(object sender, EventArgs e)
        {
           
            frm_buscar_producto bus_pro = new frm_buscar_producto(false);
            bus_pro.ShowDialog();
            
            if( bus_pro.DialogResult == DialogResult.OK)
            {
                contadorProducto = bus_pro.numeroPro;
                cargar_producto(contadorProducto);
                lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;
            }           
        }

        //validar cajas de texto ////////////////////////////////////////////////////////////
        #region



        private void txtstock_KeyPress(object sender, KeyPressEventArgs e)
       {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtprecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void txtprecioventa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }
        #endregion  

        ////////// botones navegación
        #region


        private void btinicio_Click(object sender, EventArgs e)
        {
            
            this.cargar_producto(0);
            contadorProducto = 0;
            lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;

        }

        private void btfin_Click(object sender, EventArgs e)
        {
            contadorProducto = totalProducto - 1;
            this.cargar_producto(contadorProducto);
            lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;
        }


       
        private void btsig_Click(object sender, EventArgs e)
        {
            contadorProducto++;
            if (contadorProducto < totalProducto)
            {
                this.cargar_producto(contadorProducto);
                lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;
            }

            else 
            {
                //MessageBox.Show("ES EL ULTIMO PRODUCTO", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                contadorProducto--;
            }
        }


       
        private void btatras_Click(object sender, EventArgs e)
        {
            contadorProducto--;

            if (contadorProducto < 0)
            {

                //MessageBox.Show("ES EL PRIMER PRODUCTO", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                contadorProducto++;
                
            }
            else
            {
                
                this.cargar_producto(contadorProducto);
                lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;
            }

        }

        #endregion

        private void frm_producto_Load(object sender, EventArgs e)
        {
            fillComboBoxes();
            mibd = pro.cargar_productos();
            totalProducto = mibd.Tables["PRODUCTO"].Rows.Count;            
            if ( totalProducto > 0)
            {
                habilitar_textos(true);
                habilitar_botones_accion(true);
                btguardar.Enabled = false;
                btcancelar.Enabled = false;
                btnuevo.Select();
                cargar_producto(0);                
                lbRegistro.Text = "Producto " + (contadorProducto + 1) + " de " + totalProducto;                               
            } else {
                habilitar_textos(true);
                habilitar_botones_accion(true);
                btmodificar.Enabled = false;
            }
        }


        private void comboBoxTipoProducto_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Llena el combo box de las tallas de acuerdo al tipoProducto
        /// </summary>
        /// <param name="val"></param>
        public void fillComboBoxTalla()
        {
            List<string> listSizes = new List<string>();
            listSizes.Add("GR");
            listSizes.Add("MG");
            listSizes.Add("L");
            listSizes.Add("ML");
            comboBoxTalla.DataSource = listSizes;
        }


        /// <summary>
        /// Controla el estado del stock. Solo administrador
        /// </summary>
        private void stockState()
        {
            if (refUsuario.getCargo() == "ADMINISTRADOR")
            {
                txtstock.ReadOnly = false;
            }
            else
            {
                txtstock.ReadOnly = true;
            }
        }

       private void btnMarca_Click(object sender, EventArgs e)
        {
            frmMarca frm = new frmMarca();
            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                ///////// MARCA
                comboBoxMarca.DataSource = pro.getBrand();
                comboBoxMarca.DisplayMember = "mar_nom";
                comboBoxMarca.ValueMember = "id";
            }
        }

        private void comboBoxMarca_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtdescripcion.Focus();
        }

        
    }
}
