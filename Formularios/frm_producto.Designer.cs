﻿namespace fabisa.Formularios
{
    partial class frm_producto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_producto));
            this.btnuevo = new System.Windows.Forms.Button();
            this.btguardar = new System.Windows.Forms.Button();
            this.btmodificar = new System.Windows.Forms.Button();
            this.btbuscar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.txtdescripcion = new System.Windows.Forms.TextBox();
            this.txtstock = new System.Windows.Forms.TextBox();
            this.btcancelar = new System.Windows.Forms.Button();
            this.gbproducto = new System.Windows.Forms.GroupBox();
            this.comboBoxLocalidad = new System.Windows.Forms.ComboBox();
            this.btnMarca = new System.Windows.Forms.Button();
            this.textBoxCodPer = new System.Windows.Forms.TextBox();
            this.comboBoxMarca = new System.Windows.Forms.ComboBox();
            this.comboBoxTalla = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxTipoProducto = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtprecioventa = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.check = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btinicio = new System.Windows.Forms.Button();
            this.btatras = new System.Windows.Forms.Button();
            this.btsig = new System.Windows.Forms.Button();
            this.btfin = new System.Windows.Forms.Button();
            this.toolTippro = new System.Windows.Forms.ToolTip(this.components);
            this.lbRegistro = new System.Windows.Forms.Label();
            this.notification1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.gbproducto.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnuevo
            // 
            this.btnuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnuevo.Image")));
            this.btnuevo.Location = new System.Drawing.Point(3, 3);
            this.btnuevo.Name = "btnuevo";
            this.btnuevo.Size = new System.Drawing.Size(64, 56);
            this.btnuevo.TabIndex = 1;
            this.toolTippro.SetToolTip(this.btnuevo, "Nuevo");
            this.btnuevo.UseVisualStyleBackColor = true;
            this.btnuevo.Click += new System.EventHandler(this.btnuevo_Click);
            // 
            // btguardar
            // 
            this.btguardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btguardar.Image = ((System.Drawing.Image)(resources.GetObject("btguardar.Image")));
            this.btguardar.Location = new System.Drawing.Point(3, 65);
            this.btguardar.Name = "btguardar";
            this.btguardar.Size = new System.Drawing.Size(64, 53);
            this.btguardar.TabIndex = 2;
            this.toolTippro.SetToolTip(this.btguardar, "Guardar");
            this.btguardar.UseVisualStyleBackColor = true;
            this.btguardar.Click += new System.EventHandler(this.btguardar_Click);
            // 
            // btmodificar
            // 
            this.btmodificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmodificar.Image = ((System.Drawing.Image)(resources.GetObject("btmodificar.Image")));
            this.btmodificar.Location = new System.Drawing.Point(3, 124);
            this.btmodificar.Name = "btmodificar";
            this.btmodificar.Size = new System.Drawing.Size(64, 56);
            this.btmodificar.TabIndex = 3;
            this.toolTippro.SetToolTip(this.btmodificar, "Modificar");
            this.btmodificar.UseVisualStyleBackColor = true;
            this.btmodificar.Click += new System.EventHandler(this.btmodificar_Click);
            // 
            // btbuscar
            // 
            this.btbuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbuscar.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar.Image")));
            this.btbuscar.Location = new System.Drawing.Point(3, 186);
            this.btbuscar.Name = "btbuscar";
            this.btbuscar.Size = new System.Drawing.Size(64, 60);
            this.btbuscar.TabIndex = 4;
            this.toolTippro.SetToolTip(this.btbuscar, "Buscar");
            this.btbuscar.UseVisualStyleBackColor = true;
            this.btbuscar.Click += new System.EventHandler(this.btbuscar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Código :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Descripcion :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Marca :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Stock :";
            // 
            // txtcodigo
            // 
            this.txtcodigo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtcodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcodigo.Location = new System.Drawing.Point(329, 11);
            this.txtcodigo.MaxLength = 1;
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(73, 22);
            this.txtcodigo.TabIndex = 20;
            this.txtcodigo.Visible = false;
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtdescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescripcion.Location = new System.Drawing.Point(154, 127);
            this.txtdescripcion.MaxLength = 150;
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Size = new System.Drawing.Size(229, 22);
            this.txtdescripcion.TabIndex = 10;
            // 
            // txtstock
            // 
            this.txtstock.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstock.Location = new System.Drawing.Point(154, 220);
            this.txtstock.MaxLength = 10;
            this.txtstock.Name = "txtstock";
            this.txtstock.Size = new System.Drawing.Size(73, 22);
            this.txtstock.TabIndex = 14;
            this.txtstock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtstock_KeyPress);
            // 
            // btcancelar
            // 
            this.btcancelar.AutoSize = true;
            this.btcancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(3, 252);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(64, 54);
            this.btcancelar.TabIndex = 5;
            this.toolTippro.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // gbproducto
            // 
            this.gbproducto.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gbproducto.Controls.Add(this.comboBoxLocalidad);
            this.gbproducto.Controls.Add(this.btnMarca);
            this.gbproducto.Controls.Add(this.textBoxCodPer);
            this.gbproducto.Controls.Add(this.comboBoxMarca);
            this.gbproducto.Controls.Add(this.comboBoxTalla);
            this.gbproducto.Controls.Add(this.label9);
            this.gbproducto.Controls.Add(this.comboBoxTipoProducto);
            this.gbproducto.Controls.Add(this.label6);
            this.gbproducto.Controls.Add(this.txtprecioventa);
            this.gbproducto.Controls.Add(this.label8);
            this.gbproducto.Controls.Add(this.check);
            this.gbproducto.Controls.Add(this.label7);
            this.gbproducto.Controls.Add(this.label1);
            this.gbproducto.Controls.Add(this.txtcodigo);
            this.gbproducto.Controls.Add(this.label2);
            this.gbproducto.Controls.Add(this.txtdescripcion);
            this.gbproducto.Controls.Add(this.txtstock);
            this.gbproducto.Controls.Add(this.label3);
            this.gbproducto.Controls.Add(this.label5);
            this.gbproducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbproducto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbproducto.Location = new System.Drawing.Point(115, 51);
            this.gbproducto.Name = "gbproducto";
            this.gbproducto.Size = new System.Drawing.Size(468, 307);
            this.gbproducto.TabIndex = 6;
            this.gbproducto.TabStop = false;
            this.gbproducto.Text = "Producto";
            // 
            // comboBoxLocalidad
            // 
            this.comboBoxLocalidad.FormattingEnabled = true;
            this.comboBoxLocalidad.Items.AddRange(new object[] {
            "BODEGA"});
            this.comboBoxLocalidad.Location = new System.Drawing.Point(238, 28);
            this.comboBoxLocalidad.Name = "comboBoxLocalidad";
            this.comboBoxLocalidad.Size = new System.Drawing.Size(145, 24);
            this.comboBoxLocalidad.TabIndex = 8;
            // 
            // btnMarca
            // 
            this.btnMarca.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnMarca.Location = new System.Drawing.Point(391, 92);
            this.btnMarca.Name = "btnMarca";
            this.btnMarca.Size = new System.Drawing.Size(55, 23);
            this.btnMarca.TabIndex = 21;
            this.btnMarca.Text = "nueva";
            this.btnMarca.UseVisualStyleBackColor = false;
            this.btnMarca.Click += new System.EventHandler(this.btnMarca_Click);
            // 
            // textBoxCodPer
            // 
            this.textBoxCodPer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxCodPer.Location = new System.Drawing.Point(154, 28);
            this.textBoxCodPer.MaxLength = 10;
            this.textBoxCodPer.Name = "textBoxCodPer";
            this.textBoxCodPer.Size = new System.Drawing.Size(78, 22);
            this.textBoxCodPer.TabIndex = 7;
            // 
            // comboBoxMarca
            // 
            this.comboBoxMarca.FormattingEnabled = true;
            this.comboBoxMarca.Location = new System.Drawing.Point(154, 93);
            this.comboBoxMarca.Name = "comboBoxMarca";
            this.comboBoxMarca.Size = new System.Drawing.Size(229, 24);
            this.comboBoxMarca.TabIndex = 9;
            this.comboBoxMarca.SelectionChangeCommitted += new System.EventHandler(this.comboBoxMarca_SelectionChangeCommitted);
            // 
            // comboBoxTalla
            // 
            this.comboBoxTalla.FormattingEnabled = true;
            this.comboBoxTalla.Location = new System.Drawing.Point(154, 156);
            this.comboBoxTalla.Name = "comboBoxTalla";
            this.comboBoxTalla.Size = new System.Drawing.Size(229, 24);
            this.comboBoxTalla.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "Unidad de Medida :";
            // 
            // comboBoxTipoProducto
            // 
            this.comboBoxTipoProducto.FormattingEnabled = true;
            this.comboBoxTipoProducto.Location = new System.Drawing.Point(154, 58);
            this.comboBoxTipoProducto.Name = "comboBoxTipoProducto";
            this.comboBoxTipoProducto.Size = new System.Drawing.Size(229, 24);
            this.comboBoxTipoProducto.TabIndex = 8;
            this.comboBoxTipoProducto.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTipoProducto_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 16);
            this.label6.TabIndex = 17;
            this.label6.Text = "Tipo de Mercancia :";
            // 
            // txtprecioventa
            // 
            this.txtprecioventa.Location = new System.Drawing.Point(154, 188);
            this.txtprecioventa.Name = "txtprecioventa";
            this.txtprecioventa.Size = new System.Drawing.Size(73, 22);
            this.txtprecioventa.TabIndex = 13;
            this.txtprecioventa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprecioventa_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 16);
            this.label8.TabIndex = 16;
            this.label8.Text = "Precio de Venta:";
            // 
            // check
            // 
            this.check.AutoSize = true;
            this.check.Location = new System.Drawing.Point(329, 225);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(15, 14);
            this.check.TabIndex = 15;
            this.check.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(274, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Iva:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.flowLayoutPanel1.Controls.Add(this.btnuevo);
            this.flowLayoutPanel1.Controls.Add(this.btguardar);
            this.flowLayoutPanel1.Controls.Add(this.btmodificar);
            this.flowLayoutPanel1.Controls.Add(this.btbuscar);
            this.flowLayoutPanel1.Controls.Add(this.btcancelar);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(27, 52);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(71, 312);
            this.flowLayoutPanel1.TabIndex = 26;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btinicio);
            this.flowLayoutPanel2.Controls.Add(this.btatras);
            this.flowLayoutPanel2.Controls.Add(this.btsig);
            this.flowLayoutPanel2.Controls.Add(this.btfin);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(200, 371);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(264, 56);
            this.flowLayoutPanel2.TabIndex = 27;
            // 
            // btinicio
            // 
            this.btinicio.Image = ((System.Drawing.Image)(resources.GetObject("btinicio.Image")));
            this.btinicio.Location = new System.Drawing.Point(3, 3);
            this.btinicio.Name = "btinicio";
            this.btinicio.Size = new System.Drawing.Size(59, 53);
            this.btinicio.TabIndex = 16;
            this.toolTippro.SetToolTip(this.btinicio, "Inicio");
            this.btinicio.UseVisualStyleBackColor = true;
            this.btinicio.Click += new System.EventHandler(this.btinicio_Click);
            // 
            // btatras
            // 
            this.btatras.Image = ((System.Drawing.Image)(resources.GetObject("btatras.Image")));
            this.btatras.Location = new System.Drawing.Point(68, 3);
            this.btatras.Name = "btatras";
            this.btatras.Size = new System.Drawing.Size(60, 53);
            this.btatras.TabIndex = 17;
            this.toolTippro.SetToolTip(this.btatras, "Atras");
            this.btatras.UseVisualStyleBackColor = true;
            this.btatras.Click += new System.EventHandler(this.btatras_Click);
            // 
            // btsig
            // 
            this.btsig.Image = ((System.Drawing.Image)(resources.GetObject("btsig.Image")));
            this.btsig.Location = new System.Drawing.Point(134, 3);
            this.btsig.Name = "btsig";
            this.btsig.Size = new System.Drawing.Size(60, 53);
            this.btsig.TabIndex = 18;
            this.toolTippro.SetToolTip(this.btsig, "Siguiente");
            this.btsig.UseVisualStyleBackColor = true;
            this.btsig.Click += new System.EventHandler(this.btsig_Click);
            // 
            // btfin
            // 
            this.btfin.Image = ((System.Drawing.Image)(resources.GetObject("btfin.Image")));
            this.btfin.Location = new System.Drawing.Point(200, 3);
            this.btfin.Name = "btfin";
            this.btfin.Size = new System.Drawing.Size(59, 53);
            this.btfin.TabIndex = 19;
            this.toolTippro.SetToolTip(this.btfin, "Ultimo");
            this.btfin.UseVisualStyleBackColor = true;
            this.btfin.Click += new System.EventHandler(this.btfin_Click);
            // 
            // lbRegistro
            // 
            this.lbRegistro.AutoSize = true;
            this.lbRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRegistro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbRegistro.Location = new System.Drawing.Point(402, 9);
            this.lbRegistro.Name = "lbRegistro";
            this.lbRegistro.Size = new System.Drawing.Size(143, 24);
            this.lbRegistro.TabIndex = 28;
            this.lbRegistro.Text = "Producto 0 de 0";
            // 
            // notification1
            // 
            this.notification1.Text = "notifyIcon1";
            this.notification1.Visible = true;
            // 
            // frm_producto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(612, 433);
            this.Controls.Add(this.lbRegistro);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.gbproducto);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_producto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GESTION DE PRODUCTO";
            this.Load += new System.EventHandler(this.frm_producto_Load);
            this.gbproducto.ResumeLayout(false);
            this.gbproducto.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnuevo;
        private System.Windows.Forms.Button btguardar;
        private System.Windows.Forms.Button btmodificar;
        private System.Windows.Forms.Button btbuscar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.TextBox txtdescripcion;
        private System.Windows.Forms.TextBox txtstock;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.GroupBox gbproducto;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btinicio;
        private System.Windows.Forms.Button btatras;
        private System.Windows.Forms.Button btsig;
        private System.Windows.Forms.Button btfin;
        private System.Windows.Forms.ToolTip toolTippro;
        private System.Windows.Forms.CheckBox check;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtprecioventa;
        private System.Windows.Forms.Label lbRegistro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxTipoProducto;
        private System.Windows.Forms.ComboBox comboBoxTalla;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxMarca;
        private System.Windows.Forms.TextBox textBoxCodPer;
        private System.Windows.Forms.NotifyIcon notification1;
        private System.Windows.Forms.Button btnMarca;
        private System.Windows.Forms.ComboBox comboBoxLocalidad;
    }
}