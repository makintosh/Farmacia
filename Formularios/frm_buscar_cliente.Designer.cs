﻿namespace fabisa
{
    partial class frm_buscar_cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_buscar_cliente));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.combocriterio = new System.Windows.Forms.ComboBox();
            this.grilladatos = new System.Windows.Forms.DataGridView();
            this.btcancelar = new System.Windows.Forms.Button();
            this.btaceptar = new System.Windows.Forms.Button();
            this.txtcriterio = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btreporte = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grilladatos)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(21, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Criterio a Buscar :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(76, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Buscar :";
            // 
            // combocriterio
            // 
            this.combocriterio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combocriterio.FormattingEnabled = true;
            this.combocriterio.Items.AddRange(new object[] {
            "APELLIDO",
            "CEDULA"});
            this.combocriterio.Location = new System.Drawing.Point(134, 34);
            this.combocriterio.Name = "combocriterio";
            this.combocriterio.Size = new System.Drawing.Size(192, 24);
            this.combocriterio.TabIndex = 2;
            this.combocriterio.SelectionChangeCommitted += new System.EventHandler(this.combocriterio_SelectionChangeCommitted);
            // 
            // grilladatos
            // 
            this.grilladatos.AllowUserToAddRows = false;
            this.grilladatos.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.grilladatos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grilladatos.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grilladatos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grilladatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grilladatos.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grilladatos.Location = new System.Drawing.Point(48, 185);
            this.grilladatos.Name = "grilladatos";
            this.grilladatos.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.grilladatos.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grilladatos.Size = new System.Drawing.Size(642, 238);
            this.grilladatos.TabIndex = 4;
            // 
            // btcancelar
            // 
            this.btcancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(494, 37);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(56, 55);
            this.btcancelar.TabIndex = 7;
            this.toolTip1.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // btaceptar
            // 
            this.btaceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btaceptar.Image = ((System.Drawing.Image)(resources.GetObject("btaceptar.Image")));
            this.btaceptar.Location = new System.Drawing.Point(430, 37);
            this.btaceptar.Name = "btaceptar";
            this.btaceptar.Size = new System.Drawing.Size(58, 55);
            this.btaceptar.TabIndex = 8;
            this.toolTip1.SetToolTip(this.btaceptar, "Aceptar");
            this.btaceptar.UseVisualStyleBackColor = true;
            this.btaceptar.Click += new System.EventHandler(this.btaceptar_Click);
            // 
            // txtcriterio
            // 
            this.txtcriterio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcriterio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcriterio.Location = new System.Drawing.Point(134, 72);
            this.txtcriterio.Name = "txtcriterio";
            this.txtcriterio.Size = new System.Drawing.Size(192, 22);
            this.txtcriterio.TabIndex = 9;
            this.txtcriterio.TextChanged += new System.EventHandler(this.txtcriterio_TextChanged_1);
            // 
            // btreporte
            // 
            this.btreporte.Image = ((System.Drawing.Image)(resources.GetObject("btreporte.Image")));
            this.btreporte.Location = new System.Drawing.Point(556, 37);
            this.btreporte.Name = "btreporte";
            this.btreporte.Size = new System.Drawing.Size(56, 55);
            this.btreporte.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btreporte, "Reporte");
            this.btreporte.UseVisualStyleBackColor = true;
            this.btreporte.Click += new System.EventHandler(this.btreporte_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btreporte);
            this.groupBox1.Controls.Add(this.btcancelar);
            this.groupBox1.Controls.Add(this.txtcriterio);
            this.groupBox1.Controls.Add(this.btaceptar);
            this.groupBox1.Controls.Add(this.combocriterio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(48, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(642, 108);
            this.groupBox1.TabIndex = 66;
            this.groupBox1.TabStop = false;
            // 
            // frm_buscar_cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(737, 467);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grilladatos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_buscar_cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BUSCAR CLIENTE";
            this.Load += new System.EventHandler(this.busquedacliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grilladatos)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox combocriterio;
        private System.Windows.Forms.DataGridView grilladatos;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.Button btaceptar;
        private System.Windows.Forms.TextBox txtcriterio;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btreporte;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}