﻿namespace fabisa.Formularios
{
    partial class frm_addCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_addCliente));
            this.btguardar1 = new System.Windows.Forms.Button();
            this.lbcedula = new System.Windows.Forms.Label();
            this.lbnombre = new System.Windows.Forms.Label();
            this.lbapellido = new System.Windows.Forms.Label();
            this.lbtelefono = new System.Windows.Forms.Label();
            this.lbdireccion = new System.Windows.Forms.Label();
            this.txtcedula = new System.Windows.Forms.TextBox();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.txtapellido = new System.Windows.Forms.TextBox();
            this.txttelefono = new System.Windows.Forms.TextBox();
            this.gpcliente = new System.Windows.Forms.GroupBox();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.gpcliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // btguardar1
            // 
            this.btguardar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btguardar1.Image = ((System.Drawing.Image)(resources.GetObject("btguardar1.Image")));
            this.btguardar1.Location = new System.Drawing.Point(127, 234);
            this.btguardar1.Name = "btguardar1";
            this.btguardar1.Size = new System.Drawing.Size(148, 60);
            this.btguardar1.TabIndex = 0;
            this.btguardar1.Text = "Aceptar";
            this.btguardar1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btguardar1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btguardar1.UseVisualStyleBackColor = true;
            this.btguardar1.Click += new System.EventHandler(this.btguardar1_Click);
            // 
            // lbcedula
            // 
            this.lbcedula.AutoSize = true;
            this.lbcedula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcedula.Location = new System.Drawing.Point(6, 29);
            this.lbcedula.Name = "lbcedula";
            this.lbcedula.Size = new System.Drawing.Size(54, 16);
            this.lbcedula.TabIndex = 2;
            this.lbcedula.Text = "Cédula:";
            // 
            // lbnombre
            // 
            this.lbnombre.AutoSize = true;
            this.lbnombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnombre.Location = new System.Drawing.Point(6, 64);
            this.lbnombre.Name = "lbnombre";
            this.lbnombre.Size = new System.Drawing.Size(60, 16);
            this.lbnombre.TabIndex = 3;
            this.lbnombre.Text = "Nombre:";
            // 
            // lbapellido
            // 
            this.lbapellido.AutoSize = true;
            this.lbapellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbapellido.Location = new System.Drawing.Point(6, 100);
            this.lbapellido.Name = "lbapellido";
            this.lbapellido.Size = new System.Drawing.Size(58, 16);
            this.lbapellido.TabIndex = 4;
            this.lbapellido.Text = "Apellido";
            // 
            // lbtelefono
            // 
            this.lbtelefono.AutoSize = true;
            this.lbtelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtelefono.Location = new System.Drawing.Point(6, 136);
            this.lbtelefono.Name = "lbtelefono";
            this.lbtelefono.Size = new System.Drawing.Size(65, 16);
            this.lbtelefono.TabIndex = 5;
            this.lbtelefono.Text = "Telefono:";
            // 
            // lbdireccion
            // 
            this.lbdireccion.AutoSize = true;
            this.lbdireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdireccion.Location = new System.Drawing.Point(6, 168);
            this.lbdireccion.Name = "lbdireccion";
            this.lbdireccion.Size = new System.Drawing.Size(68, 16);
            this.lbdireccion.TabIndex = 6;
            this.lbdireccion.Text = "Direccion:";
            // 
            // txtcedula
            // 
            this.txtcedula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcedula.Location = new System.Drawing.Point(72, 22);
            this.txtcedula.MaxLength = 13;
            this.txtcedula.Name = "txtcedula";
            this.txtcedula.Size = new System.Drawing.Size(196, 22);
            this.txtcedula.TabIndex = 7;
            this.txtcedula.Validated += new System.EventHandler(this.txtcedula_Validated);
            this.txtcedula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcedula_KeyPress);
            // 
            // txtnombre
            // 
            this.txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnombre.Location = new System.Drawing.Point(72, 57);
            this.txtnombre.MaxLength = 50;
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(196, 22);
            this.txtnombre.TabIndex = 8;
            // 
            // txtapellido
            // 
            this.txtapellido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtapellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtapellido.Location = new System.Drawing.Point(72, 92);
            this.txtapellido.MaxLength = 50;
            this.txtapellido.Name = "txtapellido";
            this.txtapellido.Size = new System.Drawing.Size(196, 22);
            this.txtapellido.TabIndex = 9;
            // 
            // txttelefono
            // 
            this.txttelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttelefono.Location = new System.Drawing.Point(72, 128);
            this.txttelefono.MaxLength = 11;
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(196, 22);
            this.txttelefono.TabIndex = 10;
            this.txttelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelefono_KeyPress);
            // 
            // gpcliente
            // 
            this.gpcliente.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gpcliente.Controls.Add(this.lbcedula);
            this.gpcliente.Controls.Add(this.txtdireccion);
            this.gpcliente.Controls.Add(this.lbnombre);
            this.gpcliente.Controls.Add(this.txttelefono);
            this.gpcliente.Controls.Add(this.lbapellido);
            this.gpcliente.Controls.Add(this.txtapellido);
            this.gpcliente.Controls.Add(this.lbtelefono);
            this.gpcliente.Controls.Add(this.txtnombre);
            this.gpcliente.Controls.Add(this.lbdireccion);
            this.gpcliente.Controls.Add(this.txtcedula);
            this.gpcliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gpcliente.Location = new System.Drawing.Point(38, 30);
            this.gpcliente.Name = "gpcliente";
            this.gpcliente.Size = new System.Drawing.Size(326, 198);
            this.gpcliente.TabIndex = 12;
            this.gpcliente.TabStop = false;
            this.gpcliente.Text = "Cliente";
            // 
            // txtdireccion
            // 
            this.txtdireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdireccion.Location = new System.Drawing.Point(72, 160);
            this.txtdireccion.MaxLength = 300;
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(196, 22);
            this.txtdireccion.TabIndex = 11;
            // 
            // frm_addCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(402, 306);
            this.Controls.Add(this.gpcliente);
            this.Controls.Add(this.btguardar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_addCliente";
            this.Opacity = 0.95;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLIENTE";
            this.Load += new System.EventHandler(this.frm_addCliente_Load);
            this.gpcliente.ResumeLayout(false);
            this.gpcliente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btguardar1;
        private System.Windows.Forms.Label lbcedula;
        private System.Windows.Forms.Label lbnombre;
        private System.Windows.Forms.Label lbapellido;
        private System.Windows.Forms.Label lbtelefono;
        private System.Windows.Forms.Label lbdireccion;
        private System.Windows.Forms.TextBox txtcedula;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.TextBox txtapellido;
        private System.Windows.Forms.TextBox txttelefono;
        private System.Windows.Forms.GroupBox gpcliente;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}