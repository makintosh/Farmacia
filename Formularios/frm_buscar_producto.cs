﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Formularios;
using fabisa.Reportes;

namespace fabisa.Formularios
{
    public partial class frm_buscar_producto : Form
    {
        public frm_buscar_producto(bool t)
        {
            InitializeComponent();
            posta = t;
        }


        private producto obj_pro;
        public int numeroPro = 0;
        bool posta;
        
        public producto getProducto()
        { 
            return obj_pro; 
        }

        public void setProducto(producto pro)
        { 
            this.obj_pro = pro; 
        }


        private void buscarproducto_Load(object sender, EventArgs e)
        {
            obj_pro = new producto();                     
            comboBox1.Text = "DESCRIPCION";
            datosProducto.DataSource = obj_pro.seleccionar_productos();
            headersDataTable();
        }

        private void headersDataTable()
        {
            
            datosProducto.Columns[0].HeaderText = "Código";
            datosProducto.Columns[0].Width = 90;
            datosProducto.Columns[0].ReadOnly = true;

            datosProducto.Columns[1].HeaderText = "Descripcion";
            datosProducto.Columns[1].Width =  400;
            datosProducto.Columns[1].ReadOnly = true;

            datosProducto.Columns[2].HeaderText = "Unidad de medida";
            datosProducto.Columns[2].Width = 100;
            datosProducto.Columns[2].ReadOnly = true;

            datosProducto.Columns[3].HeaderText = "Stock";
            datosProducto.Columns[3].Width = 50;
            datosProducto.Columns[3].ReadOnly = true;

            datosProducto.Columns[4].HeaderText = "P. Venta";
            datosProducto.Columns[4].Width = 50;

            datosProducto.Columns[7].HeaderText = "P. Compra";
            datosProducto.Columns[7].Width = 50;

            datosProducto.Columns["tienda"].Visible = false;
            datosProducto.Columns["codigo"].Visible = false;            
            datosProducto.Columns["iva_pro"].Visible = false;
            //datosProducto.Columns["p_compra"].Visible = true;
            txtitems.Text = datosProducto.Rows.Count.ToString();
            txtbuscar.Select();
            
        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text.Equals("DESCRIPCION"))
            {
                datosProducto.DataSource = obj_pro.buscarporFiltros(txtbuscar.Text,"producto");             
            }

            if(comboBox1.Text.Equals("CODIGO"))
            {
                datosProducto.DataSource = obj_pro.buscarporFiltros(txtbuscar.Text, "unic_ode");               
            }
            
            txtitems.Text = datosProducto.Rows.Count.ToString();
        }

        private producto nuevoProducto()
        {
            obj_pro.setCodigo(Convert.ToInt16(datosProducto.CurrentRow.Cells["codigo"].Value));
            obj_pro.setDescripcion(datosProducto.CurrentRow.Cells["descripcion"].Value.ToString() + " " + datosProducto.CurrentRow.Cells["talla"].Value.ToString());
            obj_pro.setCodigoIdentificador(datosProducto.CurrentRow.Cells["unic_ode"].Value.ToString()); // id
            obj_pro.setPrecioVenta(double.Parse(datosProducto.CurrentRow.Cells["p_venta"].Value.ToString()));
            obj_pro.setUnidades(Convert.ToInt32(datosProducto.CurrentRow.Cells["stock"].Value));
            obj_pro.setIva(bool.Parse(datosProducto.CurrentRow.Cells["iva_pro"].Value.ToString()));
            obj_pro.setPrecioCompra(double.Parse(datosProducto.CurrentRow.Cells["p_compra"].Value.ToString()));
            return obj_pro;
        }

        public void datosaformulario() 
        {
            if (datosProducto.RowCount > 0)
            {
                nuevoProducto();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else 
            {
                MessageBox.Show("PRODUCTO NO ENCONTRADO", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btcancelar_Click(object sender, EventArgs e)
        {
            this.Close();           
        }

        private void btaceptar_Click(object sender, EventArgs e)
        {
            datosaformulario();
            
        }

        private void data_DoubleClick(object sender, EventArgs e)
        {            
            datosaformulario();          
            
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtbuscar.Focus();
        }

        private void btreporte_Click(object sender, EventArgs e)
        {
            dsReporte ds = new dsReporte();
            byte contadorProductos = 0;
            while (contadorProductos < datosProducto.Rows.Count)
            {
                ds.Tables["dtProducto"].Rows.Add(datosProducto.Rows[contadorProductos].Cells["unic_ode"].Value, datosProducto.Rows[contadorProductos].Cells["descripcion"].Value, datosProducto.Rows[contadorProductos].Cells["talla"].Value, datosProducto.Rows[contadorProductos].Cells["p_venta"].Value, datosProducto.Rows[contadorProductos].Cells["stock"].Value);
                contadorProductos++;

            }

            frm_Reporte_Productos frmRProducto = new frm_Reporte_Productos(ds);
            frmRProducto.ShowDialog();
        
         }

        private void datosProducto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try {
                // purchase price column
                if (datosProducto.CurrentCell.ColumnIndex == 7) {
                    double newPurchasePrice = double.Parse(datosProducto.CurrentRow.Cells["p_compra"].Value.ToString());
                    obj_pro.actualizarPrecioCompra(newPurchasePrice, 
                        Convert.ToInt32(datosProducto.CurrentRow.Cells["codigo"].Value));
                }  
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }                  
        }

        /// <summary>
        /// Pinta fila conforme a una condicion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void datosProducto_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if(!(bool.Parse(datosProducto.Rows[e.RowIndex].Cells["tienda"].Value.ToString()))) // si NO estan en la tienda se colorean
            {
                datosProducto.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.CornflowerBlue;
            }
        }

    }
}
