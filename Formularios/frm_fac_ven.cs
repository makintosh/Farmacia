﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using System.Data.OleDb;
using System.Data.ProviderBase;
using fabisa.Conexion;
using fabisa.Reportes;
using fabisa.Formularios;
using fabisa.Utils;

namespace fabisa.Formularios
{
    public partial class frm_fac_ven : Form
    {
        
        fac_venta obj_facv = new fac_venta();//refencia a factura_venta
        producto obj_pro = new producto();//referencia a producto
        cliente obj_cli = new cliente();//referencia a cliente
        usuario obj_usu = new usuario();// referencia a usuario

        DataSet tablabd = new DataSet();//     
        DataSet tablapro = new DataSet();
        // autocompletar
        DataTable dt_producto = new DataTable();
        DataTable dt_cliente = new DataTable();

        int flagBorrar = 0;
        int flag_factura = 0;        
        
        // constructor que recibe la cedula de usuario como parametro.
        public frm_fac_ven(string cedulaUsuario){
            InitializeComponent();
            obj_usu.setCedula(cedulaUsuario);
        }

        private void autocompletarProducto()
        {
            dt_producto = obj_facv.autocompletar_producto_facturaventa();
            autocompletarTxt_pro();
        }

        private void autcompleteClient() {
            dt_cliente = obj_facv.autocompletar_cliente();
            autocompletarTxt_cli();
        }
        
        /// <summary>
        /// Hook gets called when module is initialized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void facturaventa_Load(object sender, EventArgs e)
        {
            cajas_readonly(true);
            btfactura.Enabled = false;
            btcancelar.Enabled = false;
            btborrar.Enabled = false;
            btbuscar_cli.Enabled = false;
            btbuscar_pro.Enabled = false;
            btreporte.Enabled = false;
            btnAnular.Enabled = false;
            btverfacturas.Enabled = true;
            btnAdd.Enabled = false;
            flagBorrar = 1;
            setEmployeeData();
            autocompletarProducto();
        }

        private void btverfacturas_Click(object sender, EventArgs e)
        {
            frm_facturas_emitidas frmFacEmi = new frm_facturas_emitidas();
            DialogResult resultado = frmFacEmi.ShowDialog();
            if (resultado == DialogResult.OK){
                tablaproducto.DataSource = null;
                searchInvoiceById(frmFacEmi.numeroFactura);
                cajas_readonly(true);
            }
        }

        // BUSCAR CLIENTE
        private void btbuscar_cli_Click(object sender, EventArgs e)
        {

            frm_buscar_cliente bus_cli = new frm_buscar_cliente(1);
            DialogResult resultado = bus_cli.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                txtcedula.Text = bus_cli.getCliente().getCedula();
                txtcliente.Text = bus_cli.getCliente().getNombre() +" "+ bus_cli.getCliente().getApellido();
                txtdireccion.Text = bus_cli.getCliente().getDireccion();
                txttel.Text = bus_cli.getCliente().getTelefono();
                txtproducto.Focus();
            }

        }

        //BUSCAR PRODUCTO
        private void btbuscar_pro_Click(object sender, EventArgs e)
        {
            frm_buscar_producto bus_pro = new frm_buscar_producto(true);
            DialogResult respuesta = bus_pro.ShowDialog();
            if (respuesta == DialogResult.OK)
            {
                obj_pro.setCodigo(bus_pro.getProducto().getCodigo());
                obj_pro.setDescripcion(bus_pro.getProducto().getDescripcion());
                obj_pro.setPrecioVenta(bus_pro.getProducto().getPrecioVenta());
                obj_pro.setIva(bus_pro.getProducto().getIva());
                obj_pro.setCodigoIdentificador(bus_pro.getProducto().getCodigoIdentificador());
                enviarProductoAlDetalle();
            }  
        }

        //FACTURA
        private void btfactura_Click(object sender, EventArgs e)
        {
            try {
                if ((txtcedula.TextLength >= 10 && txtcedula.TextLength <= 13) 
                    && txtsubtotal.Text.Length > 0 
                    && comboBoxEmployee.SelectedIndex != 0) {
                    flagBorrar = 0;
                    flag_factura = 0;

                    for (int i = 0; i < tablaproducto.Rows.Count; i++)
                    {
                        if (tablaproducto.Rows[i].Cells["Cantidad"].Value == null) {
                            MessageBox.Show("INGRESA LA CANTIDAD DE " + tablaproducto.Rows[i].Cells["producto"].Value.ToString());
                            flag_factura = 1;
                            i = tablaproducto.Rows.Count;
                        }
                    }

                    if (flag_factura == 0)
                    {
                        obj_facv.guardar(nueva_factura(), txtcedula.Text, obj_usu.getCedula(), dtfecha.Value);
                        detalleFactura obj_detalle_factura = new detalleFactura();
                        for (int t = 0; t < tablaproducto.Rows.Count; t++)
                        {
                            obj_detalle_factura.setCantidad(Convert.ToInt16(tablaproducto.Rows[t].Cells["cantidad"].Value)); // cantidad
                            obj_pro.setCodigo(Convert.ToInt32(tablaproducto.Rows[t].Cells["codigo"].Value));// codigo                      
                            obj_detalle_factura.setTotal(Convert.ToDouble(tablaproducto.Rows[t].Cells["total"].Value)); // total
                            obj_detalle_factura.setCurrentPrice(Convert.ToDouble(tablaproducto.Rows[t].Cells["precio"].Value));
                            obj_facv.guardar_detalle_factura(obj_detalle_factura.getTotal(),
                                obj_detalle_factura.getCantidad(),
                                obj_facv.getNumeroFactura(),
                                obj_pro.getCodigo(),
                                obj_detalle_factura.getCurrentPrice());

                        }
                        cajas_readonly(true);
                        txtdescuento.Visible = false;
                        habilitar_botones_accion(true);
                        tablaproducto.ReadOnly = true;
                        comboBoxEmployee.Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show("Por favor verificar los campos vendedor, cédula y los totales", 
                        "Información", 
                        MessageBoxButtons.OK, 
                        MessageBoxIcon.Information);
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error al guardar la factura", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }

        //BOTON REPORTES
        private void btreporte_Click(object sender, EventArgs e)
        {
            dsReporte ds = new dsReporte();  
            
            
            // carga datos de la tabla factura conforme al DataSet
            ds.Tables["dtFactura"].Rows.Add(txtnumerofac.Text, dtfecha.Value.ToLongDateString(), 
                txtsubtotal.Text, txtiva.Text, txtivacero.Text,
                txttotal.Text, comboBoxEmployee.Text,textboxdes.Text);

            //carga datos de la tabla detalle
            for (int i = 0; i < tablaproducto.RowCount; i++)
            {
                ds.Tables["dtDetalle"].Rows.Add(
                    tablaproducto.Rows[i].Cells["id"].Value,tablaproducto.Rows[i].Cells["producto"].Value, 
                    tablaproducto.Rows[i].Cells["cantidad"].Value, 
                    tablaproducto.Rows[i].Cells["precio"].Value,
                    tablaproducto.Rows[i].Cells["total"].Value);
            }

            // carga datos del cliente
            ds.Tables["dtCliente"].Rows.Add(txtcliente.Text,null, txttel.Text, txtdireccion.Text, txtcedula.Text);

            Reportes_Fventas frm_reportes = new Reportes_Fventas(ds);
            frm_reportes.Show();          
        }


        // BOTON CANCELAR
        private void btcancelar_Click(object sender, EventArgs e)
        {
            limpiar_cajas_factura();
            tablaproducto.DataSource = null;
            tablaproducto.Columns.Clear();
            habilitar_botones_accion(true);
            cajas_readonly(true);
            comboBoxEmployee.Enabled = false;

            if (flagBorrar != 0) {
                txtnumerofac.Clear();
                btreporte.Enabled = false;
                btverfacturas.Enabled = true;
                btnAnular.Enabled = false;
            }
        }


        private void dibujarTablaProducto()
        {
            tablaproducto.Columns.Add("codigo", "codigo"); // codigo oculto 0
            tablaproducto.Columns.Add("id", "codigo"); // identificador 1
            tablaproducto.Columns.Add("producto", "producto");// 2
            tablaproducto.Columns.Add("cantidad", "cantidad");// 3
            tablaproducto.Columns.Add("precio", "precio");// 4
            tablaproducto.Columns.Add("total", "total");// 5
            tablaproducto.Columns.Add("iva", "iva");// 6

            tablaproducto.Columns["iva"].Visible = false; //iva 
            tablaproducto.Columns["codigo"].Visible = false; //codigo
            tablaproducto.Columns["producto"].Width = 180;

            tablaproducto.Columns["codigo"].ReadOnly = true;
            tablaproducto.Columns["producto"].ReadOnly = true;
            tablaproducto.Columns["total"].ReadOnly = true;

            //tablaproducto.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter;
        }


        /// <summary>
        /// Accion Boton Nueva Factura
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnuevfac_Click(object sender, EventArgs e)
        {
            tablaproducto.ReadOnly = false;
            cajas_readonly(false);
            obj_facv.setNumeroFactura(obj_facv.contar_max() + 1);
            txtnumerofac.Text = obj_facv.getNumeroFactura().ToString();
            dtfecha.Value = DateTime.Now;
            limpiar_cajas_factura();
            
            tablaproducto.DataSource = null;
            tablaproducto.Rows.Clear();
            dibujarTablaProducto();
            
            txtcedula.Focus();                                                     
            habilitar_botones_accion(false);// botones de accion
            btborrar.Enabled = false;
            btfactura.Enabled = false;
            textboxdes.Visible = true;
            txtdescuento.Visible = true;
            txtdescuento.Value = 0;
            txtSearchInvoice.Text = "";
            lbAnular.Text = "Activa";
            comboBoxEmployee.Enabled = true;
            comboBoxEmployee.SelectedIndex = 0;
        }


        //autocompleta producto
        private void autocompletarTxt_pro()
        {
            //Asignar propiedades para el autocompletado
            this.txtproducto.AutoCompleteSource = AutoCompleteSource.CustomSource;
            this.txtproducto.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            
            //Llenar el textbox con los datos
            foreach (DataRow dr in dt_producto.Rows)
            {
                this.txtproducto.AutoCompleteCustomSource.Add(dr["PRODUCTO"].ToString());                
            }
            
        }

        //autocompleta el textbox de cliente
        private void autocompletarTxt_cli()
        {
            //Asignar propiedades para el autocompletado
            this.txtcedula.AutoCompleteSource = AutoCompleteSource.CustomSource;
            this.txtcedula.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            //Llenar el textbox con los datos
            foreach (DataRow dr in dt_cliente.Rows)
            {
                this.txtcedula.AutoCompleteCustomSource.Add(dr["cedula_per"].ToString());
                
            }
            
        }

        
        /// <summary>
        /// Load an invoice into UI by numero_fac
        /// </summary>
        /// <param name="num">numero_fac</param>
        private void loadInvoice(int num)
        {
            try {
                DataRowCollection row = tablabd.Tables["FACTURA"].Rows;
                if (row.Count > 0)
                {
                    tablaproducto.DataSource = null;
                    tablaproducto.Columns.Clear();
                    txtdescuento.Visible = false;
                    textboxdes.Visible = true;
                    txtnumerofac.Text = row[num]["numero"].ToString();
                    txtcedula.Text = row[num]["cedula_cli"].ToString();
                    txtcliente.Text = row[num]["nombre"].ToString() + " " + row[num]["apellido"].ToString(); ;
                    txttel.Text = row[num]["telefono"].ToString();
                    txtdireccion.Text = row[num]["direccion"].ToString();
                    obj_facv.setFechaFactura(DateTime.Parse(row[num]["fecha"].ToString()));//
                    dtfecha.Value = obj_facv.getFechaFactura();
                    obj_facv.setFechaFactura(DateTime.Parse(row[num]["hora"].ToString()));

                    tablaproducto.DataSource = obj_facv.cargar_detalle_facturas(int.Parse(txtnumerofac.Text)); // fill el detalle de la factura
                    ordenarHeadersTablaDetalle();


                    obj_facv.setSubtotal(double.Parse( row[num]["subtotal"].ToString()));
                    obj_facv.setDescuento_fac(double.Parse(row[num]["descuento"].ToString()));
                    obj_facv.setTotalFactura(double.Parse(row[num]["total"].ToString()));
                    obj_facv.setIva(double.Parse(row[num]["iva"].ToString()));

                    txtsubtotal.Text = obj_facv.getSubtotal().ToString("0.00"); // subtotal
                    txtiva.Text = obj_facv.getIva().ToString("0.00"); // iva
                    txtivacero.Text = "0.00"; // iva cero
                    textboxdes.Text = obj_facv.getDescuento_fac().ToString("0.00"); //descuento
                    txttotal.Text = obj_facv.getTotalFactura().ToString("0.00"); // total

                    
                    lbAnular.Text = row[num]["estado"].ToString();
                    txtitems.Text = tablaproducto.Rows.Count.ToString();
                    
                    if (lbAnular.Text.Equals("False")){
                        btnAnular.Enabled = false;
                        lbAnular.Text = "Anulada";
                    }
                    else{
                        btnAnular.Enabled = true;
                        lbAnular.Text = "Activa";
                    }
                    tablaproducto.ReadOnly = true;
                    comboBoxEmployee.SelectedIndex = getIndexByValue(row[num]["cedula_usu"].ToString());
                    comboBoxEmployee.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Returns comboBox item index searching by value
        /// </summary>
        /// <param name="itemValue"></param>
        /// <returns></returns>
        private int getIndexByValue(string itemValue) {
            int index = 0;
            for (int i = 0; i < comboBoxEmployee.Items.Count; i++) {
                DataRowView r = (DataRowView)comboBoxEmployee.Items[i];
                if (itemValue.Equals(r.Row.ItemArray[1])) {
                    index = i;
                    i = comboBoxEmployee.Items.Count;
                } 
            }
            return index;
        }


        private void ordenarHeadersTablaDetalle()
        {
            tablaproducto.Columns[0].Visible = false; // oculta la columna de codigo
            

            tablaproducto.Columns[1].HeaderText = "Codigo";
            tablaproducto.Columns[1].Width = 50;

            tablaproducto.Columns[2].HeaderText = "Producto";
            tablaproducto.Columns[2].Width = 200;
            
            tablaproducto.Columns[3].HeaderText = "Cantidad";
            tablaproducto.Columns[3].Width = 100;

            tablaproducto.Columns[4].HeaderText = "Precio";
            tablaproducto.Columns[4].Width = 100;

            tablaproducto.Columns[5].HeaderText = "Total";
            tablaproducto.Columns[5].Width = 50;
        }
   
        //cargar el producto y enviarlo al la tabla
        private void enviarProductoAlDetalle()
        {
            
            int flag1=0;
            btfactura.Enabled = true;
            btborrar.Enabled = true;
            if (tablaproducto.Rows.Count == 0)//primer producto
            {
                tablaproducto.Rows.Add(obj_pro.getCodigo(), obj_pro.getCodigoIdentificador(),obj_pro.getDescripcion(), null, obj_pro.getPrecioVenta(), null,obj_pro.getIva());
                tablaproducto.CurrentCell = tablaproducto[3, 0];                         
                btfactura.Enabled = true;
                btborrar.Enabled = true;
                tablaproducto.Focus();
   
            }

            
            else//ya hay un producto en la lista
            {
                // proceso que verifica que el producto no se encuentre en la grilla de datos.
                for (int q = 0; q < tablaproducto.Rows.Count; q++)
                {

                    if (obj_pro.getCodigo().ToString().Equals(Convert.ToString(tablaproducto.Rows[q].Cells[0].Value))) //evalua si ya se encuentra
                    {
                        MessageBox.Show("El producto ya se encuentra en lista modifica la cantidad");
                        flag1 = tablaproducto.Rows.Count;
                        
                    }                   

                }
                
                if(flag1 ==0)
                    
                {
                        tablaproducto.Rows.Add(obj_pro.getCodigo(),obj_pro.getCodigoIdentificador() ,obj_pro.getDescripcion(), null, obj_pro.getPrecioVenta(), null,obj_pro.getIva());
                        tablaproducto.CurrentCell = tablaproducto[3, tablaproducto.RowCount - 1]; // coloca el foco en la cantidad
                        tablaproducto.Focus();                      
                        
                }
            }

            txtitems.Text = tablaproducto.Rows.Count.ToString();
            
        }
        
        private fac_venta nueva_factura()
        {
               
               obj_facv.setNumeroFactura(Convert.ToInt32(txtnumerofac.Text));
               obj_facv.setFechaFactura(DateTime.Now);
               obj_facv.setSubtotal(Convert.ToDouble(txtsubtotal.Text));
               obj_facv.setDescuento_fac(Convert.ToDouble(textboxdes.Text));
               obj_facv.setIvaCero(Convert.ToDouble(txtivacero.Text));
               obj_facv.setIva(Convert.ToDouble(txtiva.Text));                
               obj_facv.setTotalFactura(Convert.ToDouble(txttotal.Text));
               return obj_facv;
        }

        private void cajas_readonly(bool t)
        {
            txtcedula.ReadOnly = t;
            txtcliente.ReadOnly = t;
            txtitems.ReadOnly = t;
            txtivacero.ReadOnly = t;
            txtdireccion.ReadOnly = t;            
            txttotal.ReadOnly = t;
            txtnumerofac.ReadOnly = t;
            txtproducto.ReadOnly = t;
            txtsubtotal.ReadOnly = t;
            txttel.ReadOnly = t;
            txtiva.ReadOnly = t;
            textboxdes.ReadOnly = t;
            dtfecha.Enabled = !t;

        }

        //limpiar las cajas de texto
        private void limpiar_cajas_factura()
        {
            
            txtcedula.Clear();
            txtitems.Clear();
            txtcliente.Clear();
            txtdireccion.Clear();
            txttel.Clear();
            //txtcodp.Clear();
            txtproducto.Clear();
            //txtprecio.Clear();
            txtsubtotal.Clear();
            txtivacero.Clear();
            txttotal.Clear();
            txtiva.Clear();
            textboxdes.Clear();
        }

        private void habilitar_botones_accion(bool t)
        {
            btnuevfac.Enabled = t;
            btnAnular.Enabled = t;
            btreporte.Enabled = t;
            btverfacturas.Enabled = t;
            btborrar.Enabled = !t;
            btcancelar.Enabled = !t;
            btfactura.Enabled = !t;
            btbuscar_cli.Enabled = !t;
            btnAdd.Enabled = !t;
            btbuscar_pro.Enabled = !t;
            btnSearchInvoice.Enabled = t;
        }

        /// <summary>
        /// Metodo que retorna en un arreglo de enteros los indices de las filas seleccionadas
        /// </summary>
        /// <param name="numberRowsSelected"></param> numero de Filas seleccionadas en el datagridview
        /// <returns></returns>
        private int[] getIndexOfSelectedRows(int numberRowsSelected)
        {
            int [] numberRowsBack = new int[numberRowsSelected];
            numberRowsSelected = 0;
            for (int i = 0; i < tablaproducto.Rows.Count; i++)
            {
                if (tablaproducto.Rows[i].Selected)
                {
                    numberRowsBack[numberRowsSelected++] = tablaproducto.Rows[i].Index;
                }
            }
            return numberRowsBack;
        }

        private void btborrar_Click(object sender, EventArgs e)
        {
            if (tablaproducto.SelectedRows.Count > 0)
            {
                if (tablaproducto.Rows.Count != 1)
                {
                    int[] indexOfRowsSelected = this.getIndexOfSelectedRows(tablaproducto.SelectedRows.Count);
                    byte contadorArreglo = byte.Parse(indexOfRowsSelected.Length.ToString());
                    byte contador = 0;

                    do
                    {
                        tablaproducto.Rows.RemoveAt(indexOfRowsSelected[--contadorArreglo]);
                        contador++;

                    } while (contador < indexOfRowsSelected.Length);

                    sumas_factura();
                    //txtcodp.Clear();
                    txtproducto.Clear();
                    //txtprecio.Clear();
                    txtproducto.Focus();
                }
                else
                {
                    btborrar.Enabled = false;
                    btfactura.Enabled = false;
                    tablaproducto.Rows.Remove(tablaproducto.CurrentRow);
                    sumas_factura();
                    txtproducto.Focus();

                }
                txtitems.Text = tablaproducto.Rows.Count.ToString();
            }
            else 
            {
                MessageBox.Show("NINGUNA FILA SELECCIONADA", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        /// <summary>
        /// Event handler at tablaproducto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tablaproducto_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {       
            try
            {
                int currentQuantity = Convert.ToInt32(tablaproducto.CurrentRow.Cells["cantidad"].Value);
                // Price column 
                if (tablaproducto.CurrentCell.ColumnIndex == 4) {
                    calculateSubtotal(currentQuantity);
                    txtproducto.Focus();
                }
                
                // Quantity column
                if (tablaproducto.CurrentCell.ColumnIndex == 3)
                {
                    obj_pro.setUnidades(obj_facv.saber_stock(Convert.ToInt32(tablaproducto.CurrentRow.Cells["codigo"].Value)));// extrae el stock del producto de la BD
                    if (obj_pro.getUnidades() >= currentQuantity) {
                        if (currentQuantity > 0) {
                            txtproducto.Text = null;
                            calculateSubtotal(currentQuantity);
                            tablaproducto.CurrentCell = tablaproducto["precio", tablaproducto.CurrentRow.Index];
                        }
                        else {
                            MessageBox.Show("LA CANTIDAD DEBE SER MAYOR QUE 0", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            tablaproducto.CurrentCell.Value = null;
                        }
                    }
                    else {
                        MessageBox.Show("NO HAY STOCK SUFICIENTE HAY " + obj_pro.getUnidades().ToString() + " UNIDADES DE " + tablaproducto.CurrentRow.Cells["producto"].Value.ToString(), "VERIFICAR", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        tablaproducto.CurrentCell.Value = null;
                    }   
                }
            }catch {
                MessageBox.Show("SOLO SE ACEPTAN NUMEROS ENTEROS POSITIVOS!!");
                tablaproducto.CurrentCell.Value = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private void calculateSubtotal(int currentQuantity) {
            double total = currentQuantity * Convert.ToDouble(tablaproducto.CurrentRow.Cells["precio"].Value);
            tablaproducto.CurrentRow.Cells["total"].Value = total.ToString("0.00");
            sumas_factura();
        }
        
        /// <summary>
        /// Sum totals logic
        /// </summary>
        private void sumas_factura()
        {
            try
            {
                obj_facv.setSubtotal(0);
                obj_facv.setIva(0);
                for (int r = 0; r < tablaproducto.Rows.Count; r++)//suma los totales de los productos
                {
                    double rowTotal = Convert.ToDouble(tablaproducto.Rows[r].Cells["total"].Value);
                    // if the current product has IVA
                    if (bool.Parse(tablaproducto.Rows[r].Cells["iva"].Value.ToString())) {
                        double currentSubtotalWithoutIVA = (rowTotal * 100) / (parameters.IVA + 100); 
                        obj_facv.setSubtotal(obj_facv.getSubtotal() + currentSubtotalWithoutIVA); 
                        obj_facv.setIva( ((currentSubtotalWithoutIVA * parameters.IVA) / 100) + obj_facv.getIva()); // iva
                    } else {
                        obj_facv.setSubtotal(obj_facv.getSubtotal() + rowTotal );
                    }
                }
                obj_facv.setDescuento_fac((obj_facv.getSubtotal() * short.Parse(txtdescuento.Value.ToString())) / 100);
                obj_facv.setTotalFactura((obj_facv.getSubtotal() - obj_facv.getDescuento_fac()) + obj_facv.getIva());
                txtsubtotal.Text = obj_facv.getSubtotal().ToString("0.00"); // subtotal                         
                textboxdes.Text = obj_facv.getDescuento_fac().ToString("0.00");// descuento
                txtivacero.Text = 0.ToString("0.00");            // iva 0%
                txtiva.Text = obj_facv.getIva().ToString("0.00");// iva 12%
                txttotal.Text = obj_facv.getTotalFactura().ToString("0.00");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
     // Evento cuando aplasto enter en el textbox       
     private void enter_txtproducto(object sender, KeyEventArgs e)

       {
           try
           {
               if (e.KeyCode == Keys.Enter) {
                   tablapro = obj_facv.completar_producto_factura_venta(txtproducto.Text);
                   if (tablapro.Tables["producto"].Rows.Count != 0) {
                       //txtcodp.Text = tablapro.Tables["producto"].Rows[0]["Codigo"].ToString();
                       obj_pro.setCodigo(int.Parse(tablapro.Tables["producto"].Rows[0]["Codigo"].ToString()));//codigo
                       obj_pro.setCodigoIdentificador(tablapro.Tables["producto"].Rows[0]["unic_ode"].ToString());
                       obj_pro.setDescripcion(txtproducto.Text);
                       obj_pro.setPrecioVenta(double.Parse(tablapro.Tables["producto"].Rows[0]["Precio"].ToString()));
                       //txtprecio.Text = tablapro.Tables["producto"].Rows[0]["Precio"].ToString();
                       obj_pro.setIva(bool.Parse(tablapro.Tables["producto"].Rows[0]["iva_pro"].ToString()));
                       this.enviarProductoAlDetalle();
                   } else {
                       MessageBox.Show(txtproducto.Text + " NO SE ENCUENTRA EN LA BASE DE DATOS", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                       txtproducto.Clear();
                       txtproducto.Focus();
                   }
               }
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
                    
       }

     private void enter_txtcedcliente(object sender, KeyEventArgs e)
     {
         if (e.KeyCode == Keys.Enter)
         {
             tablapro = obj_facv.autocompletar_cliente_en_textbox(txtcedula.Text);
             if (tablapro.Tables["cliente"].Rows.Count != 0)
             {
                 txtcliente.Text = tablapro.Tables["cliente"].Rows[0]["Cliente"].ToString();
                 txtdireccion.Text = tablapro.Tables["cliente"].Rows[0]["Direccion"].ToString();
                 txttel.Text = tablapro.Tables["cliente"].Rows[0]["Tel"].ToString();
                 txtproducto.Focus();
             }
             else
             {
                 MessageBox.Show("NO SE ENCUENTRA EN LA BASE DE DATOS","ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
                 txtcedula.Clear();
                 txtcedula.Focus();
             }
         }
     }

     private void txtcedula_KeyPress(object sender, KeyPressEventArgs e)
     {
         if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
         {
             e.Handled = false;
         }
         else
         {
             e.Handled = true;
         }
     }

     private void txtprecio_KeyPress(object sender, KeyPressEventArgs e)
     {
         String Aceptados = "0123456789.," + Convert.ToChar(8);

         if (Aceptados.Contains("" + e.KeyChar))
         { e.Handled = false; }
         else
         { e.Handled = true; }
     }

     private void txttel_KeyPress(object sender, KeyPressEventArgs e)
     {
         if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
         {
             e.Handled = false;
         }
         else
         {
             e.Handled = true;
         }
     }

     private void btnAdd_Click(object sender, EventArgs e)
     {
         frm_addCliente frmNuevo = new frm_addCliente();
         DialogResult resultado = frmNuevo.ShowDialog();

         if (resultado == DialogResult.OK)
         {
             txtcedula.Text = frmNuevo.getCliente().getCedula();
             txtcliente.Text = frmNuevo.getCliente().getNombre() + " " + frmNuevo.getCliente().getApellido();
             txttel.Text = frmNuevo.getCliente().getTelefono();
             txtdireccion.Text = frmNuevo.getCliente().getDireccion();
             dt_cliente = obj_facv.autocompletar_cliente(); // rellena el datatable con los clientes actualizados
             txtproducto.Focus();
         }
     }

     private void btnAnular_Click(object sender, EventArgs e)
     {
         DialogResult anular = MessageBox.Show("Desea Anular la Factura ?", "ANULAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
         if (anular == DialogResult.Yes)
         {
             int stockpro = 0;
             detalleFactura obj_detalle_factura = new detalleFactura();
             for (int t = 0; t < tablaproducto.Rows.Count; t++){
                 obj_detalle_factura.setCantidad(Convert.ToInt16(tablaproducto.Rows[t].Cells["cantidad"].Value)); // cantidad
                 obj_pro.setCodigo(Convert.ToInt32(tablaproducto.Rows[t].Cells["codigo"].Value));// codigo
                 stockpro = obj_facv.saber_stock(obj_pro.getCodigo()) + obj_detalle_factura.getCantidad(); // stock                                        
                 obj_facv.modificar_stock(obj_pro.getCodigo(), stockpro);
             }
             obj_facv.anularFactura(int.Parse(txtnumerofac.Text), "factura_venta"); // anula la factura de venta
             limpiar_cajas_factura();
             tablaproducto.DataSource = null;
             tablaproducto.Columns.Clear();
             cajas_readonly(true);
             habilitar_botones_accion(true);
             btnAnular.Enabled = false;
             btreporte.Enabled = false;
         }
     }

     

     private void txtdescuento_ValueChanged(object sender, EventArgs e)
     {
         obj_facv.setDescuento_fac(0);         
         sumas_factura();         
     }
        
    /// <summary>
    /// On click btnSearchInvoice 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnSearchInvoice_Click(object sender, EventArgs e) {
        try {
            searchInvoiceById(int.Parse(txtSearchInvoice.Text));
        }
        catch {
            txtSearchInvoice.Text = "";
            txtSearchInvoice.Focus();
            MessageBox.Show("Ingrese el número de la factura", 
                "Validación", 
                MessageBoxButtons.OK, 
                MessageBoxIcon.Error);
        }
    }
    
    /// <summary>
    /// Search invoice by numero_fac
    /// </summary>
    /// <param name="invoiceId"></param>
    private void searchInvoiceById(int invoiceId) {
        tablabd = obj_facv.getInvoiceById(invoiceId);
        if (tablabd.Tables[0].Rows.Count > 0) {
            loadInvoice(0);
            btreporte.Enabled = true;
        }
        else {
            MessageBox.Show("No existen datos",
            "Información",
            MessageBoxButtons.OK,
            MessageBoxIcon.Information);
        }
    }

    /// <summary>
    /// Set employee data to comboBoxEmployee
    /// </summary>
    private void setEmployeeData()
    {
        DataTable data = obj_facv.cargar_combo_usuario();
        ComboBoxUtil.addNewRowToDataTableAtZero(data);
        comboBoxEmployee.DataSource = data;
        comboBoxEmployee.ValueMember = "Cedula";
        comboBoxEmployee.DisplayMember = "Vendedor";
        comboBoxEmployee.SelectedIndex = 0;
    }

    /// <summary>
    /// On comboBox option changed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void comboBoxEmployee_SelectionChangeCommitted(object sender, EventArgs e) {
        obj_usu.setCedula(comboBoxEmployee.SelectedValue.ToString());
    }
         
  }//penultimo
}//ultimo
