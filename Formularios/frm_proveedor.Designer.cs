﻿namespace fabisa.Formularios
{
    partial class frm_proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_proveedor));
            this.btnuevo = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bteliminar = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.btbuscar = new System.Windows.Forms.Button();
            this.btmodificar = new System.Windows.Forms.Button();
            this.btguardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtrazon = new System.Windows.Forms.TextBox();
            this.txtruc = new System.Windows.Forms.TextBox();
            this.txtciudad = new System.Windows.Forms.TextBox();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.txttel = new System.Windows.Forms.TextBox();
            this.gproveedor = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btinicio = new System.Windows.Forms.Button();
            this.btatras = new System.Windows.Forms.Button();
            this.btsig = new System.Windows.Forms.Button();
            this.btfin = new System.Windows.Forms.Button();
            this.toolTipprovee = new System.Windows.Forms.ToolTip(this.components);
            this.lbRegistro = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.gproveedor.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnuevo
            // 
            this.btnuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnuevo.Image")));
            this.btnuevo.Location = new System.Drawing.Point(0, 3);
            this.btnuevo.Name = "btnuevo";
            this.btnuevo.Size = new System.Drawing.Size(63, 56);
            this.btnuevo.TabIndex = 0;
            this.toolTipprovee.SetToolTip(this.btnuevo, "Nuevo");
            this.btnuevo.UseVisualStyleBackColor = true;
            this.btnuevo.Click += new System.EventHandler(this.btnuevo_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.bteliminar);
            this.panel1.Controls.Add(this.btcancelar);
            this.panel1.Controls.Add(this.btbuscar);
            this.panel1.Controls.Add(this.btmodificar);
            this.panel1.Controls.Add(this.btguardar);
            this.panel1.Controls.Add(this.btnuevo);
            this.panel1.Location = new System.Drawing.Point(21, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(67, 338);
            this.panel1.TabIndex = 1;
            // 
            // bteliminar
            // 
            this.bteliminar.Image = ((System.Drawing.Image)(resources.GetObject("bteliminar.Image")));
            this.bteliminar.Location = new System.Drawing.Point(0, 220);
            this.bteliminar.Name = "bteliminar";
            this.bteliminar.Size = new System.Drawing.Size(63, 58);
            this.bteliminar.TabIndex = 6;
            this.toolTipprovee.SetToolTip(this.bteliminar, "Eliminar");
            this.bteliminar.UseVisualStyleBackColor = true;
            this.bteliminar.Click += new System.EventHandler(this.bteliminar_Click);
            // 
            // btcancelar
            // 
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(1, 278);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(63, 57);
            this.btcancelar.TabIndex = 4;
            this.toolTipprovee.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // btbuscar
            // 
            this.btbuscar.Image = ((System.Drawing.Image)(resources.GetObject("btbuscar.Image")));
            this.btbuscar.Location = new System.Drawing.Point(0, 163);
            this.btbuscar.Name = "btbuscar";
            this.btbuscar.Size = new System.Drawing.Size(63, 57);
            this.btbuscar.TabIndex = 3;
            this.toolTipprovee.SetToolTip(this.btbuscar, "Buscar");
            this.btbuscar.UseVisualStyleBackColor = true;
            this.btbuscar.Click += new System.EventHandler(this.btbuscar_Click);
            // 
            // btmodificar
            // 
            this.btmodificar.Image = ((System.Drawing.Image)(resources.GetObject("btmodificar.Image")));
            this.btmodificar.Location = new System.Drawing.Point(0, 104);
            this.btmodificar.Name = "btmodificar";
            this.btmodificar.Size = new System.Drawing.Size(63, 57);
            this.btmodificar.TabIndex = 2;
            this.toolTipprovee.SetToolTip(this.btmodificar, "Modificar");
            this.btmodificar.UseVisualStyleBackColor = true;
            this.btmodificar.Click += new System.EventHandler(this.btmodificar_Click);
            // 
            // btguardar
            // 
            this.btguardar.Image = ((System.Drawing.Image)(resources.GetObject("btguardar.Image")));
            this.btguardar.Location = new System.Drawing.Point(0, 58);
            this.btguardar.Name = "btguardar";
            this.btguardar.Size = new System.Drawing.Size(63, 47);
            this.btguardar.TabIndex = 1;
            this.toolTipprovee.SetToolTip(this.btguardar, "Guardar");
            this.btguardar.UseVisualStyleBackColor = true;
            this.btguardar.Click += new System.EventHandler(this.btguardar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Razón Social :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "RUC :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Dirección :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Telefono :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(58, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Ciudad :";
            // 
            // txtrazon
            // 
            this.txtrazon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtrazon.Location = new System.Drawing.Point(156, 37);
            this.txtrazon.MaxLength = 200;
            this.txtrazon.Name = "txtrazon";
            this.txtrazon.Size = new System.Drawing.Size(209, 22);
            this.txtrazon.TabIndex = 1;
            // 
            // txtruc
            // 
            this.txtruc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtruc.Location = new System.Drawing.Point(156, 75);
            this.txtruc.MaxLength = 13;
            this.txtruc.Name = "txtruc";
            this.txtruc.Size = new System.Drawing.Size(209, 22);
            this.txtruc.TabIndex = 2;
            this.txtruc.Validated += new System.EventHandler(this.txtruc_Validated);
            this.txtruc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtruc_KeyPress);
            // 
            // txtciudad
            // 
            this.txtciudad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtciudad.Location = new System.Drawing.Point(156, 113);
            this.txtciudad.Name = "txtciudad";
            this.txtciudad.Size = new System.Drawing.Size(209, 22);
            this.txtciudad.TabIndex = 4;
            this.txtciudad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtciudad_KeyPress);
            // 
            // txtdireccion
            // 
            this.txtdireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdireccion.Location = new System.Drawing.Point(156, 151);
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(209, 22);
            this.txtdireccion.TabIndex = 5;
            // 
            // txttel
            // 
            this.txttel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttel.Location = new System.Drawing.Point(156, 189);
            this.txttel.MaxLength = 9;
            this.txttel.Name = "txttel";
            this.txttel.Size = new System.Drawing.Size(209, 22);
            this.txttel.TabIndex = 6;
            this.txttel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttel_KeyPress);
            // 
            // gproveedor
            // 
            this.gproveedor.BackColor = System.Drawing.Color.DarkSlateGray;
            this.gproveedor.Controls.Add(this.label1);
            this.gproveedor.Controls.Add(this.label2);
            this.gproveedor.Controls.Add(this.label3);
            this.gproveedor.Controls.Add(this.txttel);
            this.gproveedor.Controls.Add(this.label4);
            this.gproveedor.Controls.Add(this.txtdireccion);
            this.gproveedor.Controls.Add(this.label5);
            this.gproveedor.Controls.Add(this.txtciudad);
            this.gproveedor.Controls.Add(this.txtrazon);
            this.gproveedor.Controls.Add(this.txtruc);
            this.gproveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gproveedor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gproveedor.Location = new System.Drawing.Point(122, 52);
            this.gproveedor.Name = "gproveedor";
            this.gproveedor.Size = new System.Drawing.Size(424, 264);
            this.gproveedor.TabIndex = 13;
            this.gproveedor.TabStop = false;
            this.gproveedor.Text = "Proveedor";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btinicio);
            this.flowLayoutPanel2.Controls.Add(this.btatras);
            this.flowLayoutPanel2.Controls.Add(this.btsig);
            this.flowLayoutPanel2.Controls.Add(this.btfin);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(210, 337);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(255, 56);
            this.flowLayoutPanel2.TabIndex = 28;
            // 
            // btinicio
            // 
            this.btinicio.Image = ((System.Drawing.Image)(resources.GetObject("btinicio.Image")));
            this.btinicio.Location = new System.Drawing.Point(3, 3);
            this.btinicio.Name = "btinicio";
            this.btinicio.Size = new System.Drawing.Size(53, 53);
            this.btinicio.TabIndex = 15;
            this.btinicio.UseVisualStyleBackColor = true;
            this.btinicio.Click += new System.EventHandler(this.btinicio_Click);
            // 
            // btatras
            // 
            this.btatras.Image = ((System.Drawing.Image)(resources.GetObject("btatras.Image")));
            this.btatras.Location = new System.Drawing.Point(62, 3);
            this.btatras.Name = "btatras";
            this.btatras.Size = new System.Drawing.Size(57, 53);
            this.btatras.TabIndex = 14;
            this.btatras.UseVisualStyleBackColor = true;
            this.btatras.Click += new System.EventHandler(this.btatras_Click);
            // 
            // btsig
            // 
            this.btsig.Image = ((System.Drawing.Image)(resources.GetObject("btsig.Image")));
            this.btsig.Location = new System.Drawing.Point(125, 3);
            this.btsig.Name = "btsig";
            this.btsig.Size = new System.Drawing.Size(59, 53);
            this.btsig.TabIndex = 13;
            this.btsig.UseVisualStyleBackColor = true;
            this.btsig.Click += new System.EventHandler(this.btsig_Click);
            // 
            // btfin
            // 
            this.btfin.Image = ((System.Drawing.Image)(resources.GetObject("btfin.Image")));
            this.btfin.Location = new System.Drawing.Point(190, 3);
            this.btfin.Name = "btfin";
            this.btfin.Size = new System.Drawing.Size(60, 53);
            this.btfin.TabIndex = 12;
            this.btfin.UseVisualStyleBackColor = true;
            this.btfin.Click += new System.EventHandler(this.btfin_Click);
            // 
            // lbRegistro
            // 
            this.lbRegistro.AutoSize = true;
            this.lbRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRegistro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbRegistro.Location = new System.Drawing.Point(392, 14);
            this.lbRegistro.Name = "lbRegistro";
            this.lbRegistro.Size = new System.Drawing.Size(155, 24);
            this.lbRegistro.TabIndex = 29;
            this.lbRegistro.Text = "Proveedor 0 de 0";
            // 
            // frm_proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(651, 425);
            this.Controls.Add(this.lbRegistro);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.gproveedor);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_proveedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GESTION PROVEEDOR";
            this.Load += new System.EventHandler(this.gestionprovee_Load);
            this.panel1.ResumeLayout(false);
            this.gproveedor.ResumeLayout(false);
            this.gproveedor.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnuevo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btbuscar;
        private System.Windows.Forms.Button btmodificar;
        private System.Windows.Forms.Button btguardar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtrazon;
        private System.Windows.Forms.TextBox txtruc;
        private System.Windows.Forms.TextBox txtciudad;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.TextBox txttel;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.Button bteliminar;
        private System.Windows.Forms.GroupBox gproveedor;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btinicio;
        private System.Windows.Forms.Button btatras;
        private System.Windows.Forms.Button btsig;
        private System.Windows.Forms.Button btfin;
        private System.Windows.Forms.ToolTip toolTipprovee;
        private System.Windows.Forms.Label lbRegistro;
    }
}