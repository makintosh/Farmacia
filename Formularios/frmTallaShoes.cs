﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frmTallaShoes : Form
    {

        // variables globales
        string id = "", descripcion = "";
        int tipo = 0, marca = 0;
        bool t;
        public producto[] p;

        
        
        public frmTallaShoes(string id, int tipo, int marca, string descripcion, bool lugar)
        {
            InitializeComponent();
            this.id = id;
            this.descripcion = descripcion;
            this.tipo = tipo;
            this.marca = marca;
            t = lugar;
        }

        private void frmTallaShoes_Load(object sender, EventArgs e)
        {
            stateTextBoxes(false);
        }

        
        /// <summary>
        /// Habilita o Deshabilita TextBoxes
        /// </summary>
        /// <param name="t"></param>
        private void stateTextBoxes(bool t)
        {   
            foreach (Control control in this.Controls)
            {
                if (control is TextBox)
                {
                    TextBox tb = (TextBox)control; //castea
                    tb.Enabled = t;
                }
            }
        }

        private void validarStock(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void validarPVP(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                textBox1.Enabled = true;
                textBox12.Enabled = true;
                textBox1.Focus();
            }
            else
            {
                textBox1.Enabled = false;
                textBox12.Enabled = false;
                textBox1.Clear();
                textBox12.Clear();
            }
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                textBox2.Enabled = true;
                textBox13.Enabled = true;
                textBox2.Focus();
            }
            else
            {
                textBox2.Enabled = false;
                textBox13.Enabled = false;
                textBox2.Clear();
                textBox13.Clear();
            }
        }

        private void checkBox3_Click(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                textBox3.Enabled = true;
                textBox14.Enabled = true;
                textBox3.Focus();
            }
            else
            {
                textBox3.Enabled = false;
                textBox14.Enabled = false;
                textBox3.Clear();
                textBox14.Clear();
            }

        }

        private void checkBox4_Click(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                textBox4.Enabled = true;
                textBox15.Enabled = true;
                textBox4.Focus();
            }
            else
            {
                textBox4.Enabled = false;
                textBox15.Enabled = false;
                textBox4.Clear();
                textBox15.Clear();
            }
        }

        private void checkBox5_Click(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                textBox5.Enabled = true;
                textBox16.Enabled = true;
                textBox5.Focus();
            }
            else
            {
                textBox5.Enabled = false;
                textBox16.Enabled = false;
                textBox5.Clear();
                textBox16.Clear();
            }
        }

        private void checkBox6_Click(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
            {
                textBox6.Enabled = true;
                textBox17.Enabled = true;
                textBox6.Focus();
            }
            else
            {
                textBox6.Enabled = false;
                textBox17.Enabled = false;
                textBox6.Clear();
                textBox17.Clear();
            }
        }

        private void checkBox7_Click(object sender, EventArgs e)
        {
            if (checkBox7.Checked)
            {
                textBox7.Enabled = true;
                textBox18.Enabled = true;
                textBox7.Focus();
            }
            else
            {
                textBox7.Enabled = false;
                textBox18.Enabled = false;
                textBox7.Clear();
                textBox7.Clear();
            }
        }

        private void checkBox8_Click(object sender, EventArgs e)
        {
            if (checkBox8.Checked)
            {
                textBox8.Enabled = true;
                textBox19.Enabled = true;
                textBox8.Focus();
            }
            else
            {
                textBox8.Enabled = false;
                textBox19.Enabled = false;
                textBox8.Clear();
                textBox19.Clear();
            }
        }

        private void checkBox9_Click(object sender, EventArgs e)
        {
            if (checkBox9.Checked)
            {
                textBox9.Enabled = true;
                textBox20.Enabled = true;
                textBox9.Focus();
            }
            else
            {
                textBox9.Enabled = false;
                textBox20.Enabled = false;
                textBox9.Clear();
                textBox20.Clear();
            }
        }

        private void checkBox10_Click(object sender, EventArgs e)
        {
            if (checkBox10.Checked)
            {
                textBox10.Enabled = true;
                textBox21.Enabled = true;
                textBox10.Focus();
            }
            else
            {
                textBox10.Enabled = false;
                textBox21.Enabled = false;
                textBox10.Clear();
                textBox21.Clear();
            }
        }

        private void checkBox11_Click(object sender, EventArgs e)
        {
            if (checkBox11.Checked)
            {
                textBox11.Enabled = true;
                textBox22.Enabled = true;
                textBox11.Focus();
            }
            else
            {
                textBox11.Enabled = false;
                textBox22.Enabled = false;
                textBox11.Clear();
                textBox22.Clear();
            }
        }

        private void buttonAceptar_Click(object sender, EventArgs e)
        {
            short contador = 0, contadorStock = 0, contadorPvp = 0;

            string[] talla = new string[11];
            short[] stock;
            double[] pvp;


            foreach (Control control in this.Controls)
            {
                if (control is CheckBox)
                {
                    CheckBox c = (CheckBox)control;
                    if (c.Checked)
                    {
                        talla[contador] = c.Text;
                        contador++;
                    }
                }
            }

            stock = new short[contador];
            pvp = new double[contador];

            foreach (Control control in this.Controls)
            {

                if (control is TextBox)
                {
                    TextBox t = (TextBox)control;
                    if (t.Enabled && (t.Tag.ToString() == "atock") && t.Text != "")
                    {

                        stock[contadorStock] = short.Parse(t.Text);
                        contadorStock++;
                    }

                    if (t.Enabled && (t.Tag.ToString() == "pvp") && t.Text != "")
                    {
                        pvp[contadorPvp] = double.Parse(t.Text);
                        contadorPvp++;
                    }

                }
            } //

            p = new producto[contador]; //crea el array de producto 
            // producto pro = new producto();

            for (short i = 0; i < p.Length; i++)
            {
                p[i] = new producto();
                if (i == 0)
                    p[i].setCodigo(p[i].numeroMayor() + 1);//codigobd
                else
                    p[i].setCodigo(p[i].numeroMayor() + (1 + i));
                p[i].setCodigoIdentificador(id);// id
                p[i].setDescripcion(descripcion);//descripcion
                p[i].setMarca(marca);//marca 
                p[i].setTipoProducto(tipo);//tipo
                p[i].setTalla(talla[i]);//talla
                p[i].setUnidades(stock[i]);//stock
                p[i].setPrecioVenta(pvp[i]); //pvp
                p[i].setTienda(t);
            }
            this.DialogResult = DialogResult.OK;
        }








    }
}
