﻿namespace fabisa.Formularios
{
    partial class frm_busqueda_usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_busqueda_usuario));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcriterio = new System.Windows.Forms.TextBox();
            this.cmbuser = new System.Windows.Forms.ComboBox();
            this.btaceptar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btcancelar = new System.Windows.Forms.Button();
            this.btreporte = new System.Windows.Forms.Button();
            this.datosUsuario = new System.Windows.Forms.DataGridView();
            this.gpUser = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.datosUsuario)).BeginInit();
            this.gpUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Criterio a Buscar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Buscar";
            // 
            // txtcriterio
            // 
            this.txtcriterio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcriterio.Location = new System.Drawing.Point(175, 69);
            this.txtcriterio.Name = "txtcriterio";
            this.txtcriterio.Size = new System.Drawing.Size(178, 22);
            this.txtcriterio.TabIndex = 3;
            this.txtcriterio.TextChanged += new System.EventHandler(this.txtcriterio_TextChanged);
            // 
            // cmbuser
            // 
            this.cmbuser.FormattingEnabled = true;
            this.cmbuser.Items.AddRange(new object[] {
            "CEDULA",
            "APELLIDO"});
            this.cmbuser.Location = new System.Drawing.Point(175, 30);
            this.cmbuser.Name = "cmbuser";
            this.cmbuser.Size = new System.Drawing.Size(178, 24);
            this.cmbuser.TabIndex = 4;
            this.cmbuser.SelectionChangeCommitted += new System.EventHandler(this.cmbuser_SelectionChangeCommitted);
            // 
            // btaceptar
            // 
            this.btaceptar.Image = ((System.Drawing.Image)(resources.GetObject("btaceptar.Image")));
            this.btaceptar.Location = new System.Drawing.Point(454, 33);
            this.btaceptar.Name = "btaceptar";
            this.btaceptar.Size = new System.Drawing.Size(51, 56);
            this.btaceptar.TabIndex = 5;
            this.toolTip1.SetToolTip(this.btaceptar, "Aceptar");
            this.btaceptar.UseVisualStyleBackColor = true;
            this.btaceptar.Click += new System.EventHandler(this.btaceptar_Click);
            // 
            // btcancelar
            // 
            this.btcancelar.Image = ((System.Drawing.Image)(resources.GetObject("btcancelar.Image")));
            this.btcancelar.Location = new System.Drawing.Point(507, 33);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(55, 56);
            this.btcancelar.TabIndex = 6;
            this.toolTip1.SetToolTip(this.btcancelar, "Cancelar");
            this.btcancelar.UseVisualStyleBackColor = true;
            this.btcancelar.Click += new System.EventHandler(this.btcancelar_Click);
            // 
            // btreporte
            // 
            this.btreporte.Image = ((System.Drawing.Image)(resources.GetObject("btreporte.Image")));
            this.btreporte.Location = new System.Drawing.Point(564, 33);
            this.btreporte.Name = "btreporte";
            this.btreporte.Size = new System.Drawing.Size(51, 56);
            this.btreporte.TabIndex = 7;
            this.toolTip1.SetToolTip(this.btreporte, "Reporte");
            this.btreporte.UseVisualStyleBackColor = true;
            this.btreporte.Click += new System.EventHandler(this.btreporte_Click);
            // 
            // datosUsuario
            // 
            this.datosUsuario.AllowUserToAddRows = false;
            this.datosUsuario.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.datosUsuario.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.datosUsuario.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.datosUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.datosUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosUsuario.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.datosUsuario.Location = new System.Drawing.Point(35, 169);
            this.datosUsuario.Name = "datosUsuario";
            this.datosUsuario.ReadOnly = true;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DarkCyan;
            this.datosUsuario.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.datosUsuario.Size = new System.Drawing.Size(650, 238);
            this.datosUsuario.TabIndex = 8;
            // 
            // gpUser
            // 
            this.gpUser.Controls.Add(this.cmbuser);
            this.gpUser.Controls.Add(this.label1);
            this.gpUser.Controls.Add(this.txtcriterio);
            this.gpUser.Controls.Add(this.label2);
            this.gpUser.Controls.Add(this.btreporte);
            this.gpUser.Controls.Add(this.btaceptar);
            this.gpUser.Controls.Add(this.btcancelar);
            this.gpUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpUser.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gpUser.Location = new System.Drawing.Point(35, 33);
            this.gpUser.Name = "gpUser";
            this.gpUser.Size = new System.Drawing.Size(650, 116);
            this.gpUser.TabIndex = 9;
            this.gpUser.TabStop = false;
            // 
            // frm_busqueda_usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(721, 434);
            this.Controls.Add(this.gpUser);
            this.Controls.Add(this.datosUsuario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_busqueda_usuario";
            this.Opacity = 0.95;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BUSCAR USUARIO";
            this.Load += new System.EventHandler(this.frm_busqueda_usuario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.datosUsuario)).EndInit();
            this.gpUser.ResumeLayout(false);
            this.gpUser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcriterio;
        private System.Windows.Forms.ComboBox cmbuser;
        private System.Windows.Forms.Button btaceptar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.Button btreporte;
        private System.Windows.Forms.DataGridView datosUsuario;
        private System.Windows.Forms.GroupBox gpUser;
    }
}