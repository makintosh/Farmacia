﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.ProviderBase;
using System.Data.OleDb;
using System.Data.Common;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frm_login : Form
    {
               
        public frm_login()
        {
            InitializeComponent();
        }

        usuario obj_usu = new usuario();
        byte numeroOportunidades = 3;

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBoxUsuario.DataSource = obj_usu.datosATablaUsuario();
            comboBoxUsuario.DisplayMember = "nombre_per";
            comboBoxUsuario.ValueMember = "cedula_per";
            
           
        }

        public void logueo(String usuario, String pass)
        {
            DataTable dt_usu = obj_usu.logueo(usuario,pass);          
            if (dt_usu.Rows.Count==1)
            {
                obj_usu.setNombre(dt_usu.Rows[0]["nombre_per"].ToString());
                obj_usu.setCargo(dt_usu.Rows[0]["tipo_usu"].ToString());
                obj_usu.setApellido(dt_usu.Rows[0]["apellido_per"].ToString());
                obj_usu.setCedula(dt_usu.Rows[0]["cedula_per"].ToString());
                frm_principal frm_pri=new frm_principal(obj_usu);
                frm_pri.WindowState = FormWindowState.Maximized;
                this.Hide();
                frm_pri.ShowDialog();
            } 
            
            else 
            {
                MessageBox.Show("Usuario no encontrado");
                numeroOportunidades--;
                lbnumeros.Text = numeroOportunidades.ToString();
                if (numeroOportunidades == 0)
                {
                    Application.Exit();
                }
            }
            

        }

        private void btinicio_Click(object sender, EventArgs e)
        {
            logueo(comboBoxUsuario.SelectedValue.ToString(), txtcontra.Text);
            //txtusu.Clear();
            //txtusu.Focus();
            txtcontra.Clear();


        }

        private void txtusu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void comboBoxUsuario_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtcontra.Focus();
        }

       

    }
}
