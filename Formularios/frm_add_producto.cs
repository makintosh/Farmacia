﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;

namespace fabisa.Formularios
{
    public partial class frm_add_producto : Form
    {
        public frm_add_producto()
        {
            InitializeComponent();
            textBoxCodPer.Select();
        }

        public string tipoProducto , marca;

        public producto objProducto = new producto();

        private void btguardarycerrar_Click(object sender, EventArgs e)
        {
            if (txtdescripcion.Text.Length > 0 && txtpreciocompra.Text.Length > 0 && comboBoxTipoProducto.Text != "" && comboBoxMarca.Text != "")
            {
                objProducto.guardar_producto(nuevoProducto());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private producto nuevoProducto()
        {
            try
            {
                objProducto.setCodigo(objProducto.numeroMayor() + 1);//codigo
                objProducto.setDescripcion(txtdescripcion.Text);// descripcion
                objProducto.setCodigoIdentificador(textBoxCodPer.Text);// id
                objProducto.setMarca(int.Parse(comboBoxMarca.SelectedValue.ToString())); //marca
                tipoProducto = comboBoxTipoProducto.Text;
                marca = comboBoxMarca.Text;
                objProducto.setTipoProducto(int.Parse(comboBoxTipoProducto.SelectedValue.ToString()));//tipo
                objProducto.setTalla(comboBoxTalla.Text);//talla
                objProducto.setPrecioCompra(double.Parse(txtpreciocompra.Text));
                objProducto.setPrecioVenta(double.Parse(txtPrecioVenta.Text));
                objProducto.setUnidades(0);
                objProducto.setIva(check.Checked);
            }

            catch (Exception t)
            {
                MessageBox.Show(t.Message);
            }
            return objProducto;
        }

        private void limpiar_cajas_texto()
        {
            txtdescripcion.Clear();
            txtPrecioVenta.Clear();
            txtpreciocompra.Clear();
            txtpreciocompra.Clear();
            check.Checked = false;
            
            //dt_fecha.Value = DateTime.Now;
            
        }

        private void txtpreciocompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void txtprecioventa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.'))
            {
                e.KeyChar = ',';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void txtprecioventa_Validated(object sender, EventArgs e)
        {
         
        }

        private void frm_add_producto_Load(object sender, EventArgs e)
        {
            textBoxCodPer.Focus();
            fillComboBoxes();            
        }

        private void comboBoxTipoProducto_SelectionChangeCommitted(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Llena los combo box tipoProducto e Marca
        /// </summary>
        public void fillComboBoxes()
        {
            ///////// TIPO PRODUCTO
            comboBoxTipoProducto.DataSource = objProducto.getProductType();
            comboBoxTipoProducto.DisplayMember = "tip_nom";
            comboBoxTipoProducto.ValueMember = "id";
            ///////// MARCA
            comboBoxMarca.DataSource = objProducto.getBrand();
            comboBoxMarca.DisplayMember = "mar_nom";
            comboBoxMarca.ValueMember = "id";

            fillComboBoxTalla();
        }

        private void txtpreciocompra_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void txtPrecioVenta_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(','))
            {
                e.KeyChar = '.';
            }
            else
            {
                if (char.IsNumber(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Llena el combo box de las tallas de acuerdo al tipoProducto
        /// </summary>
        /// <param name="val"></param>
        public void fillComboBoxTalla()
        {
            List<string> listSizes = new List<string>();
            listSizes.Add("GR");
            listSizes.Add("MG");
            listSizes.Add("L");
            listSizes.Add("ML");
            comboBoxTalla.DataSource = listSizes;
        }
    }
}
