﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fabisa.Aplicacion;
using fabisa.Reportes;

namespace fabisa.Formularios
{
    public partial class frm_busqueda_usuario : Form
    {
        public frm_busqueda_usuario()
        {
            InitializeComponent();
        }

        public usuario objUsuario;
        public int numeroUser=0;

        
        private void lbcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_busqueda_usuario_Load(object sender, EventArgs e)
        {
             objUsuario = new usuario();
            datosUsuario.DataSource = objUsuario.datosATablaUsuario();
            datosUsuario.Columns["cedula_per"].HeaderText = "Cedula";
            datosUsuario.Columns["nombre_per"].HeaderText = "Nombre";
            datosUsuario.Columns["apellido_per"].HeaderText = "Apellido";
            datosUsuario.Columns["telefono_per"].HeaderText = "Telefono";
            datosUsuario.Columns["direccion_per"].HeaderText = "Dirección";
            datosUsuario.Columns["tipo_usu"].HeaderText = "Cargo";
            datosUsuario.Columns["sueldo_usu"].Visible = false;
            datosUsuario.Columns["estado_per"].Visible = false;
            datosUsuario.Columns["contrase_usu"].Visible = false;
        }

        private void cmbuser_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtcriterio.Focus();
        }

        private void txtcriterio_TextChanged(object sender, EventArgs e)
        {
            if (cmbuser.Text.Equals(""))
            {
                MessageBox.Show("Criterio está vacio");

            }

            else
            {
                if (cmbuser.SelectedItem.ToString().Equals("CEDULA"))
                {
                    datosUsuario.DataSource = objUsuario.busquedaPorFiltros("cedula_per", txtcriterio.Text);
                }

                if (cmbuser.SelectedItem.ToString().Equals("APELLIDO"))
                {
                    datosUsuario.DataSource = objUsuario.busquedaPorFiltros("apellido_per", txtcriterio.Text);
                }
            }


        }

        private void seleccionar_dato(DataGridView grilladatos)
        {
            if (grilladatos.RowCount > 0)
            {
                objUsuario.setCedula(Convert.ToString(grilladatos.CurrentRow.Cells["cedula_per"].Value));
                objUsuario.setNombre(Convert.ToString(grilladatos.CurrentRow.Cells["nombre_per"].Value));
                objUsuario.setApellido(Convert.ToString(grilladatos.CurrentRow.Cells["apellido_per"].Value));
                objUsuario.setTelefono(Convert.ToString(grilladatos.CurrentRow.Cells["telefono_per"].Value));
                objUsuario.setDireccion(Convert.ToString(grilladatos.CurrentRow.Cells["direccion_per"].Value));
                objUsuario.setCargo(Convert.ToString(grilladatos.CurrentRow.Cells["tipo_usu"].Value));
              //  objUsuario.setSueldo(Convert.ToDouble(grilladatos.CurrentRow.Cells["sueldo_usu"].Value));
                this.DialogResult = DialogResult.OK;
                numeroUser = index(objUsuario.getCedula());
                this.Close();
            }
            else
            {
                MessageBox.Show("CLIENTE NO ENCONTRADO", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btaceptar_Click(object sender, EventArgs e)
        {
            seleccionar_dato(datosUsuario);
        }

        private void btcancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btreporte_Click(object sender, EventArgs e)
        {
            dsReporte ds = new dsReporte();
            byte contadorReporte =0;
            while (contadorReporte < datosUsuario.Rows.Count)
            {
                ds.Tables["dtusuario"].Rows.Add(datosUsuario.Rows[contadorReporte].Cells["tipo_usu"].Value,datosUsuario.Rows[contadorReporte].Cells["sueldo_usu"].Value,datosUsuario.Rows[contadorReporte].Cells["nombre_per"].Value,datosUsuario.Rows[contadorReporte].Cells["apellido_per"].Value,datosUsuario.Rows[contadorReporte].Cells["telefono_per"].Value,datosUsuario.Rows[contadorReporte].Cells["direccion_per"].Value,datosUsuario.Rows[contadorReporte].Cells["cedula_per"].Value);
                contadorReporte++;
            }

            frmReporteUsuarios frmRUsuario = new frmReporteUsuarios(ds);
            frmRUsuario.ShowDialog();
        }

        private int index(string cedulaUsuario)
        {
            int index = 0;
            for (int i = 0; i < objUsuario.datosATablaUsuario().Rows.Count; i++)
            {
                if (cedulaUsuario.Equals(objUsuario.datosATablaUsuario().Rows[i][0].ToString()))
                {
                    return index = i;
                    //i = obj_cli.seleccionartodo().Rows.Count;
                }

            }
            return index;
        }

    }
}
